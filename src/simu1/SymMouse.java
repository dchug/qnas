/*
 * Decompiled with CFR 0_115.
 */
package simu1;

//import java.awt.Component;
//import java.awt.MenuItem;
import java.awt.Point;
//import java.awt.PopupMenu;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import simu1.Cpu;
import simu1.GraphCanvas;

class SymMouse
extends MouseAdapter {
    GraphCanvas mParent;
    Cpu[] mCpu;
    Point mQueueUpperLeft;
    Point mQueueLowerRight;

    public SymMouse(GraphCanvas parent, Cpu[] cpu, Point queueUpperLeft, Point queueLowerRight) {
        this.mParent = parent;
        this.mCpu = cpu;
        this.mQueueUpperLeft = new Point(queueUpperLeft.x, queueUpperLeft.y);
        this.mQueueLowerRight = new Point(queueLowerRight.x, queueLowerRight.y);
    }

    public void mouseClicked(MouseEvent event) {
        int click_X = event.getX();
        int click_Y = event.getY();
        if (click_X >= this.mQueueUpperLeft.x && click_X <= this.mQueueLowerRight.x && click_Y >= this.mQueueUpperLeft.y && click_Y <= this.mQueueLowerRight.y) {
            this.mParent.popup.getItem(0).setLabel("Queue");
            this.mParent.popup.show(this.mParent, click_X, click_Y);
        } else if (click_X >= this.mCpu[0].UpperLeftPoint.x && click_X <= this.mCpu[0].UpperLeftPoint.x + 2 * this.mCpu[0].radius && click_Y >= this.mCpu[0].UpperLeftPoint.y && click_Y <= this.mCpu[0].UpperLeftPoint.y + 2 * this.mCpu[0].radius) {
            this.mParent.popup.getItem(0).setLabel("Server");
            this.mParent.popup.show(this.mParent, click_X, click_Y);
        }
    }
}

