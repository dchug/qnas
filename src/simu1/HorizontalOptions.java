/*
 * Decompiled with CFR 0_115.
 */
package simu1;

import java.applet.AppletContext;
import java.awt.Button;
import java.awt.Choice;
//import java.awt.Component;
import java.awt.Event;
import java.awt.FlowLayout;
import java.awt.Label;
//import java.awt.LayoutManager;
import java.awt.Panel;
//import java.io.PrintStream;
import java.net.URL;
import kcmdg.sntp.InfoStorage;
//import kcmdg.sntp.SetupDialog;
import simu1.Applet1;
//import simu1.GraphCanvas;
//import simu1.Options;
//import simu1.ResultFrame;

@SuppressWarnings("serial")
public class HorizontalOptions
extends Panel {
    Label l1 = new Label("Display: ");
    Label l2 = new Label("Model Selector: ");
    Label mEmptySpace = new Label("         ");
    Label mEmptySpace2 = new Label("         ");
    Button b1 = new Button("Faster");
    Button b2 = new Button("Slower");
    Button b3 = new Button("Single Event");
    Button b4 = new Button("Max Speed");
    Choice mChooser = new Choice();
    String[] mChoose;
    String[] mTarget = new String[]{"simu1", "simu4", "simu3", "simu6", "simu7"};
    Applet1 mParent;
    public InfoStorage mInfoStorage;

    HorizontalOptions(InfoStorage infoStorage, Applet1 myparent) {
        this.mParent = myparent;
        this.mInfoStorage = infoStorage;
        this.setLayout(new FlowLayout());
        this.mChoose = new String[5];
        this.mChoose[0] = new String("Single Server Model");
        this.mChoose[1] = new String("Two Servers Model");
        this.mChoose[2] = new String("Single Server with Feedback");
        this.mChoose[3] = new String("Interactive System");
        this.mChoose[4] = new String("Central Server Model");
        this.mChooser.addItem("");
        this.mChooser.addItem(this.mChoose[0]);
        this.mChooser.addItem(this.mChoose[1]);
        this.mChooser.addItem(this.mChoose[2]);
        this.mChooser.addItem(this.mChoose[3]);
        this.mChooser.addItem(this.mChoose[4]);
        this.add(this.l2);
        this.add(this.mChooser);
        this.add(this.mEmptySpace);
        this.add(this.l1);
        this.add(this.b1);
        this.add(this.b2);
        this.add(this.b3);
        this.add(this.mEmptySpace2);
        this.add(this.b4);
    }

    public boolean action(Event evt, Object arg) {
        String tempSelection;
        if (evt.target instanceof Button) {
            if (((String)arg).equals("Slower")) {
                if (this.mParent.graphcanvas.mDisplayMode == 102) {
                    this.mParent.graphcanvas.changeModeTo(100);
                } else if (this.mParent.graphcanvas.mDisplayMode == 100) {
                    this.mParent.graphcanvas.incrementZoomDelta();
                }
            }
            if (((String)arg).equals("Faster")) {
                if (this.mParent.graphcanvas.mDisplayMode == 101) {
                    this.mParent.graphcanvas.changeModeTo(100);
                } else if (this.mParent.graphcanvas.mDisplayMode == 100) {
                    this.mParent.graphcanvas.decrementZoomDelta();
                }
            }
            if (((String)arg).equals("Max Speed")) {
                this.mParent.graphcanvas.changeModeTo(102);
            }
            if (((String)arg).equals("Single Event")) {
                if (this.mParent.graphcanvas.mDisplayMode != 101) {
                    this.mParent.graphcanvas.changeModeTo(101);
                } else {
                    this.mParent.graphcanvas.mSingleStepMode_MoveOneStep = 1;
                }
            }
        }
        if (evt.target instanceof Choice && (tempSelection = this.mChooser.getSelectedItem().trim()).length() > 0) {
            this.display(tempSelection);
        }
        return true;
    }

    @SuppressWarnings("deprecation")
	public void display(String url) {
        int target = -1;
        int i = 0;
        while (i < 5) {
            if (this.mChoose[i].equals(url)) {
                target = i;
                break;
            }
            ++i;
        }
        if (target < 0) {
            return;
        }
        try {
            if (this.mParent.mTableSorterFrame != null) {
                this.mParent.mTableSorterFrame.hide();
                this.mParent.mTableSorterFrame.dispose();
            }
            if (this.mParent.options.szSetupDialog != null) {
                this.mParent.options.szSetupDialog.hide();
                this.mParent.options.szSetupDialog.dispose();
            }
            AppletContext context = this.mParent.getAppletContext();
            URL szCurrentAppletURL = this.mParent.getCodeBase();
            String tempStr = szCurrentAppletURL.toString().trim();
            tempStr = tempStr.concat(this.mTarget[target]);
            tempStr = tempStr.concat(".html");
            szCurrentAppletURL = new URL(tempStr);
            System.out.println("URL == " + tempStr);
            context.showDocument(szCurrentAppletURL);
        }
        catch (Exception e) {
            System.out.println("Error == " + e);
        }
    }
}

