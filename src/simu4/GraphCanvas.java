package simu4;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.Point;
import java.awt.PopupMenu;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.ImageObserver;
import java.io.PrintStream;
import java.text.NumberFormat;
import java.util.NoSuchElementException;
import kcmdg.sntp.InfoStorage;
import simu4.Applet1;
import simu4.Arrow;
import simu4.Cpu;
import simu4.EventVector;
import simu4.HorizontalJoin;
import simu4.InitTaskVector;
import simu4.RecycleChannel;
import simu4.ResultFrame;
import simu4.Square;
import simu4.Task;
import simu4.TaskQVector;

public class GraphCanvas
extends Canvas
implements Runnable {
    final int MAXNODES = 20;
    final int MAXQUEUELEN = 15;
    final int JOB_NO = 100;
    final int QSIZE = 500;
    public int mTaskQVectorNum = 1;
    int queueLen;
    int jobNumber;
    int arrowNumber;
    int cpuNumber;
    int mHorizontalJoinNumber;
    final double mFasterCoefficient = 0.8;
    final double mSlowerCoefficient = 1.2;
    private Cpu[] CPU;
    private Square[] qBox;
    private Arrow[] arrow;
    public TaskQVector[] mTaskQVector;
    public HorizontalJoin[] mHorizontalJoin;
    public RecycleChannel mEntryChannel;
    public RecycleChannel mExitChannel;
    private InitTaskVector mTasks;
    public EventVector mEventVector;
    public int mRunFlag = 0;
    static final int WAIT = 0;
    static final int RUN = 1;
    public int mDisplayMode;
    public int mSingleStepMode_MoveOneStep = 0;
    static final int NORMAL_DISPLAY_MODE = 100;
    static final int SINGLE_STEP_DISPLAY_MODE = 101;
    static final int MAX_SPEED_DISPLAY_MODE = 102;
    public double mZoomDelta = 1.0;
    public NumberFormat mDoubleFormat;
    public NumberFormat mIntegerFormat;
    public NumberFormat mDoubleFormat7Digits;
    public boolean mFromBeginning;
    Font roman = new Font("TimesRoman", 1, 12);
    Font helvetica = new Font("Helvetica", 1, 15);
    FontMetrics fmetrics;
    int h;
    private Image offScreenImage;
    private Graphics offScreenGraphics;
    private Dimension offScreenSize;
    Thread algrthm;
    public boolean mStepByStepMode;
    Applet1 parent;
    InfoStorage mInfoStorage;
    PopupMenu popup;
    MenuItem mCpuMenuItem;
    static long[] r;
    static long xmod;
    static double rnmax;
    static int i;
    static int j;
    static int k;
    static int n;
    static int n2;
    static int firstcall;
    static double[] table;

    static {
        long[] arrl = new long[18];
        arrl[0] = 585064;
        arrl[1] = 120340;
        arrl[2] = 336032;
        arrl[3] = 360031;
        arrl[4] = 645901;
        arrl[5] = 19751;
        arrl[6] = 19746;
        arrl[7] = 60323;
        arrl[8] = 17001;
        arrl[9] = 89672;
        arrl[10] = 20304;
        arrl[11] = 12434;
        arrl[12] = 45302;
        arrl[13] = 89603;
        arrl[14] = 31234;
        arrl[15] = 68690;
        arrl[16] = 234085;
        r = arrl;
        xmod = 1049057;
        rnmax = xmod;
        i = 17;
        j = 16;
        k = 0;
        n = 18;
        n2 = 200;
        firstcall = 1;
        table = new double[n2];
    }

    GraphCanvas(InfoStorage infoStorage, Applet1 myparent) {
        this.fmetrics = this.getFontMetrics(this.roman);
        this.h = this.fmetrics.getHeight() / 3;
        this.parent = myparent;
        this.mInfoStorage = infoStorage;
        this.setBackground(Color.white);
        this.mDisplayMode = 100;
        this.mRunFlag = 0;
        this.mFromBeginning = true;
    }

    double FINV(double U, double A, double S) {
        return (Math.sqrt(S * S + 2.0 * A * U) - S) / A;
    }

    double RNG(int ID, double A, double S) {
        double R = 0.0;
        double U = this.URNG();
        if (ID < 1 || ID > 5) {
            System.out.println("\n\nERROR: Incorrect selection of random number generator\n");
            System.exit(1);
        }
        switch (ID) {
            case 1: {
                if (A <= 0.0 && S <= 0.0) {
                    R = 0.001;
                    break;
                }
                int i = 2;
                while (i < 13) {
                    U += this.URNG();
                    ++i;
                }
                R = A + S * (U - 6.0);
                break;
            }
            case 2: {
                if (A <= 0.0 && S <= 0.0) {
                    R = 0.001;
                    break;
                }
                R = A + U * (S - A);
                break;
            }
            case 3: {
                if (A <= 0.0) {
                    R = 0.001;
                    break;
                }
                R = (- A) * Math.log(U);
                break;
            }
            case 4: {
                R = this.FINV(U, A, S);
                break;
            }
            case 5: {
                if (A <= 0.0) {
                    R = 0.001;
                    break;
                }
                R = A;
                break;
            }
        }
        return R;
    }

    double URNG() {
        GraphCanvas.r[GraphCanvas.i] = (r[j] + r[k]) % xmod;
        double rn = (double)r[i] / rnmax;
        i = (i + 1) % n;
        j = (j + 1) % n;
        k = (k + 1) % n;
        return rn;
    }

    static /* synthetic */ Square[] access$0(GraphCanvas $0) {
        return $0.qBox;
    }

    static /* synthetic */ Cpu[] access$1(GraphCanvas $0) {
        return $0.CPU;
    }

    public void clear() {
        int i;
        if (this.algrthm != null) {
            this.algrthm.stop();
        }
        int j = 0;
        while (j < this.mTaskQVectorNum) {
            this.mTaskQVector[j] = null;
            ++j;
        }
        this.mTaskQVector = null;
        this.mTasks = null;
        if (this.queueLen > 0) {
            i = 0;
            while (i < this.queueLen) {
                this.qBox[i].setCurrTask(null);
                ++i;
            }
        }
        if (this.cpuNumber > 0) {
            i = 0;
            while (i < this.cpuNumber) {
                this.CPU[i].setRunningTask(null);
                ++i;
            }
        }
        this.mExitChannel.clearAllTasks();
        this.mEntryChannel.clearAllTasks();
        this.mDisplayMode = 100;
        this.mRunFlag = 0;
        this.mSingleStepMode_MoveOneStep = 0;
        this.repaint();
    }

    @SuppressWarnings("unchecked")
	public double createNewTask(int IDA, double AA, double SA, double szEventTime) {
        double TA = 0.0;
        while ((TA = this.RNG(IDA, AA, SA)) <= 0.0) {
        }
        Task newTask = this.mTasks.createNewColorTask();
        newTask.mRemainingInterArrivalTime = newTask.mInterArrivalTime = TA;
        newTask.mEventTime = szEventTime += TA;
        newTask.mNextMove = 1;
        newTask.mIsBeingProcessed = false;
        this.mEventVector.addElement(newTask);
        return szEventTime;
    }

    public void decrementZoomDelta() {
        this.mZoomDelta *= 0.8;
        this.mInfoStorage.mSlowModelAvgServTime = (int)(this.mZoomDelta * this.mInfoStorage.mAvgServA[0]);
    }

    public void incrementZoomDelta() {
        this.mZoomDelta *= 1.2;
        this.mInfoStorage.mSlowModelAvgServTime = (int)(this.mZoomDelta * this.mInfoStorage.mAvgServA[0]);
    }

    public void init() {
        this.mDisplayMode = 100;
        this.mSingleStepMode_MoveOneStep = 0;
        this.queueLen = 15;
        this.arrowNumber = 1;
        this.cpuNumber = 2;
        this.mHorizontalJoinNumber = 2;
        this.mTaskQVectorNum = 1;
        this.qBox = new Square[this.queueLen];
        this.CPU = new Cpu[this.cpuNumber];
        this.arrow = new Arrow[this.arrowNumber];
        this.mHorizontalJoin = new HorizontalJoin[this.mHorizontalJoinNumber];
        int w = 8;
        int h = 4;
        int midPointX = 530;
        int midPointY = 170;
        int i = 0;
        while (i < this.queueLen) {
            this.qBox[i] = new Square(midPointX - i * 4 * w, midPointY, 4 * w, Color.black);
            ++i;
        }
        i = 0;
        while (i < this.arrowNumber) {
            if (i == 0) {
                this.arrow[i] = new Arrow(midPointX - (this.queueLen + 1) * 4 * w, midPointY + 2 * w, midPointX - (this.queueLen - 1) * 4 * w - w, midPointY + 2 * w);
            }
            ++i;
        }
        i = 0;
        while (i < this.mHorizontalJoinNumber) {
            if (i == 0) {
                Point line1_start = new Point(this.qBox[0].UpperLeftPoint.x + this.qBox[0].height, this.qBox[0].UpperLeftPoint.y + this.qBox[0].height / 2);
                Point line1_end = new Point(this.qBox[0].UpperLeftPoint.x + this.qBox[0].height + 4 * w, this.qBox[0].UpperLeftPoint.y + this.qBox[0].height / 2);
                Arrow tempArrow = new Arrow(line1_start, line1_end);
                HorizontalJoin temp_H_join = new HorizontalJoin();
                this.mHorizontalJoin[i] = new HorizontalJoin(tempArrow, 1, w);
            }
            if (i == 1) {
                Point start = new Point(this.mHorizontalJoin[0].mArrow[1].getRealHead_X() + 8 * w, this.mHorizontalJoin[0].mArrow[0].start.y);
                Point end = new Point(this.mHorizontalJoin[0].mArrow[1].getRealHead_X() + 12 * w, this.mHorizontalJoin[0].mArrow[0].start.y);
                Arrow anArrow = new Arrow(start, end);
                Point line1_start = new Point(anArrow.getRealHead_X() + 2 * w, this.mHorizontalJoin[0].mArrow[0].start.y);
                Point line1_end = new Point(anArrow.getRealHead_X() + 6 * w, this.mHorizontalJoin[0].mArrow[0].start.y);
                Arrow tempArrow = new Arrow(line1_start, line1_end);
                HorizontalJoin temp_H_join = new HorizontalJoin();
                this.mHorizontalJoin[i] = new HorizontalJoin(tempArrow, 2, w);
            }
            ++i;
        }
        i = 0;
        while (i < this.cpuNumber) {
            if (i == 0) {
                int arrowRealHead_X = this.mHorizontalJoin[0].mArrow[1].getRealHead_X();
                int arrowRealHead_Y = this.mHorizontalJoin[0].mArrow[1].getRealHead_Y();
                this.CPU[i] = new Cpu(arrowRealHead_X, arrowRealHead_Y - 4 * w, 4 * w, Color.white, this);
                this.CPU[i].setName("CPU0");
            } else if (i == 1) {
                int arrowRealHead_X = this.mHorizontalJoin[0].mArrow[2].getRealHead_X();
                int arrowRealHead_Y = this.mHorizontalJoin[0].mArrow[2].getRealHead_Y();
                this.CPU[i] = new Cpu(arrowRealHead_X, arrowRealHead_Y - 4 * w, 4 * w, Color.white, this);
                this.CPU[i].setName("CPU1");
            }
            ++i;
        }
        this.mTaskQVector = null;
        this.mTaskQVector = new TaskQVector[this.mTaskQVectorNum];
        this.mTaskQVector[0] = new TaskQVector(500, this.CPU);
        double[] szProbability = new double[]{0.5, 0.5};
        this.mTaskQVector[0].setRules(szProbability, null);
        this.mTaskQVector[0].setVisualQueue(this.qBox);
        this.mTaskQVector[0].mParent = this.parent;
        this.CPU[0].setSourceTaskQueue(this.mTaskQVector[0]);
        this.CPU[1].setSourceTaskQueue(this.mTaskQVector[0]);
        this.mEventVector = new EventVector();
        this.mTasks = null;
        this.mTasks = new InitTaskVector(this, this.mInfoStorage);
        this.mEntryChannel = new RecycleChannel(new Point(this.arrow[0].getRealHead_X() - 12 * w, this.arrow[0].getRealHead_Y()), new Point(this.arrow[0].getRealHead_X() - 8 * w, this.arrow[0].getRealHead_Y()), new Point(this.arrow[0].getRealHead_X() - 4 * w, this.arrow[0].getRealHead_Y()), new Point(this.arrow[0].getRealHead_X(), this.arrow[0].getRealHead_Y()), this.mTaskQVector[0]);
        this.mEntryChannel.setName("entry channel");
        this.mEntryChannel.setParent(this);
        this.mExitChannel = new RecycleChannel(new Point(this.mHorizontalJoin[1].mArrow[0].getRealHead_X(), this.mHorizontalJoin[1].mArrow[0].getRealHead_Y()), new Point(this.mHorizontalJoin[1].mArrow[0].getRealHead_X() + 3 * w, this.mHorizontalJoin[1].mArrow[0].getRealHead_Y()), new Point(this.mHorizontalJoin[1].mArrow[0].getRealHead_X() + 6 * w, this.mHorizontalJoin[1].mArrow[0].getRealHead_Y()), new Point(this.mHorizontalJoin[1].mArrow[0].getRealHead_X() + 9 * w, this.mHorizontalJoin[1].mArrow[0].getRealHead_Y()), null);
        this.mExitChannel.setName("exit channel");
        this.mExitChannel.setParent(this);
        this.CPU[0].mExitChannel = this.mExitChannel;
        this.CPU[1].mExitChannel = this.mExitChannel;
        this.mDoubleFormat = NumberFormat.getNumberInstance();
        this.mDoubleFormat.setMaximumFractionDigits(3);
        this.mDoubleFormat.setMinimumFractionDigits(3);
        this.mDoubleFormat.setMaximumIntegerDigits(20);
        this.mDoubleFormat7Digits = NumberFormat.getNumberInstance();
        this.mDoubleFormat7Digits.setMaximumFractionDigits(7);
        this.mDoubleFormat7Digits.setMinimumFractionDigits(3);
        this.mDoubleFormat7Digits.setMaximumIntegerDigits(20);
        this.mIntegerFormat = NumberFormat.getNumberInstance();
        this.mIntegerFormat.setParseIntegerOnly(true);
        this.mZoomDelta = (double)this.mInfoStorage.mSlowModelAvgServTime / this.mInfoStorage.mAvgServA[0];
        this.algrthm = null;
        this.algrthm = new Thread(this);
        this.popup = new PopupMenu();
        this.mCpuMenuItem = new MenuItem("Server");
        this.popup.add(this.mCpuMenuItem);
        this.add(this.popup);
        SymMouse aSymMouse = new SymMouse();
        this.addMouseListener(aSymMouse);
        this.repaint();
    }

    public String intToString(int i) {
        char c = (char)(97 + i);
        return String.valueOf(c);
    }

    public void moveTaskForward(TaskQVector szTasksInQueue, Square[] szSquare) {
        int i = 0;
        int QueueLen = szSquare.length;
        if (szTasksInQueue == null || szTasksInQueue.isEmpty()) {
            int j = 0;
            while (j < QueueLen) {
                szSquare[j].setCurrTask(null);
                ++j;
            }
            return;
        }
        i = 0;
        while (i < szTasksInQueue.size()) {
            if (i >= QueueLen) {
                return;
            }
            Task task = (Task)szTasksInQueue.elementAt(i);
            szSquare[i].setCurrTask(task);
            ++i;
        }
        if (i >= QueueLen) {
            return;
        }
        int j = i;
        while (j < QueueLen) {
            szSquare[j].setCurrTask(null);
            ++j;
        }
    }

    public void paint(Graphics g) {
        int i;
        g.setFont(this.roman);
        g.setColor(Color.white);
        Color oldColor = Color.white;
        if (this.queueLen > 0) {
            i = 0;
            while (i < this.queueLen) {
                this.qBox[i].drawSquare(g);
                ++i;
            }
        }
        if (this.arrowNumber > 0) {
            i = 0;
            while (i < this.arrowNumber) {
                this.arrow[i].drawArrow(g);
                ++i;
            }
        }
        i = 0;
        while (i < this.mHorizontalJoinNumber) {
            this.mHorizontalJoin[i].drawHorizontalJoin(g);
            ++i;
        }
        if (this.cpuNumber > 0) {
            i = 0;
            while (i < this.cpuNumber) {
                this.CPU[i].drawCpu(g);
                ++i;
            }
        }
        this.mExitChannel.drawRecycleChannel(g);
        this.mEntryChannel.drawRecycleChannel(g);
        g.setColor(oldColor);
    }

    double rng() {
        if (firstcall > 0) {
            int i = 0;
            while (i < n) {
                GraphCanvas.table[i] = this.URNG();
                ++i;
            }
            firstcall = 0;
        }
        int itable = (int)((double)n * this.URNG());
        double rnumber = table[itable];
        GraphCanvas.table[itable] = this.URNG();
        return rnumber;
    }

    @SuppressWarnings("deprecation")
	public void run() {
        int szIndex = 0;
        double TIME = 0.0;
        double T = 0.0;
        long JOBA = 0;
        long JOBS = 0;
        double TBUSY = 0.0;
        double JOBSEC = 0.0;
        double SDJOB = 0.0;
        double TNA = 0.0;
        double TND = 0.0;
        double ATS = 0.0;
        double SDTS = 0.0;
        double ATA = 0.0;
        double SDTA = 0.0;
        double ART = 0.0;
        double SDRT = 0.0;
        int JMAX = 100;
        int JPRINT = this.mInfoStorage.mJPRINT;
        int IDA = this.mInfoStorage.mArrivalDistribution;
        double AA = this.mInfoStorage.mAvgArrivalA;
        double SA = this.mInfoStorage.mAvgArrivalS;
        int IDS = this.mInfoStorage.mServDistribution[0];
        int k = 0;
        while (k < this.cpuNumber) {
            this.CPU[k].AS = this.mInfoStorage.mAvgServA[k];
            this.CPU[k].SS = this.mInfoStorage.mAvgServS[k];
            ++k;
        }
        System.out.println("\n\nSIMULATION RESULTS\n\nJOBS  TIME   ATA  SDTA  ATS  SDTS  ART  SDRT  AJOB  SDJOB  U   X\tT\n-----------------------------------------------------------------------------");
        double szEventTime = TIME;
        int i = 0;
        while (i < JMAX) {
            szEventTime = this.createNewTask(IDA, AA, SA, szEventTime);
            ++i;
        }
        while (this.mEventVector.size() > 0) {
            double XATS;
            double U;
            double U1;
            double XART;
            double XSDJOB;
            double XSDTS;
            double U2;
            double XSDTA;
            double AJOB;
            double X;
            double XATA;
            double XSDRT;
            if (this.mRunFlag == 1) {
                block88 : {
                    while (this.mDisplayMode == 101 && this.mSingleStepMode_MoveOneStep <= 0) {
                        try {
                            Thread.currentThread();
                            Thread.sleep(500);
                            continue;
                        }
                        catch (InterruptedException v0) {
                            continue;
                        }
                        catch (Exception v1) {}
                    }
                    this.mSingleStepMode_MoveOneStep = 0;
                    try {
                        Task szIncomingTask;
                        double TS;
                        long NJOB;
                        double szElapsedTime;
                        double DELTAT;
                        szIncomingTask = (Task)this.mEventVector.firstElement();
                        this.mEventVector.removeElementAt(0);
                        if (szIncomingTask.mNextMove == 1) {
                            szElapsedTime = szIncomingTask.mRemainingInterArrivalTime;
                            if (szIncomingTask.mRemainingInterArrivalTime < 0.0) {
                                szIncomingTask.mRemainingInterArrivalTime = 0.0;
                            }
                            if (this.mDisplayMode != 102) {
                                try {
                                    Thread.currentThread();
                                    Thread.sleep((int)(szIncomingTask.mRemainingInterArrivalTime * this.mZoomDelta));
                                }
                                catch (InterruptedException v2) {
                                }
                                catch (Exception v3) {}
                            }
                            NJOB = JOBA - JOBS;
                            ++JOBA;
                            ATA += szIncomingTask.mInterArrivalTime;
                            SDTA += szIncomingTask.mInterArrivalTime * szIncomingTask.mInterArrivalTime;
                            DELTAT = szIncomingTask.mEventTime - TIME;
                            if (NJOB > 0) {
                                TBUSY += DELTAT;
                            }
                            JOBSEC += DELTAT * (double)NJOB;
                            SDJOB += DELTAT * (double)NJOB * (double)NJOB;
                            TIME = szIncomingTask.mEventTime;
                            int i2 = 0;
                            while (i2 < this.cpuNumber) {
                                if (this.CPU[i2].mCurrentTask != null) {
                                    this.CPU[i2].mTBUSY += DELTAT;
                                }
                                ++i2;
                            }
                            szIncomingTask.mIsBeingProcessed = false;
                            szIncomingTask.mRealArrivalTime = szIncomingTask.mEventTime;
                            if (this.mDisplayMode != 102) {
                                this.mEntryChannel.recycleTask(szIncomingTask);
                            } else {
                                this.mTaskQVector[0].appendToList(szIncomingTask);
                            }
                            if (this.mDisplayMode != 102) {
                                this.moveTaskForward(this.mTaskQVector[0], this.mTaskQVector[0].mVisualQueue);
                            }
                            double szRng = this.rng();
                            int j = 0;
                            while (j < this.mTaskQVectorNum) {
                                Task szFirstTask;
                                int i3;
                                int szServerNumInTaskQueue = this.mTaskQVector[j].mConnectedServers.length;
                                if (szRng > 0.5) {
                                    i3 = 0;
                                    while (i3 < szServerNumInTaskQueue) {
                                        if (this.mTaskQVector[j].mConnectedServers[i3].mCurrentTask == null) {
                                            if (this.mTaskQVector[j].size() > 0) {
                                                szFirstTask = null;
                                                szFirstTask = this.mTaskQVector[j].getFirstUnprocessedElement();
                                                if (szFirstTask != null) {
                                                    szFirstTask.mIsBeingProcessed = true;
                                                    while ((TS = this.RNG(IDS, this.mTaskQVector[j].mConnectedServers[i3].AS, this.mTaskQVector[j].mConnectedServers[i3].SS)) <= 0.0) {
                                                    }
                                                    szFirstTask.mRemainingServiceTime = szFirstTask.mServiceTime = TS;
                                                    szFirstTask.mEventTime = TIME + szFirstTask.mServiceTime;
                                                    szFirstTask.mNextMove = 2;
                                                    szFirstTask.mSourceServer = this.mTaskQVector[j].mConnectedServers[i3];
                                                    szFirstTask.mTimeInServers += szFirstTask.mServiceTime;
                                                    this.mTaskQVector[j].setJobDirection(szFirstTask);
                                                    this.mEventVector.insertTaskByEventTime(szFirstTask);
                                                    this.mTaskQVector[j].mConnectedServers[i3].setRunningTask(szFirstTask);
                                                }
                                            }
                                        } else {
                                            this.mTaskQVector[j].mConnectedServers[i3].mCurrentTask.mRemainingServiceTime -= szElapsedTime;
                                        }
                                        ++i3;
                                    }
                                } else {
                                    i3 = szServerNumInTaskQueue - 1;
                                    while (i3 >= 0) {
                                        if (this.mTaskQVector[j].mConnectedServers[i3].mCurrentTask == null) {
                                            if (this.mTaskQVector[j].size() > 0) {
                                                szFirstTask = null;
                                                szFirstTask = this.mTaskQVector[j].getFirstUnprocessedElement();
                                                if (szFirstTask != null) {
                                                    szFirstTask.mIsBeingProcessed = true;
                                                    while ((TS = this.RNG(IDS, this.mTaskQVector[j].mConnectedServers[i3].AS, this.mTaskQVector[j].mConnectedServers[i3].SS)) <= 0.0) {
                                                    }
                                                    szFirstTask.mRemainingServiceTime = szFirstTask.mServiceTime = TS;
                                                    szFirstTask.mEventTime = TIME + szFirstTask.mServiceTime;
                                                    szFirstTask.mNextMove = 2;
                                                    szFirstTask.mSourceServer = this.mTaskQVector[j].mConnectedServers[i3];
                                                    szFirstTask.mTimeInServers += szFirstTask.mServiceTime;
                                                    this.mTaskQVector[j].setJobDirection(szFirstTask);
                                                    this.mEventVector.insertTaskByEventTime(szFirstTask);
                                                    this.mTaskQVector[j].mConnectedServers[i3].setRunningTask(szFirstTask);
                                                }
                                            }
                                        } else {
                                            this.mTaskQVector[j].mConnectedServers[i3].mCurrentTask.mRemainingServiceTime -= szElapsedTime;
                                        }
                                        --i3;
                                    }
                                }
                                ++j;
                            }
                            if (this.mDisplayMode != 102) {
                                this.repaint();
                            }
                            break block88;
                        }
                        if (szIncomingTask.mNextMove != 2) break block88;
                        szElapsedTime = szIncomingTask.mRemainingServiceTime;
                        if (szIncomingTask.mRemainingServiceTime < 0.0) {
                            szIncomingTask.mRemainingServiceTime = 0.0;
                        }
                        if (this.mDisplayMode != 102 && szIncomingTask.mSourceServer.mCurrentTask != null) {
                            try {
                                Thread.currentThread();
                                Thread.sleep((int)(szIncomingTask.mRemainingServiceTime * this.mZoomDelta));
                            }
                            catch (InterruptedException v4) {
                            }
                            catch (Exception v5) {}
                        }
                        boolean szShouldExit = false;
                        Task szExitTask = null;
                        Object szRecycleTask = null;
                        if (szIncomingTask.mDestTaskQueue == null) {
                            NJOB = JOBA - JOBS;
                            DELTAT = szIncomingTask.mEventTime - TIME;
                            TBUSY += DELTAT;
                            JOBSEC += DELTAT * (double)NJOB;
                            SDJOB += DELTAT * (double)NJOB * (double)NJOB;
                            TIME = szIncomingTask.mEventTime;
                            ++JOBS;
                            ATS += szIncomingTask.mServiceTime;
                            SDTS += szIncomingTask.mServiceTime * szIncomingTask.mServiceTime;
                            ART = ART + TIME - szIncomingTask.mRealArrivalTime;
                            SDRT += (TIME - szIncomingTask.mRealArrivalTime) * (TIME - szIncomingTask.mRealArrivalTime);
                            int i4 = 0;
                            while (i4 < this.cpuNumber) {
                                if (this.CPU[i4].mCurrentTask != null) {
                                    this.CPU[i4].mTBUSY += DELTAT;
                                }
                                ++i4;
                            }
                            TaskQVector szSrcTaskQueue = szIncomingTask.mSourceServer.mTaskQVector;
                            int szIndex2 = szSrcTaskQueue.indexOf(szIncomingTask);
                            if (szIndex2 >= 0) {
                                szSrcTaskQueue.removeElementAt(szIndex2);
                            }
                            szIncomingTask.mSourceServer.setRunningTask(null);
                            if (this.mDisplayMode != 102) {
                                this.moveTaskForward(szSrcTaskQueue, szSrcTaskQueue.mVisualQueue);
                            }
                            szShouldExit = true;
                            szExitTask = szIncomingTask;
                            szEventTime = this.createNewTask(IDA, AA, SA, szEventTime);
                        }
                        double szRng = this.rng();
                        int j = 0;
                        while (j < this.mTaskQVectorNum) {
                            Task szFirstTask;
                            int i5;
                            int szServerNumInTaskQueue = this.mTaskQVector[j].mConnectedServers.length;
                            if (szRng > 0.5) {
                                i5 = 0;
                                while (i5 < szServerNumInTaskQueue) {
                                    if (this.mTaskQVector[j].mConnectedServers[i5].mCurrentTask == null) {
                                        if (this.mTaskQVector[j].size() > 0) {
                                            szFirstTask = null;
                                            szFirstTask = this.mTaskQVector[j].getFirstUnprocessedElement();
                                            if (szFirstTask != null) {
                                                szFirstTask.mIsBeingProcessed = true;
                                                while ((TS = this.RNG(IDS, this.mTaskQVector[j].mConnectedServers[i5].AS, this.mTaskQVector[j].mConnectedServers[i5].SS)) <= 0.0) {
                                                }
                                                szFirstTask.mRemainingServiceTime = szFirstTask.mServiceTime = TS;
                                                szFirstTask.mEventTime = TIME + szFirstTask.mServiceTime;
                                                szFirstTask.mNextMove = 2;
                                                szFirstTask.mSourceServer = this.mTaskQVector[j].mConnectedServers[i5];
                                                szFirstTask.mTimeInServers += szFirstTask.mServiceTime;
                                                this.mTaskQVector[j].setJobDirection(szFirstTask);
                                                this.mEventVector.insertTaskByEventTime(szFirstTask);
                                                this.mTaskQVector[j].mConnectedServers[i5].setRunningTask(szFirstTask);
                                            }
                                        }
                                    } else {
                                        this.mTaskQVector[j].mConnectedServers[i5].mCurrentTask.mRemainingServiceTime -= szElapsedTime;
                                    }
                                    ++i5;
                                }
                            } else {
                                i5 = szServerNumInTaskQueue - 1;
                                while (i5 >= 0) {
                                    if (this.mTaskQVector[j].mConnectedServers[i5].mCurrentTask == null) {
                                        if (this.mTaskQVector[j].size() > 0) {
                                            szFirstTask = null;
                                            szFirstTask = this.mTaskQVector[j].getFirstUnprocessedElement();
                                            if (szFirstTask != null) {
                                                szFirstTask.mIsBeingProcessed = true;
                                                while ((TS = this.RNG(IDS, this.mTaskQVector[j].mConnectedServers[i5].AS, this.mTaskQVector[j].mConnectedServers[i5].SS)) <= 0.0) {
                                                }
                                                szFirstTask.mRemainingServiceTime = szFirstTask.mServiceTime = TS;
                                                szFirstTask.mEventTime = TIME + szFirstTask.mServiceTime;
                                                szFirstTask.mNextMove = 2;
                                                szFirstTask.mSourceServer = this.mTaskQVector[j].mConnectedServers[i5];
                                                szFirstTask.mTimeInServers += szFirstTask.mServiceTime;
                                                this.mTaskQVector[j].setJobDirection(szFirstTask);
                                                this.mEventVector.insertTaskByEventTime(szFirstTask);
                                                this.mTaskQVector[j].mConnectedServers[i5].setRunningTask(szFirstTask);
                                            }
                                        }
                                    } else {
                                        this.mTaskQVector[j].mConnectedServers[i5].mCurrentTask.mRemainingServiceTime -= szElapsedTime;
                                    }
                                    --i5;
                                }
                            }
                            ++j;
                        }
                        if (this.mEventVector.size() > 0) {
                            ((Task)this.mEventVector.firstElement()).mRemainingInterArrivalTime -= szElapsedTime;
                        }
                        if (szShouldExit && this.mDisplayMode != 102) {
                            this.mExitChannel.recycleTask(szExitTask);
                            szShouldExit = false;
                        }
                        if (this.mDisplayMode != 102) {
                            this.repaint();
                        }
                        if (JOBS % (long)JPRINT > 0) continue;
                        U = TBUSY / TIME;
                        U1 = this.CPU[0].mTBUSY / TIME;
                        U2 = this.CPU[1].mTBUSY / TIME;
                        AJOB = JOBSEC / TIME;
                        XSDJOB = SDJOB / TIME - AJOB * AJOB <= 0.0 ? 0.0 : Math.sqrt(SDJOB / TIME - AJOB * AJOB);
                        XATS = JOBS > 0 ? ATS / (double)JOBS : 0.0;
                        XSDTS = SDTS / (double)JOBS - XATS * XATS <= 0.0 ? 0.0 : Math.sqrt(SDTS / (double)JOBS - XATS * XATS);
                        XATA = JOBA > 0 ? ATA / (double)JOBA : 0.0;
                        XSDTA = SDTA / (double)JOBA - XATA * XATA <= 0.0 ? 0.0 : Math.sqrt(SDTA / (double)JOBA - XATA * XATA);
                        XART = JOBS > 0 ? ART / (double)JOBS : 0.0;
                        XSDRT = SDRT / (double)JOBS - XART * XART <= 0.0 ? 0.0 : Math.sqrt(SDRT / (double)JOBS - XART * XART);
                        X = (double)JOBS / TIME;
                        double XT = T / (double)JOBS;
                        System.out.println(String.valueOf(this.mIntegerFormat.format(JOBS)) + "   " + this.mDoubleFormat.format(TIME) + "   " + this.mDoubleFormat.format(XATA) + "   " + this.mDoubleFormat.format(XSDTA) + "   " + this.mDoubleFormat.format(XATS) + "   " + this.mDoubleFormat.format(XSDTS) + "   " + this.mDoubleFormat.format(XART) + "   " + this.mDoubleFormat.format(XSDRT) + "   " + this.mDoubleFormat.format(AJOB) + "   " + this.mDoubleFormat.format(XSDJOB) + "   " + this.mDoubleFormat.format(U1) + "   " + this.mDoubleFormat.format(U2) + "   " + this.mDoubleFormat.format(X) + "   " + this.mDoubleFormat.format(XT));
                        System.out.println("==============================================================");
                        this.parent.mTableSorterFrame.hide();
                        if (this.mDisplayMode != 102) {
                            this.parent.mTableSorterFrame.setValueAt(this.mIntegerFormat.format(JOBS), 0, 0);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(TIME), 0, 1);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XATA), 0, 2);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDTA), 0, 3);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XATS), 0, 4);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDTS), 0, 5);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XART), 0, 6);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDRT), 0, 7);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(AJOB), 0, 8);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDJOB), 0, 9);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(U1), 0, 10);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(U2), 0, 11);
                            if (X >= 1.0) {
                                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(X), 0, 12);
                            } else {
                                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat7Digits.format(X), 0, 12);
                            }
                        } else {
                            this.parent.mTableSorterFrame.setValueAt(this.mIntegerFormat.format(JOBS), 0, 0);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(TIME), 0, 1);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XATA), 0, 2);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDTA), 0, 3);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XATS), 0, 4);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDTS), 0, 5);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XART), 0, 6);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDRT), 0, 7);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(AJOB), 0, 8);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDJOB), 0, 9);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(U1), 0, 10);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(U2), 0, 11);
                            if (X >= 1.0) {
                                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(X), 0, 12);
                            } else {
                                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat7Digits.format(X), 0, 12);
                            }
                            ++szIndex;
                        }
                        this.parent.mTableSorterFrame.show();
                        if (this.mDisplayMode == 102) {
                            try {
                                Thread.currentThread();
                                Thread.sleep(2000);
                            }
                            catch (InterruptedException v6) {
                            }
                            catch (Exception v7) {}
                        }
                    }
                    catch (NoSuchElementException v8) {
                    }
                    catch (ArrayIndexOutOfBoundsException v9) {}
                }
                if (!this.mStepByStepMode) continue;
                this.mRunFlag = 1 - this.mRunFlag;
                continue;
            }
            U = TBUSY / TIME;
            U1 = this.CPU[0].mTBUSY / TIME;
            U2 = this.CPU[1].mTBUSY / TIME;
            AJOB = JOBSEC / TIME;
            XSDJOB = SDJOB / TIME - AJOB * AJOB <= 0.0 ? 0.0 : Math.sqrt(SDJOB / TIME - AJOB * AJOB);
            XATS = JOBS > 0 ? ATS / (double)JOBS : 0.0;
            XSDTS = SDTS / (double)JOBS - XATS * XATS <= 0.0 ? 0.0 : Math.sqrt(SDTS / (double)JOBS - XATS * XATS);
            XATA = JOBA > 0 ? ATA / (double)JOBA : 0.0;
            XSDTA = SDTA / (double)JOBA - XATA * XATA <= 0.0 ? 0.0 : Math.sqrt(SDTA / (double)JOBA - XATA * XATA);
            XART = JOBS > 0 ? ART / (double)JOBS : 0.0;
            XSDRT = SDRT / (double)JOBS - XART * XART <= 0.0 ? 0.0 : Math.sqrt(SDRT / (double)JOBS - XART * XART);
            X = (double)JOBS / TIME;
            double XT = T / (double)JOBS;
            System.out.println(String.valueOf(this.mIntegerFormat.format(JOBS)) + "   " + this.mDoubleFormat.format(TIME) + "   " + this.mDoubleFormat.format(XATA) + "   " + this.mDoubleFormat.format(XSDTA) + "   " + this.mDoubleFormat.format(XATS) + "   " + this.mDoubleFormat.format(XSDTS) + "   " + this.mDoubleFormat.format(XART) + "   " + this.mDoubleFormat.format(XSDRT) + "   " + this.mDoubleFormat.format(AJOB) + "   " + this.mDoubleFormat.format(XSDJOB) + "   " + this.mDoubleFormat.format(U1) + "   " + this.mDoubleFormat.format(U2) + "   " + this.mDoubleFormat.format(X) + "\t" + this.mDoubleFormat.format(XT));
            System.out.println("==============================================================");
            this.parent.mTableSorterFrame.hide();
            if (this.mDisplayMode != 102) {
                this.parent.mTableSorterFrame.setValueAt(this.mIntegerFormat.format(JOBS), 0, 0);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(TIME), 0, 1);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XATA), 0, 2);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDTA), 0, 3);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XATS), 0, 4);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDTS), 0, 5);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XART), 0, 6);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDRT), 0, 7);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(AJOB), 0, 8);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDJOB), 0, 9);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(U1), 0, 10);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(U2), 0, 11);
                if (X >= 1.0) {
                    this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(X), 0, 12);
                } else {
                    this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat7Digits.format(X), 0, 12);
                }
            } else {
                this.parent.mTableSorterFrame.setValueAt(this.mIntegerFormat.format(JOBS), 0, 0);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(TIME), 0, 1);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XATA), 0, 2);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDTA), 0, 3);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XATS), 0, 4);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDTS), 0, 5);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XART), 0, 6);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDRT), 0, 7);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(AJOB), 0, 8);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDJOB), 0, 9);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(U1), 0, 10);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(U2), 0, 11);
                if (X >= 1.0) {
                    this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(X), 0, 12);
                } else {
                    this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat7Digits.format(X), 0, 12);
                }
                ++szIndex;
            }
            this.parent.mTableSorterFrame.show();
            while (this.mRunFlag == 0) {
                try {
                    Thread.currentThread();
                    Thread.sleep(100);
                    continue;
                }
                catch (InterruptedException v10) {
                    continue;
                }
                catch (Exception v11) {}
            }
        }
        this.algrthm = null;
    }

    @SuppressWarnings("deprecation")
	public void runalg(boolean fromBeginning) {
        this.mFromBeginning = fromBeginning;
        if (this.mFromBeginning) {
            this.mDisplayMode = 100;
            this.mSingleStepMode_MoveOneStep = 0;
            if (this.algrthm != null) {
                this.algrthm.stop();
            }
            this.init();
            this.start();
        }
    }

    public void start() {
        this.algrthm.start();
    }

    public void stop() {
        if (this.algrthm != null) {
            this.algrthm.suspend();
        }
        this.clear();
        this.mDisplayMode = 100;
        this.mSingleStepMode_MoveOneStep = 0;
    }

    @SuppressWarnings("deprecation")
	public final synchronized void update(Graphics g) {
        Dimension d = this.size();
        if (this.offScreenImage == null || d.width != this.offScreenSize.width || d.height != this.offScreenSize.height) {
            this.offScreenImage = this.createImage(d.width, d.height);
            this.offScreenSize = d;
            this.offScreenGraphics = this.offScreenImage.getGraphics();
        }
        this.offScreenGraphics.setColor(Color.white);
        this.offScreenGraphics.fillRect(0, 0, d.width, d.height);
        this.paint(this.offScreenGraphics);
        g.drawImage(this.offScreenImage, 0, 0, null);
    }

    class SymMouse
    extends MouseAdapter {
        SymMouse() {
        }

        public void mouseClicked(MouseEvent event) {
            Point queueLowerRight = new Point(GraphCanvas.access$0((GraphCanvas)GraphCanvas.this)[0].UpperLeftPoint.x + GraphCanvas.access$0((GraphCanvas)GraphCanvas.this)[0].height, GraphCanvas.access$0((GraphCanvas)GraphCanvas.this)[0].UpperLeftPoint.y + GraphCanvas.access$0((GraphCanvas)GraphCanvas.this)[0].height);
            Point mQueueUpperLeft = new Point(GraphCanvas.access$0((GraphCanvas)GraphCanvas.this)[GraphCanvas.this.queueLen - 1].UpperLeftPoint.x, GraphCanvas.access$0((GraphCanvas)GraphCanvas.this)[GraphCanvas.this.queueLen - 1].UpperLeftPoint.y);
            Point mQueueLowerRight = new Point(queueLowerRight.x, queueLowerRight.y);
            int click_X = event.getX();
            int click_Y = event.getY();
            if (click_X >= mQueueUpperLeft.x && click_X <= mQueueLowerRight.x && click_Y >= mQueueUpperLeft.y && click_Y <= mQueueLowerRight.y) {
                GraphCanvas.this.popup.getItem(0).setLabel("Queue");
                GraphCanvas.this.popup.show(GraphCanvas.this, click_X, click_Y);
            } else if (click_X >= GraphCanvas.access$1((GraphCanvas)GraphCanvas.this)[0].UpperLeftPoint.x && click_X <= GraphCanvas.access$1((GraphCanvas)GraphCanvas.this)[0].UpperLeftPoint.x + 2 * GraphCanvas.access$1((GraphCanvas)GraphCanvas.this)[0].radius && click_Y >= GraphCanvas.access$1((GraphCanvas)GraphCanvas.this)[0].UpperLeftPoint.y && click_Y <= GraphCanvas.access$1((GraphCanvas)GraphCanvas.this)[0].UpperLeftPoint.y + 2 * GraphCanvas.access$1((GraphCanvas)GraphCanvas.this)[0].radius) {
                GraphCanvas.this.popup.getItem(0).setLabel("Server 1");
                GraphCanvas.this.popup.show(GraphCanvas.this, click_X, click_Y);
            } else if (click_X >= GraphCanvas.access$1((GraphCanvas)GraphCanvas.this)[1].UpperLeftPoint.x && click_X <= GraphCanvas.access$1((GraphCanvas)GraphCanvas.this)[1].UpperLeftPoint.x + 2 * GraphCanvas.access$1((GraphCanvas)GraphCanvas.this)[1].radius && click_Y >= GraphCanvas.access$1((GraphCanvas)GraphCanvas.this)[1].UpperLeftPoint.y && click_Y <= GraphCanvas.access$1((GraphCanvas)GraphCanvas.this)[1].UpperLeftPoint.y + 2 * GraphCanvas.access$1((GraphCanvas)GraphCanvas.this)[1].radius) {
                GraphCanvas.this.popup.getItem(0).setLabel("Server 2");
                GraphCanvas.this.popup.show(GraphCanvas.this, click_X, click_Y);
            }
        }
    }

}
