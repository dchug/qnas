package simu4;

import java.awt.Frame;
import java.io.PrintStream;
import java.util.Vector;
import moondog.dialogs.MessageBox;
import simu4.Applet1;
import simu4.Cpu;
import simu4.GraphCanvas;
import simu4.Square;
import simu4.Task;

public class TaskQVector
extends Vector {
    public int mMaxVectorLen;
    public Cpu[] mConnectedServers;
    public double[] mProbability = null;
    public TaskQVector[] mDestTaskQVector = null;
    public Square[] mVisualQueue;
    public Applet1 mParent;
    static long[] R;
    static long xmod;
    static double rnmax;
    static int I;
    static int J;
    static int K;
    static int N;
    static int N2;
    static int firstcall;
    static double[] table;

    static {
        long[] arrl = new long[18];
        arrl[0] = 585064;
        arrl[1] = 120340;
        arrl[2] = 336032;
        arrl[3] = 360031;
        arrl[4] = 645901;
        arrl[5] = 19751;
        arrl[6] = 19746;
        arrl[7] = 60323;
        arrl[8] = 17001;
        arrl[9] = 89672;
        arrl[10] = 20304;
        arrl[11] = 12434;
        arrl[12] = 45302;
        arrl[13] = 89603;
        arrl[14] = 31234;
        arrl[15] = 68690;
        arrl[16] = 234085;
        R = arrl;
        xmod = 1049057;
        rnmax = xmod;
        I = 17;
        J = 16;
        K = 0;
        N = 18;
        N2 = 200;
        firstcall = 1;
        table = new double[N2];
    }

    TaskQVector(int maxVectorLen, Cpu[] servers) {
        this.mMaxVectorLen = maxVectorLen;
        int serverNum = servers.length;
        this.mConnectedServers = new Cpu[serverNum];
        int i = 0;
        while (i < serverNum) {
            this.mConnectedServers[i] = servers[i];
            ++i;
        }
    }

    @SuppressWarnings({ "deprecation", "unchecked" })
	public void appendToList(Task task) {
        if (this.size() >= this.mMaxVectorLen) {
            Frame createdFrame = new Frame("Setup Dialog");
            MessageBox.showError(createdFrame, "Queue Overflow Error. End simulation and initialize QNAS");
            System.out.println("class TaskQVector: Sorry, the new task cannot be appended--length max reached!");
            createdFrame.dispose();
            this.mParent.mOptionsThread.stop();
            this.mParent.graphcanvas.algrthm.stop();
            return;
        }
        super.addElement(task);
    }

    public int choice() {
        if (this.mProbability.length == 1) {
            return 0;
        }
        int I = 0;
        double U = 0.0;
        double F = this.mProbability[0];
        while (U <= 0.0) {
            U = this.rng();
        }
        while (U > F) {
            F += this.mProbability[++I];
        }
        return I;
    }

    public Task getFirstUnprocessedElement() {
        int szSize = this.size();
        if (szSize <= 0) {
            return null;
        }
        int i = 0;
        while (i < szSize) {
            if (!((Task)this.elementAt((int)i)).mIsBeingProcessed) {
                return (Task)this.elementAt(i);
            }
            ++i;
        }
        return null;
    }

    public boolean isFull() {
        if (this.size() < this.mMaxVectorLen) {
            return false;
        }
        return true;
    }

    public void removeFromList(Task task) {
        if (task == null) {
            return;
        }
        int szSize = this.size();
        int i = 0;
        while (i < szSize) {
            if (this.elementAt(i).equals(task)) {
                this.removeElementAt(i);
                return;
            }
            ++i;
        }
    }

    public double rng() {
        if (firstcall == 1) {
            int z = 0;
            while (z < N2) {
                TaskQVector.table[z] = this.urn();
                ++z;
            }
            System.out.println("In here once!");
            firstcall = 0;
        }
        int itable = (int)((double)N2 * this.urn());
        double rnumber = table[itable];
        TaskQVector.table[itable] = this.urn();
        return rnumber;
    }

    public void setJobDirection(Task task) {
        if (this.mDestTaskQVector == null) {
            task.mDestTaskQueue = null;
            return;
        }
        int szChoice = this.choice();
        task.mDestTaskQueue = this.mDestTaskQVector[szChoice];
    }

    public void setRules(double[] probability, TaskQVector[] destTaskQVector) {
        this.mProbability = new double[probability.length];
        int i = 0;
        while (i < probability.length) {
            this.mProbability[i] = probability[i];
            ++i;
        }
        this.mDestTaskQVector = destTaskQVector;
    }

    public void setVisualQueue(Square[] visualQueue) {
        this.mVisualQueue = visualQueue;
    }

    public double urn() {
        TaskQVector.R[TaskQVector.I] = (R[J] + R[K]) % xmod;
        double rn = (double)R[I] / rnmax;
        I = (I + 1) % N;
        J = (J + 1) % N;
        K = (K + 1) % N;
        return rn;
    }
}

