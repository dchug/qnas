package simu4;

import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.Label;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import simu4.Applet1;

public class ResultFrame
extends Frame
implements Runnable {
    public boolean mShouldClose;
    public boolean mBooleanCancelled;
    public Applet1 mParent;
    boolean fComponentsAdjusted = false;
    Label label0;
    Label label1;
    Label label2;
    Label label3;
    Label label4;
    Label label5;
    Label label6;
    Label label7;
    Label label8;
    Label label9;
    Label label10;
    Label label11;
    Label label12;
    TextField textField0;
    TextField textField1;
    TextField textField2;
    TextField textField3;
    TextField textField4;
    TextField textField5;
    TextField textField6;
    TextField textField7;
    TextField textField8;
    TextField textField9;
    TextField textField10;
    TextField textField11;
    TextField textField12;
    Button btnCancel;

    public ResultFrame(Applet1 parent) {
        this.setLayout(null);
        this.setVisible(false);
        this.setSize(1028, 92);
        this.setBackground(new Color(65535));
        this.label0 = new Label("JOBS", 1);
        this.label0.setBounds(0, 0, 76, 16);
        this.add(this.label0);
        this.label1 = new Label("TIME", 1);
        this.label1.setBounds(76, 0, 76, 16);
        this.add(this.label1);
        this.label2 = new Label("ATA", 1);
        this.label2.setBounds(152, 0, 76, 16);
        this.add(this.label2);
        this.label3 = new Label("SDTA", 1);
        this.label3.setBounds(228, 0, 76, 16);
        this.add(this.label3);
        this.label4 = new Label("ATS", 1);
        this.label4.setBounds(304, 0, 76, 16);
        this.add(this.label4);
        this.label5 = new Label("SDTS", 1);
        this.label5.setBounds(380, 0, 76, 16);
        this.add(this.label5);
        this.label6 = new Label("ART", 1);
        this.label6.setBounds(456, 0, 76, 16);
        this.add(this.label6);
        this.label7 = new Label("SDRT", 1);
        this.label7.setBounds(532, 0, 76, 16);
        this.add(this.label7);
        this.label8 = new Label("AJOB", 1);
        this.label8.setBounds(608, 0, 76, 16);
        this.add(this.label8);
        this.label9 = new Label("SDJOB", 1);
        this.label9.setBounds(684, 0, 76, 16);
        this.add(this.label9);
        this.label10 = new Label("U1", 1);
        this.label10.setBounds(760, 0, 76, 16);
        this.add(this.label10);
        this.label11 = new Label("U2", 1);
        this.label11.setBounds(836, 0, 76, 16);
        this.add(this.label11);
        this.label12 = new Label("X", 1);
        this.label12.setBounds(912, 0, 76, 16);
        this.add(this.label12);
        this.textField0 = new TextField();
        this.textField0.setBounds(0, 24, 76, 32);
        this.add(this.textField0);
        this.textField1 = new TextField();
        this.textField1.setBounds(76, 24, 76, 32);
        this.add(this.textField1);
        this.textField2 = new TextField();
        this.textField2.setBounds(152, 24, 76, 32);
        this.add(this.textField2);
        this.textField3 = new TextField();
        this.textField3.setBounds(228, 24, 76, 32);
        this.add(this.textField3);
        this.textField4 = new TextField();
        this.textField4.setBounds(304, 24, 76, 32);
        this.add(this.textField4);
        this.textField5 = new TextField();
        this.textField5.setBounds(380, 24, 76, 32);
        this.add(this.textField5);
        this.textField6 = new TextField();
        this.textField6.setBounds(456, 24, 76, 32);
        this.add(this.textField6);
        this.textField7 = new TextField();
        this.textField7.setBounds(532, 24, 76, 32);
        this.add(this.textField7);
        this.textField8 = new TextField();
        this.textField8.setBounds(608, 24, 76, 32);
        this.add(this.textField8);
        this.textField9 = new TextField();
        this.textField9.setBounds(684, 24, 76, 32);
        this.add(this.textField9);
        this.textField10 = new TextField();
        this.textField10.setBounds(760, 24, 76, 32);
        this.add(this.textField10);
        this.textField11 = new TextField();
        this.textField11.setBounds(836, 24, 76, 32);
        this.add(this.textField11);
        this.textField12 = new TextField();
        this.textField12.setBounds(912, 24, 76, 32);
        this.add(this.textField12);
        this.btnCancel = new Button();
        this.btnCancel.setLabel("Cancel");
        this.btnCancel.setBounds(456, 60, 96, 28);
        this.btnCancel.setBackground(new Color(12632256));
        this.add(this.btnCancel);
        this.setTitle("Results Table");
        SymWindow aSymWindow = new SymWindow();
        this.addWindowListener(aSymWindow);
        SymAction lSymAction = new SymAction();
        this.btnCancel.addActionListener(lSymAction);
        this.mParent = parent;
    }

    void Frame1_WindowClosing(WindowEvent event) {
        this.setVisible(false);
    }

    public void addNotify() {
        Dimension d = this.getSize();
        super.addNotify();
        if (this.fComponentsAdjusted) {
            return;
        }
        this.setSize(this.insets().left + this.insets().right + d.width, this.insets().top + this.insets().bottom + d.height);
        Component[] components = this.getComponents();
        int i = 0;
        while (i < components.length) {
            Point p = components[i].getLocation();
            p.translate(this.insets().left, this.insets().top);
            components[i].setLocation(p);
            ++i;
        }
        this.fComponentsAdjusted = true;
    }

    public void btnCancel_Action() {
        this.mBooleanCancelled = true;
        this.mShouldClose = true;
        this.setVisible(false);
    }

    public void resetData(String[][] szData) {
        if (szData == null) {
            return;
        }
        int i = szData.length;
        if (i == 0) {
            return;
        }
        int col = szData[0].length;
        int j = 0;
        while (j < col) {
            if (j == 0) {
                this.textField0.setText(szData[0][j]);
            } else if (j == 1) {
                this.textField1.setText(szData[0][j]);
            } else if (j == 2) {
                this.textField2.setText(szData[0][j]);
            } else if (j == 3) {
                this.textField3.setText(szData[0][j]);
            } else if (j == 4) {
                this.textField4.setText(szData[0][j]);
            } else if (j == 5) {
                this.textField5.setText(szData[0][j]);
            } else if (j == 6) {
                this.textField6.setText(szData[0][j]);
            } else if (j == 7) {
                this.textField7.setText(szData[0][j]);
            } else if (j == 8) {
                this.textField8.setText(szData[0][j]);
            } else if (j == 9) {
                this.textField9.setText(szData[0][j]);
            } else if (j == 10) {
                this.textField10.setText(szData[0][j]);
            } else if (j == 11) {
                this.textField11.setText(szData[0][j]);
            } else if (j == 12) {
                this.textField12.setText(szData[0][j]);
            }
            ++j;
        }
    }

    public void run() {
        do {
            try {
                Thread.currentThread();
                Thread.sleep(15000);
            }
            catch (InterruptedException v0) {
            }
            catch (Exception v1) {}
            if (this.mParent != null) continue;
            this.setVisible(false);
            this.dispose();
        } while (true);
    }

    public void setValueAt(String szData, int row, int j) {
        if (szData == null) {
            szData = "";
        }
        if (j == 0) {
            this.textField0.setText(szData);
        } else if (j == 1) {
            this.textField1.setText(szData);
        } else if (j == 2) {
            this.textField2.setText(szData);
        } else if (j == 3) {
            this.textField3.setText(szData);
        } else if (j == 4) {
            this.textField4.setText(szData);
        } else if (j == 5) {
            this.textField5.setText(szData);
        } else if (j == 6) {
            this.textField6.setText(szData);
        } else if (j == 7) {
            this.textField7.setText(szData);
        } else if (j == 8) {
            this.textField8.setText(szData);
        } else if (j == 9) {
            this.textField9.setText(szData);
        } else if (j == 10) {
            this.textField10.setText(szData);
        } else if (j == 11) {
            this.textField11.setText(szData);
        } else if (j == 12) {
            this.textField12.setText(szData);
        }
    }

    public void setVisible(boolean b) {
        if (b) {
            this.setLocation(0, 0);
        }
        super.setVisible(b);
    }

    class SymWindow
    extends WindowAdapter {
        SymWindow() {
        }

        public void windowClosing(WindowEvent event) {
            Object object = event.getSource();
            if (object == ResultFrame.this) {
                ResultFrame.this.Frame1_WindowClosing(event);
            }
        }
    }

    class SymAction
    implements ActionListener {
        SymAction() {
        }

        public void actionPerformed(ActionEvent event) {
            Object object = event.getSource();
            if (object == ResultFrame.this.btnCancel) {
                ResultFrame.this.btnCancel_Action();
            }
        }
    }

}