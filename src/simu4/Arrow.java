package simu4;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

public class Arrow {
    public Point start;
    public Point end;
    public float dir_x;
    public float dir_y;
    public int x1;
    public int x2;
    public int x3;
    public int y1;
    public int y2;
    public int y3;

    public Arrow(int s_x, int s_y, int e_x, int e_y) {
        this.start = new Point(s_x, s_y);
        this.end = new Point(e_x, e_y);
        int dx = e_x - s_x;
        int dy = e_y - s_y;
        float l = (float)Math.sqrt(dx * dx + dy * dy);
        this.dir_x = (float)dx / l;
        this.dir_y = (float)dy / l;
        this.x1 = (int)((float)this.end.x - 3.0f * this.dir_x + 6.0f * this.dir_y);
        this.x2 = (int)((float)this.end.x - 3.0f * this.dir_x - 6.0f * this.dir_y);
        this.x3 = (int)((float)this.end.x + 6.0f * this.dir_x);
        this.y1 = (int)((float)this.end.y - 3.0f * this.dir_y - 6.0f * this.dir_x);
        this.y2 = (int)((float)this.end.y - 3.0f * this.dir_y + 6.0f * this.dir_x);
        this.y3 = (int)((float)this.end.y + 6.0f * this.dir_y);
    }

    public Arrow(Point s, Point e) {
        this.start = new Point(s.x, s.y);
        this.end = new Point(e.x, e.y);
        int dx = this.end.x - this.start.x;
        int dy = this.end.y - this.start.y;
        float l = (float)Math.sqrt(dx * dx + dy * dy);
        this.dir_x = (float)dx / l;
        this.dir_y = (float)dy / l;
        this.x1 = (int)((float)this.end.x - 3.0f * this.dir_x + 6.0f * this.dir_y);
        this.x2 = (int)((float)this.end.x - 3.0f * this.dir_x - 6.0f * this.dir_y);
        this.x3 = (int)((float)this.end.x + 6.0f * this.dir_x);
        this.y1 = (int)((float)this.end.y - 3.0f * this.dir_y - 6.0f * this.dir_x);
        this.y2 = (int)((float)this.end.y - 3.0f * this.dir_y + 6.0f * this.dir_x);
        this.y3 = (int)((float)this.end.y + 6.0f * this.dir_y);
    }

    public void drawArrow(Graphics g) {
        int[] arrowhead_x = new int[]{this.x1, this.x2, this.x3, this.x1};
        int[] arrowhead_y = new int[]{this.y1, this.y2, this.y3, this.y1};
        g.setColor(Color.black);
        g.drawLine(this.start.x, this.start.y, this.end.x, this.end.y);
        g.fillPolygon(arrowhead_x, arrowhead_y, 4);
    }

    public float getDirX() {
        return this.dir_x;
    }

    public float getDirY() {
        return this.dir_y;
    }

    public Point getEnd() {
        return this.end;
    }

    public int getRealHead_X() {
        return this.x3;
    }

    public int getRealHead_Y() {
        return this.y3;
    }

    public Point getStart() {
        return this.start;
    }

    public void setDirX(float DX) {
        this.dir_x = DX;
    }

    public void setDirY(float DY) {
        this.dir_y = DY;
    }

    public void setEnd(int X, int Y) {
        this.end.x = X;
        this.end.y = Y;
    }

    public void setEnd(Point e) {
        this.end.x = e.x;
        this.end.y = e.y;
    }

    public void setStart(int X, int Y) {
        this.start.x = X;
        this.start.y = Y;
    }

    public void setStart(Point s) {
        this.start.x = s.x;
        this.start.y = s.y;
    }
}
