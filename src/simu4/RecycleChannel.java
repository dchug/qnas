package simu4;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import simu4.GraphCanvas;
import simu4.Node;
import simu4.Task;
import simu4.TaskQVector;

class RecycleChannel
extends Canvas {
    public Task[] mCurrentTask;
    public Point[] mPoint = new Point[4];
    public Point[] mShowPoint;
    private int mShowPointNumber = 3;
    final int mPointNumber = 4;
    static int mTaskPosIndex = 0;
    public boolean mNoMoreJobComing = false;
    public TaskQVector mDestinationTaskQVector;
    public String mName;
    public GraphCanvas mParent;

    public RecycleChannel(Point p1, Point p2, Point p3, Point p4, TaskQVector destinationTaskQVector) {
        int i = 0;
        while (i < 4) {
            if (i == 0) {
                this.mPoint[i] = new Point(p1.x, p1.y);
            }
            if (i == 1) {
                this.mPoint[i] = new Point(p2.x, p2.y);
            }
            if (i == 2) {
                this.mPoint[i] = new Point(p3.x, p3.y);
            }
            if (i == 3) {
                this.mPoint[i] = new Point(p4.x, p4.y);
            }
            ++i;
        }
        this.setShowPoints();
        this.mCurrentTask = new Task[3];
        int i2 = 0;
        while (i2 < 3) {
            this.mCurrentTask[i2] = null;
            ++i2;
        }
        this.mDestinationTaskQVector = destinationTaskQVector;
    }

    public void clearAllTasks() {
        int i = 0;
        while (i < this.mShowPointNumber) {
            this.mCurrentTask[i] = null;
            ++i;
        }
    }

    public void drawChannel(Graphics g) {
        this.drawRecycleChannel(g);
    }

    public void drawRecycleChannel(Graphics g) {
        g.setColor(Color.black);
        g.drawLine(this.mPoint[0].x, this.mPoint[0].y, this.mPoint[1].x, this.mPoint[1].y);
        g.drawLine(this.mPoint[1].x, this.mPoint[1].y, this.mPoint[2].x, this.mPoint[2].y);
        g.drawLine(this.mPoint[2].x, this.mPoint[2].y, this.mPoint[3].x, this.mPoint[3].y);
        int i = 0;
        while (i < this.mShowPointNumber) {
            if (this.mCurrentTask[i] != null) {
                this.mCurrentTask[i].drawTask(g);
            }
            ++i;
        }
    }

    public boolean hasJobInChannel() {
        int i = 0;
        while (i < this.mShowPointNumber) {
            if (this.mCurrentTask[i] != null) {
                return true;
            }
            ++i;
        }
        return false;
    }

    public void movePreviousJobsForward() {
        if (this.mCurrentTask[0] != null && this.mDestinationTaskQVector != null) {
            this.mDestinationTaskQVector.appendToList(this.mCurrentTask[0]);
        }
        int i = 0;
        while (i < this.mShowPointNumber - 1) {
            this.mCurrentTask[i] = this.mCurrentTask[i + 1] != null ? new Task(this.mShowPoint[this.mShowPointNumber - 1 - i].x, this.mShowPoint[this.mShowPointNumber - 1 - i].y, this.mCurrentTask[i + 1].radius, this.mCurrentTask[i + 1].color, this.mCurrentTask[i + 1].mServiceTime, this.mCurrentTask[i + 1].mInterArrivalTime) : null;
            ++i;
        }
    }

    public void paint(Graphics g) {
        this.drawRecycleChannel(g);
    }

    public void recycleTask(Task task) {
        this.setCurrentTask(task);
        this.mParent.repaint();
        int i = 0;
        while (i < this.mShowPointNumber) {
            try {
                Thread.currentThread();
                Thread.sleep(30);
            }
            catch (InterruptedException v0) {
            }
            catch (Exception v1) {}
            this.setCurrentTask(null);
            this.mParent.repaint();
            ++i;
        }
    }

    public void setCurrentTask(Task task) {
        this.movePreviousJobsForward();
        if (task == null) {
            this.mCurrentTask[this.mShowPointNumber - 1] = null;
            return;
        }
        this.mCurrentTask[this.mShowPointNumber - 1] = new Task(this.mShowPoint[0].x, this.mShowPoint[0].y, task.radius, task.color, task.mServiceTime, task.mInterArrivalTime);
    }

    public void setName(String name) {
        this.mName = name;
    }

    public void setParent(GraphCanvas parent) {
        this.mParent = parent;
        this.setShowPoints();
    }

    public void setShowPoints() {
        int radius = new Task().radius;
        this.mShowPoint = new Point[3];
        int i = 0;
        while (i < 3) {
            if (i == 0) {
                this.mShowPoint[i] = new Point((this.mPoint[0].x + this.mPoint[1].x) / 2 - radius, (this.mPoint[0].y + this.mPoint[1].y) / 2 - radius);
            }
            if (i == 1) {
                this.mShowPoint[i] = new Point((this.mPoint[1].x + this.mPoint[2].x) / 2 - radius, (this.mPoint[1].y + this.mPoint[2].y) / 2 - radius);
            }
            if (i == 2) {
                this.mShowPoint[i] = new Point((this.mPoint[2].x + this.mPoint[3].x) / 2 - radius, (this.mPoint[2].y + this.mPoint[3].y) / 2 - radius);
            }
            ++i;
        }
    }

    public void setShowPoints(Point[] point) {
        int radius = new Task().radius;
        this.mShowPointNumber = point.length;
        this.mShowPoint = new Point[this.mShowPointNumber];
        int i = 0;
        while (i < this.mShowPointNumber) {
            this.mShowPoint[i] = new Point(point[i].x - radius, point[i].y - radius);
            ++i;
        }
        this.mCurrentTask = null;
        this.mCurrentTask = new Task[this.mShowPointNumber];
        int i2 = 0;
        while (i2 < this.mShowPointNumber) {
            this.mCurrentTask[i2] = null;
            ++i2;
        }
    }
}