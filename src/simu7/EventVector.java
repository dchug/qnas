/*
 * Decompiled with CFR 0_115.
 */
package simu7;

import java.util.Vector;
import simu7.Task;

public class EventVector
extends Vector {
    static final int FOLLOW = 1;
    static final int PRECEDE = 2;

    EventVector() {
    }

    void insertTaskByEventTime(Task task) {
        if (task == null) {
            return;
        }
        if (this.size() <= 0) {
            this.addElement(task);
            return;
        }
        int szSize = this.size();
        int i = 0;
        while (i < szSize) {
            if (((Task)this.elementAt((int)i)).mEventTime >= task.mEventTime) {
                this.insertElementAt(task, i);
                return;
            }
            ++i;
        }
        this.addElement(task);
    }

    void insertTaskByRemainingInterarrivalTime(Task task) {
        if (task == null) {
            return;
        }
        if (this.size() <= 0) {
            this.addElement(task);
            return;
        }
        int szSize = this.size();
        int i = 0;
        while (i < szSize) {
            if (((Task)this.elementAt((int)i)).mRemainingInterArrivalTime >= task.mRemainingInterArrivalTime) {
                this.insertElementAt(task, i);
                return;
            }
            ++i;
        }
        this.addElement(task);
    }
}

