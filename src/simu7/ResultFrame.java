/*
 * Decompiled with CFR 0_115.
 */
package simu7;

import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.Label;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import simu7.Applet1;

public class ResultFrame
extends Frame
implements Runnable {
    public boolean mShouldClose;
    public int mCpuNumber;
    public int mActiveDiskNumber;
    public boolean mBooleanCancelled;
    boolean fComponentsAdjusted = false;
    Label label1;
    Label label2;
    Label label3;
    Label label4;
    Label label5;
    Label label6;
    Label label7;
    Label label8;
    Label label9;
    Label label10;
    Label label11;
    Label label12;
    Label label13;
    Label label14;
    Label label15;
    Label label16;
    Label label17;
    Label label18;
    Label label19;
    Label label20;
    Label label21;
    public TextField mJobNumber;
    public TextField mSystemR;
    public TextField mTime;
    public TextField mSystemX;
    public TextField mSystemN;
    public TextField[] mServerAvgU;
    public TextField[] mServerAvgQLen;
    public TextField[] mServerAvgResponse;
    public Button btnCancel;
    public Applet1 mParent;

    public ResultFrame(boolean isCentralServerModel, int activeDiskNumber, Applet1 parent) {
        this.setLayout(null);
        this.setVisible(false);
        this.setSize(700, 550);
        this.setTitle("result");
        this.setBackground(Color.lightGray);
        this.mShouldClose = false;
        this.mCpuNumber = 10;
        this.mActiveDiskNumber = activeDiskNumber;
        this.mServerAvgU = new TextField[this.mCpuNumber];
        this.mServerAvgQLen = new TextField[this.mCpuNumber];
        this.mServerAvgResponse = new TextField[this.mCpuNumber];
        this.label1 = new Label("System");
        this.label1.setBounds(36, 0, 84, 24);
        this.add(this.label1);
        this.label2 = new Label("JOBS", 1);
        this.label2.setBounds(120, 0, 144, 24);
        this.add(this.label2);
        this.label3 = new Label("TIME", 1);
        this.label3.setBounds(264, 0, 132, 24);
        this.add(this.label3);
        this.label4 = new Label("R", 1);
        this.label4.setBounds(396, 0, 96, 24);
        this.add(this.label4);
        this.label5 = new Label("X", 1);
        this.label5.setBounds(492, 0, 84, 24);
        this.add(this.label5);
        this.label6 = new Label("N(=XR)", 1);
        this.label6.setBounds(576, 0, 84, 25);
        this.add(this.label6);
        this.label7 = new Label("Whole System");
        this.label7.setBounds(36, 24, 84, 24);
        this.add(this.label7);
        this.mJobNumber = new TextField();
        this.mJobNumber.setBounds(120, 24, 144, 32);
        this.add(this.mJobNumber);
        this.mSystemR = new TextField();
        this.mSystemR.setBounds(396, 24, 96, 32);
        this.add(this.mSystemR);
        this.mTime = new TextField();
        this.mTime.setBounds(264, 24, 132, 32);
        this.add(this.mTime);
        this.mSystemX = new TextField();
        this.mSystemX.setBounds(492, 24, 84, 32);
        this.add(this.mSystemX);
        this.mSystemN = new TextField();
        this.mSystemN.setBounds(576, 24, 84, 32);
        this.add(this.mSystemN);
        this.label8 = new Label("Server");
        this.label8.setBounds(36, 60, 48, 24);
        this.add(this.label8);
        this.label9 = new Label("Average Utilization", 1);
        this.label9.setBounds(96, 60, 120, 24);
        this.add(this.label9);
        this.label10 = new Label("Average Queue Length", 1);
        this.label10.setBounds(240, 60, 132, 24);
        this.add(this.label10);
        this.label11 = new Label("Average Response Time", 1);
        this.label11.setBounds(384, 60, 144, 24);
        this.add(this.label11);
        this.label12 = new Label("Cpu:");
        this.label12.setBounds(36, 84, 53, 25);
        this.add(this.label12);
        this.mServerAvgU[0] = new TextField();
        this.mServerAvgU[0].setBounds(96, 84, 127, 32);
        this.add(this.mServerAvgU[0]);
        this.mServerAvgQLen[0] = new TextField();
        this.mServerAvgQLen[0].setBounds(240, 84, 127, 32);
        this.add(this.mServerAvgQLen[0]);
        this.mServerAvgResponse[0] = new TextField();
        this.mServerAvgResponse[0].setBounds(384, 84, 127, 32);
        this.add(this.mServerAvgResponse[0]);
        this.label13 = new Label("Channel:");
        this.label13.setBounds(36, 120, 53, 25);
        this.add(this.label13);
        this.mServerAvgU[1] = new TextField();
        this.mServerAvgU[1].setBounds(96, 120, 127, 32);
        this.add(this.mServerAvgU[1]);
        this.mServerAvgQLen[1] = new TextField();
        this.mServerAvgQLen[1].setBounds(240, 120, 127, 32);
        this.add(this.mServerAvgQLen[1]);
        this.mServerAvgResponse[1] = new TextField();
        this.mServerAvgResponse[1].setBounds(384, 120, 127, 32);
        this.add(this.mServerAvgResponse[1]);
        this.label14 = new Label("Disk1:");
        this.label14.setBounds(36, 156, 53, 25);
        this.add(this.label14);
        this.mServerAvgU[2] = new TextField();
        this.mServerAvgU[2].setBounds(96, 156, 127, 32);
        this.add(this.mServerAvgU[2]);
        this.mServerAvgQLen[2] = new TextField();
        this.mServerAvgQLen[2].setBounds(240, 156, 127, 32);
        this.add(this.mServerAvgQLen[2]);
        this.mServerAvgResponse[2] = new TextField();
        this.mServerAvgResponse[2].setBounds(384, 156, 127, 32);
        this.add(this.mServerAvgResponse[2]);
        this.label15 = new Label("Disk2:");
        this.label15.setBounds(36, 192, 53, 25);
        this.add(this.label15);
        this.mServerAvgQLen[3] = new TextField();
        this.mServerAvgQLen[3].setBounds(240, 192, 127, 32);
        this.add(this.mServerAvgQLen[3]);
        this.mServerAvgResponse[3] = new TextField();
        this.mServerAvgResponse[3].setBounds(384, 192, 127, 32);
        this.add(this.mServerAvgResponse[3]);
        this.mServerAvgU[3] = new TextField();
        this.mServerAvgU[3].setBounds(96, 192, 127, 32);
        this.add(this.mServerAvgU[3]);
        this.label16 = new Label("Disk3:");
        this.label16.setBounds(36, 228, 53, 25);
        this.add(this.label16);
        this.mServerAvgQLen[4] = new TextField();
        this.mServerAvgQLen[4].setBounds(240, 228, 127, 32);
        this.add(this.mServerAvgQLen[4]);
        this.mServerAvgResponse[4] = new TextField();
        this.mServerAvgResponse[4].setBounds(384, 228, 127, 32);
        this.add(this.mServerAvgResponse[4]);
        this.mServerAvgU[4] = new TextField();
        this.mServerAvgU[4].setBounds(96, 228, 127, 32);
        this.add(this.mServerAvgU[4]);
        this.label17 = new Label("Disk4:");
        this.label17.setBounds(36, 264, 53, 25);
        this.add(this.label17);
        this.mServerAvgQLen[5] = new TextField();
        this.mServerAvgQLen[5].setBounds(240, 264, 127, 32);
        this.add(this.mServerAvgQLen[5]);
        this.mServerAvgResponse[5] = new TextField();
        this.mServerAvgResponse[5].setBounds(384, 264, 127, 32);
        this.add(this.mServerAvgResponse[5]);
        this.mServerAvgU[5] = new TextField();
        this.mServerAvgU[5].setBounds(96, 264, 127, 32);
        this.add(this.mServerAvgU[5]);
        this.label18 = new Label("Disk5:");
        this.label18.setBounds(36, 300, 53, 25);
        this.add(this.label18);
        this.mServerAvgQLen[6] = new TextField();
        this.mServerAvgQLen[6].setBounds(240, 300, 127, 32);
        this.add(this.mServerAvgQLen[6]);
        this.mServerAvgResponse[6] = new TextField();
        this.mServerAvgResponse[6].setBounds(384, 300, 127, 32);
        this.add(this.mServerAvgResponse[6]);
        this.mServerAvgU[6] = new TextField();
        this.mServerAvgU[6].setBounds(96, 300, 127, 32);
        this.add(this.mServerAvgU[6]);
        this.label19 = new Label("Disk6:");
        this.label19.setBounds(36, 336, 53, 25);
        this.add(this.label19);
        this.mServerAvgQLen[7] = new TextField();
        this.mServerAvgQLen[7].setBounds(240, 336, 127, 32);
        this.add(this.mServerAvgQLen[7]);
        this.mServerAvgResponse[7] = new TextField();
        this.mServerAvgResponse[7].setBounds(384, 336, 127, 32);
        this.add(this.mServerAvgResponse[7]);
        this.mServerAvgU[7] = new TextField();
        this.mServerAvgU[7].setBounds(96, 336, 127, 32);
        this.add(this.mServerAvgU[7]);
        this.label20 = new Label("Disk7:");
        this.label20.setBounds(36, 372, 53, 25);
        this.add(this.label20);
        this.mServerAvgU[8] = new TextField();
        this.mServerAvgU[8].setBounds(96, 372, 127, 32);
        this.add(this.mServerAvgU[8]);
        this.mServerAvgQLen[8] = new TextField();
        this.mServerAvgQLen[8].setBounds(240, 372, 127, 32);
        this.add(this.mServerAvgQLen[8]);
        this.mServerAvgResponse[8] = new TextField();
        this.mServerAvgResponse[8].setBounds(384, 372, 127, 32);
        this.add(this.mServerAvgResponse[8]);
        this.label21 = new Label("Disk8:");
        this.label21.setBounds(36, 408, 53, 25);
        this.add(this.label21);
        this.mServerAvgU[9] = new TextField();
        this.mServerAvgU[9].setBounds(96, 408, 127, 32);
        this.add(this.mServerAvgU[9]);
        this.mServerAvgQLen[9] = new TextField();
        this.mServerAvgQLen[9].setBounds(240, 408, 127, 32);
        this.add(this.mServerAvgQLen[9]);
        this.mServerAvgResponse[9] = new TextField();
        this.mServerAvgResponse[9].setBounds(384, 408, 127, 32);
        this.add(this.mServerAvgResponse[9]);
        this.btnCancel = new Button();
        this.btnCancel.setLabel("Cancel");
        this.btnCancel.setBounds(260, 456, 140, 25);
        this.btnCancel.setBackground(new Color(12632256));
        this.add(this.btnCancel);
        this.enableDiskEntries(activeDiskNumber);
        SymAction lSymAction = new SymAction();
        this.btnCancel.addActionListener(lSymAction);
        SymWindow aSymWindow = new SymWindow();
        this.addWindowListener(aSymWindow);
        this.mParent = parent;
    }

    void Frame1_WindowClosing(WindowEvent event) {
        this.mBooleanCancelled = true;
        this.mShouldClose = true;
        this.setVisible(false);
    }

    public void addNotify() {
        Dimension d = this.getSize();
        super.addNotify();
        if (this.fComponentsAdjusted) {
            return;
        }
        Component[] components = this.getComponents();
        int i = 0;
        while (i < components.length) {
            Point p = components[i].getLocation();
            p.translate(this.insets().left, this.insets().top);
            components[i].setLocation(p);
            ++i;
        }
        this.fComponentsAdjusted = true;
    }

    public void btnCancel_Action() {
        this.mBooleanCancelled = true;
        this.mShouldClose = true;
        this.setVisible(false);
    }

    public void enableDiskEntries(int diskNumberToBeEnabled) {
        int i;
        this.mServerAvgU[0].setEnabled(true);
        this.mServerAvgU[0].setEditable(true);
        this.mServerAvgQLen[0].setEnabled(true);
        this.mServerAvgQLen[0].setEditable(true);
        this.mServerAvgResponse[0].setEnabled(true);
        this.mServerAvgResponse[0].setEditable(true);
        this.mServerAvgU[1].setEnabled(true);
        this.mServerAvgU[1].setEditable(true);
        this.mServerAvgQLen[1].setEnabled(true);
        this.mServerAvgQLen[1].setEditable(true);
        this.mServerAvgResponse[1].setEnabled(true);
        this.mServerAvgResponse[1].setEditable(true);
        this.mServerAvgU[2].setEnabled(true);
        this.mServerAvgU[2].setEditable(true);
        this.mServerAvgQLen[2].setEnabled(true);
        this.mServerAvgQLen[2].setEditable(true);
        this.mServerAvgResponse[2].setEnabled(true);
        this.mServerAvgResponse[2].setEditable(true);
        int start = 2;
        if (diskNumberToBeEnabled > 1) {
            i = 1;
            while (i < diskNumberToBeEnabled) {
                this.mServerAvgU[i + start].setEnabled(true);
                this.mServerAvgU[i + start].setEditable(true);
                this.mServerAvgQLen[i + start].setEnabled(true);
                this.mServerAvgQLen[i + start].setEditable(true);
                this.mServerAvgResponse[i + start].setEnabled(true);
                this.mServerAvgResponse[i + start].setEditable(true);
                ++i;
            }
        }
        if (diskNumberToBeEnabled < 8) {
            i = diskNumberToBeEnabled;
            while (i < 8) {
                this.mServerAvgU[i + start].setEnabled(false);
                this.mServerAvgU[i + start].setEditable(false);
                this.mServerAvgQLen[i + start].setEnabled(false);
                this.mServerAvgQLen[i + start].setEditable(false);
                this.mServerAvgResponse[i + start].setEnabled(false);
                this.mServerAvgResponse[i + start].setEditable(false);
                ++i;
            }
        }
    }

    public void initResultDialog() {
        this.mJobNumber.setText("0.0");
        this.mSystemR.setText("0.0");
        this.mTime.setText("0.0");
        this.mSystemX.setText("0.0");
        this.mSystemN.setText("0.0");
        int i = 0;
        while (i < this.mCpuNumber) {
            this.mServerAvgU[i].setText("0.0");
            this.mServerAvgQLen[i].setText("0.0");
            this.mServerAvgResponse[i].setText("0.0");
            ++i;
        }
    }

    public void run() {
        do {
            try {
                Thread.currentThread();
                Thread.sleep(60000);
            }
            catch (InterruptedException v0) {
            }
            catch (Exception v1) {}
            if (this.mParent != null) continue;
            this.setVisible(false);
            this.dispose();
        } while (true);
    }

    public void setVisible(boolean b) {
        if (b) {
            this.setLocation(50, 50);
        }
        super.setVisible(b);
    }

    class SymWindow
    extends WindowAdapter {
        SymWindow() {
        }

        public void windowClosing(WindowEvent event) {
            Object object = event.getSource();
            if (object == ResultFrame.this) {
                ResultFrame.this.Frame1_WindowClosing(event);
            }
        }
    }

    class SymAction
    implements ActionListener {
        SymAction() {
        }

        public void actionPerformed(ActionEvent event) {
            Object object = event.getSource();
            if (object == ResultFrame.this.btnCancel) {
                ResultFrame.this.btnCancel_Action();
            }
        }
    }

}

