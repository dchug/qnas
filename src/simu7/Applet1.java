/*
 * Decompiled with CFR 0_115.
 */
package simu7;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Insets;
import java.awt.Label;
import java.awt.LayoutManager;
import java.awt.TextArea;
import java.text.NumberFormat;
import kcmdg.sntp.InfoStorage;
import simu7.GraphCanvas;
import simu7.HorizontalOptions;
import simu7.Options;
import simu7.ResultFrame;

public class Applet1
extends Applet {
    public GraphCanvas graphcanvas;
    public Options options;
    public HorizontalOptions hOptions;
    public TextArea mTextAreaResults;
    public InfoStorage mInfoStorage;
    public Label mLabel;
    public Thread mOptionsThread;
    public Thread mResultTableThread;
    public ResultFrame mResultDialog = null;

    public void init() {
        this.mInfoStorage = new InfoStorage();
        this.mInfoStorage.mJPRINT = 5;
        this.mInfoStorage.mNumJobs = 1000;
        this.mInfoStorage.mSlowModelAvgServTime = 100;
        NumberFormat mDoubleFormat = NumberFormat.getNumberInstance();
        mDoubleFormat.setMaximumFractionDigits(3);
        mDoubleFormat.setMinimumFractionDigits(3);
        mDoubleFormat.setMaximumIntegerDigits(10);
        NumberFormat mIntegerFormat = NumberFormat.getNumberInstance();
        mIntegerFormat.setParseIntegerOnly(true);
        this.graphcanvas = new GraphCanvas(this.mInfoStorage, this);
        this.graphcanvas.init();
        this.options = new Options(this.mInfoStorage, this, 1);
        this.mOptionsThread = new Thread(this.options);
        this.mOptionsThread.start();
        this.hOptions = new HorizontalOptions(this.mInfoStorage, this);
        this.setLayout(new BorderLayout(10, 10));
        this.resize(688, 430);
        this.add("North", new Label("Central Server Model"));
        this.add("Center", this.graphcanvas);
        this.add("East", this.options);
        this.add("South", this.hOptions);
    }

    public Insets insets() {
        return new Insets(10, 10, 10, 10);
    }
}

