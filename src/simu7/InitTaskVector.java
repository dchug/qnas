/*
 * Decompiled with CFR 0_115.
 */
package simu7;

import java.io.PrintStream;
import kcmdg.sntp.InfoStorage;
import simu7.GraphCanvas;
import simu7.Square;
import simu7.Task;
import simu7.TaskQVector;

public class InitTaskVector {
    public TaskQVector mTasksInQueue;
    public int mJobNo;
    private boolean noWaitTask = false;
    Thread activeTaskListProcess;
    private GraphCanvas mParent;
    private InfoStorage mInfoStorage;
    static long[] R;
    static long xmod;
    static double rnmax;
    static int I;
    static int J;
    static int K;
    static int N;
    static int N2;
    static int firstcall;
    static double[] table;
    public Square[] mSquare = null;

    static {
        long[] arrl = new long[18];
        arrl[0] = 585064;
        arrl[1] = 120340;
        arrl[2] = 336032;
        arrl[3] = 360031;
        arrl[4] = 645901;
        arrl[5] = 19751;
        arrl[6] = 19746;
        arrl[7] = 60323;
        arrl[8] = 17001;
        arrl[9] = 89672;
        arrl[10] = 20304;
        arrl[11] = 12434;
        arrl[12] = 45302;
        arrl[13] = 89603;
        arrl[14] = 31234;
        arrl[15] = 68690;
        arrl[16] = 234085;
        R = arrl;
        xmod = 1049057;
        rnmax = xmod;
        I = 17;
        J = 16;
        K = 0;
        N = 18;
        N2 = 200;
        firstcall = 1;
        table = new double[N2];
    }

    public InitTaskVector(GraphCanvas parent, InfoStorage infoStorage) {
        this.mParent = parent;
        this.mInfoStorage = infoStorage;
    }

    public Task createNewColorTask() {
        Task newTask = new Task(0.0, 0.0);
        newTask.setRandomColor();
        return newTask;
    }

    public Task createNewTask() {
        int szArrivalDistribution = this.mInfoStorage.mArrivalDistribution;
        double mAvgArrivalTime = this.mInfoStorage.mAvgArrivalA;
        double szArriv_time = 0.0;
        while (szArriv_time <= 0.0) {
            szArriv_time = this.rng();
        }
        double arriv_time = szArrivalDistribution == 2 ? szArriv_time * (2.0 * mAvgArrivalTime) : -1.0 * mAvgArrivalTime * Math.log(szArriv_time);
        Task newTask = new Task(0.0, arriv_time);
        newTask.setRandomColor();
        return newTask;
    }

    public double rng() {
        if (firstcall == 1) {
            int z = 0;
            while (z < N2) {
                InitTaskVector.table[z] = this.urn();
                ++z;
            }
            System.out.println("In here once!");
            firstcall = 0;
        }
        int itable = (int)((double)N2 * this.urn());
        double rnumber = table[itable];
        InitTaskVector.table[itable] = this.urn();
        return rnumber;
    }

    private double urn() {
        InitTaskVector.R[InitTaskVector.I] = (R[J] + R[K]) % xmod;
        double rn = (double)R[I] / rnmax;
        I = (I + 1) % N;
        J = (J + 1) % N;
        K = (K + 1) % N;
        return rn;
    }
}

