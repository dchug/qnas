/*
 * Decompiled with CFR 0_115.
 */
package simu7;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.Point;
import java.awt.PopupMenu;
import java.awt.TextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.ImageObserver;
import java.io.PrintStream;
import java.text.NumberFormat;
import java.util.NoSuchElementException;
import kcmdg.sntp.InfoStorage;
import simu7.Applet1;
import simu7.Arrow;
import simu7.Cpu;
import simu7.EventVector;
import simu7.InitTaskVector;
import simu7.Node;
import simu7.RecycleChannel;
import simu7.ResultFrame;
import simu7.SplitRecycleChannel;
import simu7.Square;
import simu7.Task;
import simu7.TaskQVector;

public class GraphCanvas
extends Canvas
implements Runnable {
    int mW;
    Color[] mColor = new Color[]{Color.yellow, Color.black, Color.pink, Color.blue, Color.green, Color.red, Color.gray, Color.cyan};
    final int MAXNODES = 20;
    final int MAXQUEUELEN = 20;
    public int JOB_NO;
    final int QSIZE = 500;
    final double mFasterCoefficient = 0.8;
    final double mSlowerCoefficient = 1.2;
    int queueLen;
    int arrowNumber;
    int cpuNumber;
    int channelNumber;
    int queueNumber;
    int mDiskNumber;
    public int mTaskQVectorNum;
    double[] mProbability;
    public Cpu[] CPU;
    public Cpu mChannel;
    public Cpu[] mDisk;
    public Cpu[] mServerPointers;
    public Square[] mCpuQueue;
    public Square[] mChannelQueue;
    public Square[][] mDiskQueue;
    public TaskQVector[] mTaskQVector;
    public Arrow[] arrow;
    public RecycleChannel mLeftRecycleChannel;
    public RecycleChannel mMidRecycleChannel;
    public SplitRecycleChannel mRightRecycleChannel;
    public int szHorizontalLineLength;
    private InitTaskVector mTasks;
    public EventVector mEventVector;
    public int mRunFlag = 0;
    static final int WAIT = 0;
    static final int RUN = 1;
    public int mDisplayMode;
    public int mSingleStepMode_MoveOneStep = 0;
    static final int NORMAL_DISPLAY_MODE = 100;
    static final int SINGLE_STEP_DISPLAY_MODE = 101;
    static final int MAX_SPEED_DISPLAY_MODE = 102;
    public double mZoomDelta = 1.0;
    public NumberFormat mDoubleFormat;
    public NumberFormat mIntegerFormat;
    public NumberFormat mDoubleFormat7Digits;
    public boolean mFromBeginning;
    Font roman = new Font("TimesRoman", 1, 12);
    Font helvetica = new Font("Helvetica", 1, 15);
    FontMetrics fmetrics;
    int h;
    private Image offScreenImage;
    private Graphics offScreenGraphics;
    private Dimension offScreenSize;
    Thread algrthm;
    public boolean mStepByStepMode;
    Applet1 parent;
    InfoStorage mInfoStorage;
    PopupMenu popup;
    MenuItem mCpuMenuItem;
    public double[] mServerAvgU;
    public double[] mServerAvgQLen;
    public double[] mServerAvgResponse;
    public double mSystemR;
    public double mSystemX;
    public double mSystemN;
    public double mTime;
    public long mJobNumber;
    static long[] r;
    static long xmod;
    static double rnmax;
    static int i;
    static int j;
    static int k;
    static int n;
    static int n2;
    static int firstcall;
    static double[] table;

    static {
        long[] arrl = new long[18];
        arrl[0] = 312;
        arrl[1] = 120340;
        arrl[2] = 23437;
        arrl[3] = 360031;
        arrl[4] = 6978008;
        arrl[5] = 1234;
        arrl[6] = 19746;
        arrl[7] = 60323;
        arrl[8] = 17001;
        arrl[9] = 89672;
        arrl[10] = 20304;
        arrl[11] = 12434;
        arrl[12] = 45302;
        arrl[13] = 89603;
        arrl[14] = 31234;
        arrl[15] = 68690;
        arrl[16] = 234085;
        r = arrl;
        xmod = 1049057;
        rnmax = xmod;
        i = 17;
        j = 16;
        k = 0;
        n = 18;
        n2 = 200;
        firstcall = 1;
        table = new double[n2];
    }

    GraphCanvas(InfoStorage infoStorage, Applet1 myparent) {
        this.fmetrics = this.getFontMetrics(this.roman);
        this.h = this.fmetrics.getHeight() / 3;
        this.parent = myparent;
        this.mInfoStorage = infoStorage;
        this.setBackground(Color.white);
        this.mDisplayMode = 100;
        this.mRunFlag = 0;
        this.mFromBeginning = true;
    }

    double FINV(double U, double A, double S) {
        return (Math.sqrt(S * S + 2.0 * A * U) - S) / A;
    }

    double RNG(int ID, double A, double S) {
        double R = 0.0;
        double U = this.URNG();
        if (ID < 1 || ID > 5) {
            System.out.println("\n\nERROR: Incorrect selection of random number generator\n");
            System.exit(1);
        }
        switch (ID) {
            case 1: {
                if (A <= 0.0 && S <= 0.0) {
                    R = 0.001;
                    break;
                }
                int i = 2;
                while (i < 13) {
                    U += this.URNG();
                    ++i;
                }
                R = A + S * (U - 6.0);
                break;
            }
            case 2: {
                if (A <= 0.0 && S <= 0.0) {
                    R = 0.001;
                    break;
                }
                R = A + U * (S - A);
                break;
            }
            case 3: {
                if (A <= 0.0) {
                    R = 0.001;
                    break;
                }
                if (U <= 0.0) {
                    U = 1.0E-4;
                }
                R = (- A) * Math.log(U);
                break;
            }
            case 4: {
                R = this.FINV(U, A, S);
                break;
            }
            case 5: {
                if (A <= 0.0) {
                    R = 0.001;
                    break;
                }
                R = A;
                break;
            }
        }
        return R;
    }

    double URNG() {
        GraphCanvas.r[GraphCanvas.i] = (r[j] + r[k]) % xmod;
        double rn = (double)r[i] / rnmax;
        i = (i + 1) % n;
        j = (j + 1) % n;
        k = (k + 1) % n;
        return rn;
    }

    public int choice() {
        if (this.mProbability.length == 1) {
            return 0;
        }
        int I = 0;
        double U = 0.0;
        double F = this.mProbability[0];
        while (U <= 0.0) {
            U = this.rng();
        }
        while (U > F) {
            F += this.mProbability[++I];
        }
        return I;
    }

    public void clear() {
        if (this.algrthm != null) {
            this.algrthm.stop();
        }
        int j = 0;
        while (j < this.mDiskNumber + 2) {
            this.mTaskQVector[j] = null;
            ++j;
        }
        this.mTaskQVector = null;
        this.mTasks = null;
        GraphCanvas.j = 0;
        while (GraphCanvas.j < this.mDiskNumber) {
            i = 0;
            while (i < this.queueLen) {
                this.mDiskQueue[GraphCanvas.j][i].setCurrTask(null);
                ++i;
            }
            ++GraphCanvas.j;
        }
        i = 0;
        while (i < this.queueLen) {
            this.mCpuQueue[i].setCurrTask(null);
            this.mChannelQueue[i].setCurrTask(null);
            ++i;
        }
        i = 0;
        while (i < this.cpuNumber) {
            this.CPU[i].setRunningTask(null);
            ++i;
        }
        this.mChannel.setRunningTask(null);
        i = 0;
        while (i < this.mDiskNumber) {
            this.mDisk[i].setRunningTask(null);
            ++i;
        }
        this.mLeftRecycleChannel.clearAllTasks();
        this.mRightRecycleChannel.clearAllTasks();
        this.mMidRecycleChannel.clearAllTasks();
        this.mDisplayMode = 100;
        this.mRunFlag = 0;
        this.mSingleStepMode_MoveOneStep = 0;
        this.repaint();
    }

    public void decrementZoomDelta() {
        this.mZoomDelta *= 0.8;
        this.mInfoStorage.mSlowModelAvgServTime = (int)(this.mZoomDelta * this.mInfoStorage.mAvgServA[0]);
    }

    public void incrementZoomDelta() {
        this.mZoomDelta *= 1.2;
        this.mInfoStorage.mSlowModelAvgServTime = (int)(this.mZoomDelta * this.mInfoStorage.mAvgServA[0]);
    }

    public void init() {
        Point[] szPoint;
        int i;
        Cpu[] szCpu;
        this.mDisplayMode = 100;
        this.mSingleStepMode_MoveOneStep = 0;
        this.mDiskNumber = this.mInfoStorage.mWSNum;
        this.queueLen = 12;
        this.arrowNumber = 5 + this.mDiskNumber;
        this.cpuNumber = 1;
        this.channelNumber = 1;
        this.queueNumber = 2;
        this.mTaskQVectorNum = this.cpuNumber + this.channelNumber + this.mDiskNumber;
        this.mCpuQueue = new Square[this.queueLen];
        this.mChannelQueue = new Square[this.queueLen];
        this.mDiskQueue = new Square[this.mDiskNumber][];
        this.CPU = new Cpu[this.cpuNumber];
        this.mDisk = new Cpu[this.mDiskNumber];
        this.arrow = new Arrow[this.arrowNumber];
        int w = 8;
        int h = 4;
        int midPointX = 425;
        int midPointY = 120;
        this.mW = w;
        this.szHorizontalLineLength = 12 * w;
        int j = 0;
        while (j < this.mDiskNumber) {
            this.mDiskQueue[j] = new Square[this.queueLen];
            i = 0;
            while (i < this.queueLen) {
                this.mDiskQueue[j][i] = new Square(midPointX - 8 * w + i * 10 * 2, midPointY + 40 * j, 20, Color.black);
                ++i;
            }
            ++j;
        }
        j = 0;
        while (j < this.mDiskNumber) {
            this.arrow[j] = new Arrow(this.mDiskQueue[j][0].UpperLeftPoint.x, this.mDiskQueue[j][0].UpperLeftPoint.y + this.mDiskQueue[j][0].height / 2, this.mDiskQueue[j][0].UpperLeftPoint.x - 4 * w, this.mDiskQueue[j][0].UpperLeftPoint.y + this.mDiskQueue[j][0].height / 2);
            ++j;
        }
        int szDiskWidth = 6 * w;
        int szDiskHeight = 4 * w;
        i = 0;
        while (i < this.mDiskNumber) {
            int szTempRight_X = this.arrow[i].getRealHead_X();
            int szTempRight_Y = this.arrow[i].getRealHead_Y();
            this.mDisk[i] = new Cpu(szTempRight_X - 2 * w * 2, szTempRight_Y - 2 * w, 2 * w, Color.white, this);
            this.mDisk[i].setName("Disk" + i);
            ++i;
        }
        this.arrow[this.mDiskNumber + 2] = new Arrow(midPointX - 4 * w, midPointY - 10 * w, midPointX + 6 * w, midPointY - 10 * w);
        i = 0;
        while (i < this.queueLen) {
            this.mChannelQueue[i] = new Square(this.arrow[this.mDiskNumber + 2].getRealHead_X() + (this.queueLen - i - 1) * 10 * 2, this.arrow[this.mDiskNumber + 2].getRealHead_Y() - 10, 20, Color.black);
            ++i;
        }
        this.arrow[this.mDiskNumber + 3] = new Arrow(this.mChannelQueue[0].UpperLeftPoint.x + this.mChannelQueue[0].height, this.mChannelQueue[0].UpperLeftPoint.y + this.mChannelQueue[0].height / 2, this.mChannelQueue[0].UpperLeftPoint.x + this.mChannelQueue[0].height + 4 * w, this.mChannelQueue[0].UpperLeftPoint.y + this.mChannelQueue[0].height / 2);
        int arrowRealHead_X = this.arrow[this.mDiskNumber + 3].getRealHead_X();
        int arrowRealHead_Y = this.arrow[this.mDiskNumber + 3].getRealHead_Y();
        this.mChannel = new Cpu(arrowRealHead_X, arrowRealHead_Y - 3 * w, 3 * w, Color.white, this);
        this.mChannel.setName("Channel");
        this.arrow[this.mDiskNumber + 4] = new Arrow(this.mChannel.UpperLeftPoint.x + this.mChannel.radius * 2, this.mChannel.UpperLeftPoint.y + this.mChannel.radius, this.mChannel.UpperLeftPoint.x + this.mChannel.radius * 2 + 4 * w, this.mChannel.UpperLeftPoint.y + this.mChannel.radius);
        this.CPU[0] = new Cpu(this.arrow[this.mDiskNumber + 2].start.x - 8 * w, this.arrow[this.mDiskNumber + 2].getRealHead_Y() - 4 * w, 4 * w, Color.white, this);
        this.CPU[0].setName("Cpu0");
        Arrow tempArrow = new Arrow(this.CPU[0].UpperLeftPoint.x, this.CPU[0].UpperLeftPoint.y + this.CPU[0].radius, this.CPU[0].UpperLeftPoint.x - 4 * w, this.CPU[0].UpperLeftPoint.y + this.CPU[0].radius);
        this.arrow[this.mDiskNumber + 1] = new Arrow(tempArrow.getRealHead_X(), tempArrow.getRealHead_Y(), tempArrow.getRealHead_X() + 4 * w, tempArrow.getRealHead_Y());
        i = 0;
        while (i < this.queueLen) {
            this.mCpuQueue[i] = new Square(this.arrow[this.mDiskNumber + 1].start.x - (i + 1) * 10 * 2, this.arrow[this.mDiskNumber + 1].start.y - 10, 20, Color.black);
            ++i;
        }
        tempArrow = new Arrow(this.mCpuQueue[this.queueLen - 1].UpperLeftPoint.x, this.mCpuQueue[this.queueLen - 1].UpperLeftPoint.y + this.mCpuQueue[this.queueLen - 1].height / 2, this.mCpuQueue[this.queueLen - 1].UpperLeftPoint.x - 4 * w, this.mCpuQueue[this.queueLen - 1].UpperLeftPoint.y + this.mCpuQueue[this.queueLen - 1].height / 2);
        this.arrow[this.mDiskNumber] = new Arrow(tempArrow.getRealHead_X(), tempArrow.getRealHead_Y(), tempArrow.getRealHead_X() + 4 * w, tempArrow.getRealHead_Y());
        this.mTaskQVector = new TaskQVector[this.mDiskNumber + 2];
        j = 0;
        while (j < this.mDiskNumber) {
            szCpu = new Cpu[]{this.mDisk[j]};
            this.mTaskQVector[j] = new TaskQVector(500, szCpu);
            ++j;
        }
        this.mTaskQVector[this.mDiskNumber] = new TaskQVector(500, this.CPU);
        szCpu = new Cpu[]{this.mChannel};
        this.mTaskQVector[this.mDiskNumber + 1] = new TaskQVector(500, szCpu);
        double[] szProbability = new double[]{1.0};
        TaskQVector[] szDestinationTaskVectors = new TaskQVector[]{this.mTaskQVector[this.mDiskNumber + 1]};
        this.mTaskQVector[this.mDiskNumber].setRules(szProbability, szDestinationTaskVectors);
        szProbability = new double[this.mDiskNumber];
        szDestinationTaskVectors = new TaskQVector[this.mDiskNumber];
        i = 0;
        while (i < this.mDiskNumber) {
            szDestinationTaskVectors[i] = this.mTaskQVector[i];
            szProbability[i] = 1.0 / (double)this.mDiskNumber;
            ++i;
        }
        this.mTaskQVector[this.mDiskNumber + 1].setRules(szProbability, szDestinationTaskVectors);
        szProbability = new double[]{1.0};
        szDestinationTaskVectors = new TaskQVector[]{this.mTaskQVector[this.mDiskNumber]};
        j = 0;
        while (j < this.mDiskNumber) {
            this.mTaskQVector[j].setRules(szProbability, szDestinationTaskVectors);
            ++j;
        }
        this.mTaskQVector[this.mDiskNumber].setVisualQueue(this.mCpuQueue);
        this.mTaskQVector[this.mDiskNumber].mParent = this.parent;
        this.mTaskQVector[this.mDiskNumber + 1].setVisualQueue(this.mChannelQueue);
        this.mTaskQVector[this.mDiskNumber + 1].mParent = this.parent;
        j = 0;
        while (j < this.mDiskNumber) {
            this.mTaskQVector[j].setVisualQueue(this.mDiskQueue[j]);
            this.mTaskQVector[j].mParent = this.parent;
            ++j;
        }
        i = 0;
        while (i < this.cpuNumber) {
            this.CPU[i].setSourceTaskQueue(this.mTaskQVector[this.mDiskNumber]);
            ++i;
        }
        this.mChannel.setSourceTaskQueue(this.mTaskQVector[this.mDiskNumber + 1]);
        j = 0;
        while (j < this.mDiskNumber) {
            this.mDisk[j].setSourceTaskQueue(this.mTaskQVector[j]);
            ++j;
        }
        szDestinationTaskVectors = new TaskQVector[this.mDiskNumber];
        i = 0;
        while (i < this.mDiskNumber) {
            szDestinationTaskVectors[i] = this.mTaskQVector[i];
            ++i;
        }
        int szMidQueueIndex = 0;
        int szNextQueueIndex = 0;
        if (this.mDiskNumber > 1 && this.mDiskNumber % 2 != 0) {
            szMidQueueIndex = (int)((double)this.mDiskNumber / 2.0) + 1;
            this.mLeftRecycleChannel = new RecycleChannel(new Point(this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.x - this.szHorizontalLineLength, this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius), new Point(this.arrow[this.mDiskNumber].start.x, this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[szMidQueueIndex - 1].radius), new Point(this.arrow[this.mDiskNumber].start.x, this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[szMidQueueIndex - 1].radius), new Point(this.arrow[this.mDiskNumber].start.x, this.arrow[this.mDiskNumber].start.y), this.mTaskQVector[this.mDiskNumber]);
            this.mRightRecycleChannel = new SplitRecycleChannel(new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), this.arrow[this.mDiskNumber + 4].getRealHead_Y()), new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[szMidQueueIndex - 1].radius), new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[szMidQueueIndex - 1].radius), new Point(this.mDiskQueue[szMidQueueIndex][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[szMidQueueIndex][this.queueLen - 1].height + this.szHorizontalLineLength, this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[szMidQueueIndex - 1].radius), null);
            this.mLeftRecycleChannel.setName("mLeftRecycleChannel");
            this.mRightRecycleChannel.setName("mRightRecycleChannel");
            this.mLeftRecycleChannel.setParent(this);
            this.mRightRecycleChannel.setParent(this);
            szPoint = new Point[]{new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), this.arrow[this.mDiskNumber + 4].getRealHead_Y()), new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), this.arrow[this.mDiskNumber + 4].getRealHead_Y() + (int)((double)(this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[szMidQueueIndex - 1].radius - this.arrow[this.mDiskNumber + 4].getRealHead_Y()) / 4.0)), new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), this.arrow[this.mDiskNumber + 4].getRealHead_Y() + (int)((double)(this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[szMidQueueIndex - 1].radius - this.arrow[this.mDiskNumber + 4].getRealHead_Y()) / 4.0) * 2), new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), this.arrow[this.mDiskNumber + 4].getRealHead_Y() + (int)((double)(this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[szMidQueueIndex - 1].radius - this.arrow[this.mDiskNumber + 4].getRealHead_Y()) / 4.0) * 3), new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[szMidQueueIndex - 1].radius), new Point(this.mDiskQueue[szMidQueueIndex][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[szMidQueueIndex][this.queueLen - 1].height + this.szHorizontalLineLength + (int)((double)(this.arrow[this.mDiskNumber + 4].getRealHead_X() - (this.mDiskQueue[szMidQueueIndex][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[szMidQueueIndex][this.queueLen - 1].height + this.szHorizontalLineLength)) / 5.0) * 4, this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[szMidQueueIndex - 1].radius), new Point(this.mDiskQueue[szMidQueueIndex][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[szMidQueueIndex][this.queueLen - 1].height + this.szHorizontalLineLength + (int)((double)(this.arrow[this.mDiskNumber + 4].getRealHead_X() - (this.mDiskQueue[szMidQueueIndex][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[szMidQueueIndex][this.queueLen - 1].height + this.szHorizontalLineLength)) / 5.0) * 3, this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[szMidQueueIndex - 1].radius), new Point(this.mDiskQueue[szMidQueueIndex][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[szMidQueueIndex][this.queueLen - 1].height + this.szHorizontalLineLength + (int)((double)(this.arrow[this.mDiskNumber + 4].getRealHead_X() - (this.mDiskQueue[szMidQueueIndex][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[szMidQueueIndex][this.queueLen - 1].height + this.szHorizontalLineLength)) / 5.0) * 2, this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[szMidQueueIndex - 1].radius), new Point(this.mDiskQueue[szMidQueueIndex][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[szMidQueueIndex][this.queueLen - 1].height + this.szHorizontalLineLength, this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[szMidQueueIndex - 1].radius)};
            this.mRightRecycleChannel.setShowPoints(szPoint);
            szPoint = null;
            szPoint = new Point[]{new Point(this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.x - this.szHorizontalLineLength, this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius), new Point(this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.x - this.szHorizontalLineLength - (int)((double)(this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.x - this.szHorizontalLineLength - this.arrow[this.mDiskNumber].start.x) / 5.0), this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[szMidQueueIndex - 1].radius), new Point(this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.x - this.szHorizontalLineLength - (int)((double)(this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.x - this.szHorizontalLineLength - this.arrow[this.mDiskNumber].start.x) / 5.0) * 2, this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[szMidQueueIndex - 1].radius), new Point(this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.x - this.szHorizontalLineLength - (int)((double)(this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.x - this.szHorizontalLineLength - this.arrow[this.mDiskNumber].start.x) / 5.0) * 3, this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[szMidQueueIndex - 1].radius), new Point(this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.x - this.szHorizontalLineLength - (int)((double)(this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.x - this.szHorizontalLineLength - this.arrow[this.mDiskNumber].start.x) / 5.0) * 4, this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[szMidQueueIndex - 1].radius), new Point(this.arrow[this.mDiskNumber].start.x, this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[szMidQueueIndex - 1].radius), new Point(this.arrow[this.mDiskNumber].start.x, this.arrow[this.mDiskNumber].start.y + (int)((double)(this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[szMidQueueIndex - 1].radius - this.arrow[this.mDiskNumber].start.y) / 6.0) * 5), new Point(this.arrow[this.mDiskNumber].start.x, this.arrow[this.mDiskNumber].start.y + (int)((double)(this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[szMidQueueIndex - 1].radius - this.arrow[this.mDiskNumber].start.y) / 6.0) * 4), new Point(this.arrow[this.mDiskNumber].start.x, this.arrow[this.mDiskNumber].start.y + (int)((double)(this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[szMidQueueIndex - 1].radius - this.arrow[this.mDiskNumber].start.y) / 6.0) * 3), new Point(this.arrow[this.mDiskNumber].start.x, this.arrow[this.mDiskNumber].start.y + (int)((double)(this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[szMidQueueIndex - 1].radius - this.arrow[this.mDiskNumber].start.y) / 6.0) * 2), new Point(this.arrow[this.mDiskNumber].start.x, this.arrow[this.mDiskNumber].start.y + (int)((double)(this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[szMidQueueIndex - 1].radius - this.arrow[this.mDiskNumber].start.y) / 6.0)), new Point(this.arrow[this.mDiskNumber].start.x, this.arrow[this.mDiskNumber].start.y)};
            this.mLeftRecycleChannel.setShowPoints(szPoint);
        } else if (this.mDiskNumber > 1) {
            szMidQueueIndex = this.mDiskNumber / 2;
            szNextQueueIndex = szMidQueueIndex + 1;
            this.mLeftRecycleChannel = new RecycleChannel(new Point(this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.x - this.szHorizontalLineLength, (this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2), new Point(this.arrow[this.mDiskNumber].start.x, (this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2), new Point(this.arrow[this.mDiskNumber].start.x, (this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2), new Point(this.arrow[this.mDiskNumber].start.x, this.arrow[this.mDiskNumber].start.y), this.mTaskQVector[this.mDiskNumber]);
            this.mRightRecycleChannel = new SplitRecycleChannel(new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), this.arrow[this.mDiskNumber + 4].getRealHead_Y()), new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), (this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2), new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), (this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2), new Point(this.mDiskQueue[szMidQueueIndex][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[szMidQueueIndex][this.queueLen - 1].height + this.szHorizontalLineLength, (this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2), null);
            this.mLeftRecycleChannel.setName("mLeftRecycleChannel");
            this.mRightRecycleChannel.setName("mRightRecycleChannel");
            this.mLeftRecycleChannel.setParent(this);
            this.mRightRecycleChannel.setParent(this);
            szPoint = new Point[]{new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), this.arrow[this.mDiskNumber + 4].getRealHead_Y()), new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), this.arrow[this.mDiskNumber + 4].getRealHead_Y() + (int)((double)((this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2 - this.arrow[this.mDiskNumber + 4].getRealHead_Y()) / 4.0)), new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), this.arrow[this.mDiskNumber + 4].getRealHead_Y() + (int)((double)((this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2 - this.arrow[this.mDiskNumber + 4].getRealHead_Y()) / 4.0) * 2), new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), this.arrow[this.mDiskNumber + 4].getRealHead_Y() + (int)((double)((this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2 - this.arrow[this.mDiskNumber + 4].getRealHead_Y()) / 4.0) * 3), new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), (this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2), new Point(this.mDiskQueue[0][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[0][this.queueLen - 1].height + this.szHorizontalLineLength + (int)((double)(this.arrow[this.mDiskNumber + 4].getRealHead_X() - (this.mDiskQueue[0][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[0][this.queueLen - 1].height + this.szHorizontalLineLength)) / 5.0) * 4, (this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2), new Point(this.mDiskQueue[0][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[0][this.queueLen - 1].height + this.szHorizontalLineLength + (int)((double)(this.arrow[this.mDiskNumber + 4].getRealHead_X() - (this.mDiskQueue[0][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[0][this.queueLen - 1].height + this.szHorizontalLineLength)) / 5.0) * 3, (this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2), new Point(this.mDiskQueue[0][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[0][this.queueLen - 1].height + this.szHorizontalLineLength + (int)((double)(this.arrow[this.mDiskNumber + 4].getRealHead_X() - (this.mDiskQueue[0][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[0][this.queueLen - 1].height + this.szHorizontalLineLength)) / 5.0) * 2, (this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2), new Point(this.mDiskQueue[szMidQueueIndex][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[szMidQueueIndex][this.queueLen - 1].height + this.szHorizontalLineLength, (this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2)};
            this.mRightRecycleChannel.setShowPoints(szPoint);
            szPoint = null;
            szPoint = new Point[]{new Point(this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.x - this.szHorizontalLineLength, (this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2), new Point(this.mDisk[0].UpperLeftPoint.x - this.szHorizontalLineLength + (int)((double)(this.arrow[this.mDiskNumber].start.x - (this.mDisk[0].UpperLeftPoint.x - this.szHorizontalLineLength)) / 5.0), (this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2), new Point(this.mDisk[0].UpperLeftPoint.x - this.szHorizontalLineLength + (int)((double)(this.arrow[this.mDiskNumber].start.x - (this.mDisk[0].UpperLeftPoint.x - this.szHorizontalLineLength)) / 5.0) * 2, (this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2), new Point(this.mDisk[0].UpperLeftPoint.x - this.szHorizontalLineLength + (int)((double)(this.arrow[this.mDiskNumber].start.x - (this.mDisk[0].UpperLeftPoint.x - this.szHorizontalLineLength)) / 5.0) * 3, (this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2), new Point(this.mDisk[0].UpperLeftPoint.x - this.szHorizontalLineLength + (int)((double)(this.arrow[this.mDiskNumber].start.x - (this.mDisk[0].UpperLeftPoint.x - this.szHorizontalLineLength)) / 5.0) * 4, (this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2), new Point(this.arrow[this.mDiskNumber].start.x, (this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2), new Point(this.arrow[this.mDiskNumber].start.x, this.arrow[this.mDiskNumber].start.y + (int)((double)((this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2 - this.arrow[this.mDiskNumber].start.y) / 6.0) * 5), new Point(this.arrow[this.mDiskNumber].start.x, this.arrow[this.mDiskNumber].start.y + (int)((double)((this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2 - this.arrow[this.mDiskNumber].start.y) / 6.0) * 4), new Point(this.arrow[this.mDiskNumber].start.x, this.arrow[this.mDiskNumber].start.y + (int)((double)((this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2 - this.arrow[this.mDiskNumber].start.y) / 6.0) * 3), new Point(this.arrow[this.mDiskNumber].start.x, this.arrow[this.mDiskNumber].start.y + (int)((double)((this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2 - this.arrow[this.mDiskNumber].start.y) / 6.0) * 2), new Point(this.arrow[this.mDiskNumber].start.x, this.arrow[this.mDiskNumber].start.y + (int)((double)((this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[1].radius * 2 + this.mDisk[szNextQueueIndex - 1].UpperLeftPoint.y) / 2 - this.arrow[this.mDiskNumber].start.y) / 6.0)), new Point(this.arrow[this.mDiskNumber].start.x, this.arrow[this.mDiskNumber].start.y)};
            this.mLeftRecycleChannel.setShowPoints(szPoint);
        } else {
            szMidQueueIndex = 1;
            this.mLeftRecycleChannel = new RecycleChannel(new Point(this.mDisk[0].UpperLeftPoint.x - this.szHorizontalLineLength, this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius), new Point(this.arrow[this.mDiskNumber].start.x, this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius), new Point(this.arrow[this.mDiskNumber].start.x, (this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius + this.arrow[this.mDiskNumber].start.y) / 2), new Point(this.arrow[this.mDiskNumber].start.x, this.arrow[this.mDiskNumber].start.y), this.mTaskQVector[this.mDiskNumber]);
            this.mRightRecycleChannel = new SplitRecycleChannel(new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), this.arrow[this.mDiskNumber + 4].getRealHead_Y()), new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius), new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius), new Point(this.mDiskQueue[0][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[0][this.queueLen - 1].height + this.szHorizontalLineLength, this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius), null);
            this.mLeftRecycleChannel.setName("mLeftRecycleChannel");
            this.mRightRecycleChannel.setName("mRightRecycleChannel");
            this.mLeftRecycleChannel.setParent(this);
            this.mRightRecycleChannel.setParent(this);
            szPoint = new Point[]{new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), this.arrow[this.mDiskNumber + 4].getRealHead_Y()), new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), this.arrow[this.mDiskNumber + 4].getRealHead_Y() + (int)((double)(this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius - this.arrow[this.mDiskNumber + 4].getRealHead_Y()) / 4.0)), new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), this.arrow[this.mDiskNumber + 4].getRealHead_Y() + (int)((double)(this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius - this.arrow[this.mDiskNumber + 4].getRealHead_Y()) / 4.0) * 2), new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), this.arrow[this.mDiskNumber + 4].getRealHead_Y() + (int)((double)(this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius - this.arrow[this.mDiskNumber + 4].getRealHead_Y()) / 4.0) * 3), new Point(this.arrow[this.mDiskNumber + 4].getRealHead_X(), this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius), new Point(this.mDiskQueue[0][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[0][this.queueLen - 1].height + this.szHorizontalLineLength + (int)((double)(this.arrow[this.mDiskNumber + 4].getRealHead_X() - (this.mDiskQueue[0][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[0][this.queueLen - 1].height + this.szHorizontalLineLength)) / 5.0) * 4, this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius), new Point(this.mDiskQueue[0][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[0][this.queueLen - 1].height + this.szHorizontalLineLength + (int)((double)(this.arrow[this.mDiskNumber + 4].getRealHead_X() - (this.mDiskQueue[0][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[0][this.queueLen - 1].height + this.szHorizontalLineLength)) / 5.0) * 3, this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius), new Point(this.mDiskQueue[0][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[0][this.queueLen - 1].height + this.szHorizontalLineLength + (int)((double)(this.arrow[this.mDiskNumber + 4].getRealHead_X() - (this.mDiskQueue[0][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[0][this.queueLen - 1].height + this.szHorizontalLineLength)) / 5.0) * 2, this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius), new Point(this.mDiskQueue[0][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[0][this.queueLen - 1].height + this.szHorizontalLineLength, this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius)};
            this.mRightRecycleChannel.setShowPoints(szPoint);
            szPoint = null;
            szPoint = new Point[]{new Point(this.mDisk[0].UpperLeftPoint.x - this.szHorizontalLineLength, this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius), new Point(this.mDisk[0].UpperLeftPoint.x - this.szHorizontalLineLength + (int)((double)(this.arrow[this.mDiskNumber].start.x - (this.mDisk[0].UpperLeftPoint.x - this.szHorizontalLineLength)) / 5.0), this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius), new Point(this.mDisk[0].UpperLeftPoint.x - this.szHorizontalLineLength + (int)((double)(this.arrow[this.mDiskNumber].start.x - (this.mDisk[0].UpperLeftPoint.x - this.szHorizontalLineLength)) / 5.0) * 2, this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius), new Point(this.mDisk[0].UpperLeftPoint.x - this.szHorizontalLineLength + (int)((double)(this.arrow[this.mDiskNumber].start.x - (this.mDisk[0].UpperLeftPoint.x - this.szHorizontalLineLength)) / 5.0) * 3, this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius), new Point(this.mDisk[0].UpperLeftPoint.x - this.szHorizontalLineLength + (int)((double)(this.arrow[this.mDiskNumber].start.x - (this.mDisk[0].UpperLeftPoint.x - this.szHorizontalLineLength)) / 5.0) * 4, this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius), new Point(this.arrow[this.mDiskNumber].start.x, this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius), new Point(this.arrow[this.mDiskNumber].start.x, this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius + (int)((double)(this.arrow[this.mDiskNumber].start.y - this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius) / 6.0)), new Point(this.arrow[this.mDiskNumber].start.x, this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius + (int)((double)(this.arrow[this.mDiskNumber].start.y - this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius) / 6.0) * 2), new Point(this.arrow[this.mDiskNumber].start.x, this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius + (int)((double)(this.arrow[this.mDiskNumber].start.y - this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius) / 6.0) * 3), new Point(this.arrow[this.mDiskNumber].start.x, this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius + (int)((double)(this.arrow[this.mDiskNumber].start.y - this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[0].radius) / 6.0) * 4), new Point(this.arrow[this.mDiskNumber].start.x, this.mDisk[0].UpperLeftPoint.y + this.mDisk[0].radius + (int)((double)(this.arrow[this.mDiskNumber].start.y - this.mDisk[szMidQueueIndex - 1].UpperLeftPoint.y + this.mDisk[0].radius) / 6.0) * 5), new Point(this.arrow[this.mDiskNumber].start.x, this.arrow[this.mDiskNumber].start.y)};
            this.mLeftRecycleChannel.setShowPoints(szPoint);
        }
        double[] szDiskProbability = new double[this.mDiskNumber];
        szDestinationTaskVectors = new TaskQVector[this.mDiskNumber];
        double szTotalDiskPV = 0.0;
        double szTotalDiskProbability = 0.0;
        double szTotalPV = 0.0;
        i = 0;
        while (i < this.mDiskNumber) {
            szTotalDiskPV += (double)this.mInfoStorage.mPV[i + 2];
            ++i;
        }
        szTotalPV = this.mInfoStorage.mPV[0];
        this.mProbability = new double[this.mDiskNumber + 1];
        i = 0;
        while (i < this.mDiskNumber) {
            szDiskProbability[i] = (double)this.mInfoStorage.mPV[i + 2] / szTotalDiskPV;
            this.mProbability[i] = (double)this.mInfoStorage.mPV[i + 2] / szTotalPV;
            szTotalDiskProbability += this.mProbability[i];
            szDestinationTaskVectors[i] = this.mTaskQVector[i];
            ++i;
        }
        this.mProbability[this.mDiskNumber] = 1.0 - szTotalDiskProbability;
        this.mRightRecycleChannel.setRules(szDiskProbability, szDestinationTaskVectors);
        this.mMidRecycleChannel = new RecycleChannel(new Point(midPointX - 4 * w, midPointY - 10 * w), new Point(midPointX - w, midPointY - 10 * w), new Point(midPointX + 2 * w, midPointY - 10 * w), new Point(midPointX + 6 * w, midPointY - 10 * w), this.mTaskQVector[this.mDiskNumber + 1]);
        this.mMidRecycleChannel.setName("mMidRecycleChannel");
        this.mMidRecycleChannel.setParent(this);
        this.mEventVector = new EventVector();
        this.mTasks = null;
        this.mTasks = new InitTaskVector(this, this.mInfoStorage);
        this.mDoubleFormat = NumberFormat.getNumberInstance();
        this.mDoubleFormat.setMaximumFractionDigits(3);
        this.mDoubleFormat.setMinimumFractionDigits(3);
        this.mDoubleFormat.setMaximumIntegerDigits(20);
        this.mDoubleFormat7Digits = NumberFormat.getNumberInstance();
        this.mDoubleFormat7Digits.setMaximumFractionDigits(7);
        this.mDoubleFormat7Digits.setMinimumFractionDigits(3);
        this.mDoubleFormat7Digits.setMaximumIntegerDigits(20);
        this.mIntegerFormat = NumberFormat.getNumberInstance();
        this.mIntegerFormat.setParseIntegerOnly(true);
        this.mZoomDelta = (double)this.mInfoStorage.mSlowModelAvgServTime / this.mInfoStorage.mAvgServA[0];
        this.algrthm = null;
        this.algrthm = new Thread(this);
        this.JOB_NO = this.mInfoStorage.mNumJobs;
        j = 0;
        while (j < this.queueLen && j < this.JOB_NO) {
            Task newTask = this.mTasks.createNewColorTask();
            newTask.color = this.mColor[j % this.mColor.length];
            this.mCpuQueue[j].setCurrTask(newTask);
            ++j;
        }
        this.mServerPointers = new Cpu[this.mDiskNumber + 2];
        j = 0;
        while (j < this.mDiskNumber) {
            this.mServerPointers[j] = this.mDisk[j];
            ++j;
        }
        this.mServerPointers[this.mDiskNumber] = this.CPU[0];
        this.mServerPointers[this.mDiskNumber + 1] = this.mChannel;
        this.repaint();
        this.mServerAvgU = new double[this.mDiskNumber + 2];
        this.mServerAvgQLen = new double[this.mDiskNumber + 2];
        this.mServerAvgResponse = new double[this.mDiskNumber + 2];
        i = 0;
        while (i < this.mDiskNumber + 2) {
            this.mServerAvgU[i] = 0.0;
            this.mServerAvgQLen[i] = 0.0;
            this.mServerAvgResponse[i] = 0.0;
            ++i;
        }
        this.mSystemR = 0.0;
        this.mSystemX = 0.0;
        this.mSystemN = 0.0;
        this.mTime = 0.0;
        this.mJobNumber = 0;
        this.popup = new PopupMenu();
        this.mCpuMenuItem = new MenuItem("Channel Queue");
        this.popup.add(this.mCpuMenuItem);
        this.add(this.popup);
        SymMouse aSymMouse = new SymMouse();
        this.addMouseListener(aSymMouse);
    }

    public String intToString(int i) {
        char c = (char)(97 + i);
        return String.valueOf(c);
    }

    boolean isAlwaysZero(int ID, double A, double S) {
        boolean ret = false;
        switch (ID) {
            case 1: {
                if (A == 0.0 && S == 0.0) {
                    ret = true;
                }
            }
            case 2: {
                if (A == 0.0 && S == 0.0) {
                    ret = true;
                }
            }
            case 3: {
                if (A == 0.0) {
                    ret = true;
                }
            }
            case 4: {
                if (A == 0.0 && S == 0.0) {
                    ret = true;
                }
            }
            case 5: {
                if (A != 0.0) break;
                ret = true;
            }
        }
        return ret;
    }

    public void moveAllTasksForward() {
        j = 0;
        while (j < this.mDiskNumber + 2) {
            this.moveTaskForward(this.mTaskQVector[i], this.mTaskQVector[GraphCanvas.i].mVisualQueue);
            ++j;
        }
    }

    public void moveTaskForward(TaskQVector szTasksInQueue, Square[] szSquare) {
        int i = 0;
        int QueueLen = szSquare.length;
        if (szTasksInQueue == null || szTasksInQueue.isEmpty()) {
            int j = 0;
            while (j < QueueLen) {
                szSquare[j].setCurrTask(null);
                ++j;
            }
            return;
        }
        i = 0;
        while (i < szTasksInQueue.size()) {
            if (i >= QueueLen) {
                return;
            }
            Task task = (Task)szTasksInQueue.elementAt(i);
            szSquare[i].setCurrTask(task);
            ++i;
        }
        if (i >= QueueLen) {
            return;
        }
        int j = i;
        while (j < QueueLen) {
            szSquare[j].setCurrTask(null);
            ++j;
        }
    }

    public void paint(Graphics g) {
        int i;
        g.setFont(this.roman);
        g.setColor(Color.white);
        Color oldColor = Color.white;
        int j = 0;
        while (j < this.mDiskNumber) {
            if (this.mRunFlag == 1) {
                this.moveTaskForward(this.mTaskQVector[j], this.mTaskQVector[j].mVisualQueue);
            }
            i = 0;
            while (i < this.queueLen) {
                this.mDiskQueue[j][i].drawSquare(g);
                ++i;
            }
            ++j;
        }
        if (this.mRunFlag == 1) {
            this.moveTaskForward(this.mTaskQVector[this.mDiskNumber], this.mTaskQVector[this.mDiskNumber].mVisualQueue);
            this.moveTaskForward(this.mTaskQVector[this.mDiskNumber + 1], this.mTaskQVector[this.mDiskNumber + 1].mVisualQueue);
        }
        i = 0;
        while (i < this.queueLen) {
            this.mCpuQueue[i].drawSquare(g);
            this.mChannelQueue[i].drawSquare(g);
            ++i;
        }
        i = 0;
        while (i < this.arrowNumber) {
            this.arrow[i].drawArrow(g);
            ++i;
        }
        this.mChannel.drawCpu(g);
        i = 0;
        while (i < this.cpuNumber) {
            this.CPU[i].drawCpu(g);
            ++i;
        }
        j = 0;
        while (j < this.mDiskNumber) {
            this.mDisk[j].drawCpu(g);
            ++j;
        }
        i = 0;
        while (i < this.mDiskNumber) {
            g.drawLine(this.mDiskQueue[i][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[i][this.queueLen - 1].height, this.mDiskQueue[i][this.queueLen - 1].mCenter.y, this.mDiskQueue[i][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[i][this.queueLen - 1].height + this.szHorizontalLineLength, this.mDiskQueue[i][this.queueLen - 1].mCenter.y);
            g.drawLine(this.mDisk[i].mLeftMidPoint.x, this.mDisk[i].mLeftMidPoint.y, this.mDisk[i].mLeftMidPoint.x - this.szHorizontalLineLength, this.mDisk[i].mLeftMidPoint.y);
            this.mDisk[i].drawCpu(g);
            ++i;
        }
        g.drawLine(this.mDisk[0].mLeftMidPoint.x - this.szHorizontalLineLength, this.mDisk[0].mLeftMidPoint.y, this.mDisk[0].mLeftMidPoint.x - this.szHorizontalLineLength, this.mDisk[this.mDiskNumber - 1].mLeftMidPoint.y);
        g.drawLine(this.mDiskQueue[0][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[0][this.queueLen - 1].height + this.szHorizontalLineLength, this.mDisk[0].mRightMidPoint.y, this.mDiskQueue[this.mDiskNumber - 1][this.queueLen - 1].UpperLeftPoint.x + this.mDiskQueue[this.mDiskNumber - 1][this.queueLen - 1].height + this.szHorizontalLineLength, this.mDisk[this.mDiskNumber - 1].mRightMidPoint.y);
        this.mLeftRecycleChannel.drawRecycleChannel(g);
        this.mRightRecycleChannel.drawRecycleChannel(g);
        this.mMidRecycleChannel.drawRecycleChannel(g);
        g.setColor(oldColor);
    }

    double rng() {
        if (firstcall > 0) {
            int i = 0;
            while (i < n) {
                GraphCanvas.table[i] = this.URNG();
                ++i;
            }
            firstcall = 0;
        }
        int itable = (int)((double)n * this.URNG());
        double rnumber = table[itable];
        GraphCanvas.table[itable] = this.URNG();
        return rnumber;
    }

    public void run() {
        boolean szIndex = false;
        double U = 0.0;
        double AJOB = 0.0;
        double TA = 0.0;
        double TS = 0.0;
        double DELTAT = 0.0;
        double TIME = 0.0;
        double AA = 0.0;
        double SA = 0.0;
        double AS = 0.0;
        double SS = 0.0;
        double XSDJOB = 0.0;
        double XATS = 0.0;
        double XSDTS = 0.0;
        double XATA = 0.0;
        double XSDTA = 0.0;
        double XART = 0.0;
        double XSDRT = 0.0;
        double X = 0.0;
        double T = 0.0;
        double N = 0.0;
        long szPreviousJOBS = 0;
        long JOBA = 0;
        long JOBS = 0;
        double TBUSY = 0.0;
        double JOBSEC = 0.0;
        double SDJOB = 0.0;
        double TNA = 0.0;
        double TND = 0.0;
        double ATS = 0.0;
        double SDTS = 0.0;
        double ATA = 0.0;
        double SDTA = 0.0;
        double ART = 0.0;
        double SDRT = 0.0;
        double TART = 0.0;
        this.mServerAvgU = new double[this.mDiskNumber + 2];
        this.mServerAvgQLen = new double[this.mDiskNumber + 2];
        this.mServerAvgResponse = new double[this.mDiskNumber + 2];
        int i = 0;
        while (i < this.mDiskNumber + 2) {
            this.mServerAvgU[i] = 0.0;
            this.mServerAvgQLen[i] = 0.0;
            this.mServerAvgResponse[i] = 0.0;
            ++i;
        }
        this.mSystemR = 0.0;
        this.mSystemX = 0.0;
        this.mSystemN = 0.0;
        this.mTime = 0.0;
        this.mJobNumber = 0;
        int JMAX = 20000;
        int JPRINT = this.mInfoStorage.mJPRINT;
        int IDS = this.mInfoStorage.mServDistribution[0];
        this.CPU[0].AS = this.mInfoStorage.mAvgServA[0];
        this.CPU[0].SS = this.mInfoStorage.mAvgServS[0];
        this.mChannel.AS = this.mInfoStorage.mAvgServA[1];
        this.mChannel.SS = this.mInfoStorage.mAvgServS[1];
        int k = 0;
        while (k < this.mDiskNumber) {
            this.mDisk[k].AS = this.mInfoStorage.mAvgServA[2 + k];
            this.mDisk[k].SS = this.mInfoStorage.mAvgServS[2 + k];
            ++k;
        }
        System.out.println("\n\nSIMULATION RESULTS\n\nJOBS  TIME   ART  X\tN\tAU\tALEN\tRESP\n-----------------------------------------------------------------------------");
        TaskQVector szDummyTaskVector = new TaskQVector();
        int j = 0;
        while (j < this.JOB_NO) {
            double szEventTime = 0.0;
            Task newTask = this.mTasks.createNewColorTask();
            newTask.color = this.mColor[j % this.mColor.length];
            newTask.mRemainingInterArrivalTime = newTask.mInterArrivalTime = 0.0;
            newTask.mEventTime = szEventTime += newTask.mInterArrivalTime;
            newTask.mNextMove = 1;
            newTask.mIsBeingProcessed = false;
            newTask.mRealArrivalTime = newTask.mInterArrivalTime;
            newTask.mRealArrivalTimeInQueue = newTask.mInterArrivalTime;
            newTask.mSystemEntryTime = 0.0;
            newTask.mDestTaskQueue = szDummyTaskVector;
            if (j == 0) {
                this.mEventVector.insertTaskByEventTime(newTask);
            }
            this.mTaskQVector[this.mDiskNumber].appendToList(newTask);
            ++j;
        }
        while (this.mEventVector.size() > 0) {
            if (this.mRunFlag == 1) {
                block98 : {
                    while (this.mDisplayMode == 101 && this.mSingleStepMode_MoveOneStep <= 0) {
                        try {
                            Thread.currentThread();
                            Thread.sleep(500);
                            continue;
                        }
                        catch (InterruptedException v0) {
                            continue;
                        }
                        catch (Exception v1) {}
                    }
                    this.mSingleStepMode_MoveOneStep = 0;
                    if (this.mDisplayMode != 102) {
                        this.repaint();
                    }
                    try {
                        double szElapsedTime;
                        int szIndex2;
                        TaskQVector szSrcTaskQueue;
                        Task szIncomingTask;
                        long NJOB;
                        szIncomingTask = (Task)this.mEventVector.firstElement();
                        this.mEventVector.removeElementAt(0);
                        if (szIncomingTask.mNextMove == 1) {
                            szElapsedTime = szIncomingTask.mRemainingInterArrivalTime;
                            if (szElapsedTime < 0.0) {
                                szElapsedTime = 0.0;
                                szIncomingTask.mRemainingInterArrivalTime = 0.0;
                            }
                            NJOB = JOBA - JOBS;
                            ++JOBA;
                            DELTAT = szIncomingTask.mEventTime - TIME;
                            JOBSEC += DELTAT * (double)NJOB;
                            TIME = szIncomingTask.mEventTime;
                            GraphCanvas.j = 0;
                            while (GraphCanvas.j < this.mDiskNumber + 2) {
                                if (this.mServerPointers[GraphCanvas.j].mCurrentTask != null) {
                                    this.mServerPointers[GraphCanvas.j].mTBUSY += DELTAT;
                                }
                                this.mServerPointers[GraphCanvas.j].mQueueLength += DELTAT * (double)this.mServerPointers[GraphCanvas.j].mTaskQVector.size();
                                ++GraphCanvas.j;
                            }
                            if (this.mDisplayMode != 102) {
                                this.moveTaskForward(this.mTaskQVector[this.mDiskNumber], this.mTaskQVector[this.mDiskNumber].mVisualQueue);
                            }
                            int j2 = 0;
                            while (j2 < this.mTaskQVectorNum) {
                                int szServerNumInTaskQueue = this.mTaskQVector[j2].mConnectedServers.length;
                                int i2 = 0;
                                while (i2 < szServerNumInTaskQueue) {
                                    if (this.mTaskQVector[j2].mConnectedServers[i2].mCurrentTask == null) {
                                        if (this.mTaskQVector[j2].size() > 0) {
                                            Task szFirstTask = null;
                                            szFirstTask = this.mTaskQVector[j2].getFirstUnprocessedElement();
                                            if (szFirstTask != null) {
                                                szFirstTask.mIsBeingProcessed = true;
                                                while ((TS = this.RNG(IDS, this.mTaskQVector[j2].mConnectedServers[i2].AS, this.mTaskQVector[j2].mConnectedServers[i2].SS)) <= 0.0) {
                                                }
                                                szFirstTask.mRemainingServiceTime = szFirstTask.mServiceTime = TS;
                                                szFirstTask.mEventTime = TIME + szFirstTask.mServiceTime;
                                                szFirstTask.mNextMove = 2;
                                                szFirstTask.mSourceServer = this.mTaskQVector[j2].mConnectedServers[i2];
                                                szFirstTask.mTimeInServers += szFirstTask.mServiceTime;
                                                this.mTaskQVector[j2].setJobDirection(szFirstTask);
                                                if (szFirstTask.mSourceServer == this.CPU[0]) {
                                                    ++szFirstTask.mCpuVisits;
                                                    szFirstTask.mDestTaskQueue = this.choice() == this.mDiskNumber ? null : szDummyTaskVector;
                                                }
                                                this.mEventVector.insertTaskByEventTime(szFirstTask);
                                                this.mTaskQVector[j2].mConnectedServers[i2].setRunningTask(szFirstTask);
                                            }
                                        }
                                    } else {
                                        this.mTaskQVector[j2].mConnectedServers[i2].mCurrentTask.mRemainingServiceTime -= szElapsedTime;
                                    }
                                    ++i2;
                                }
                                ++j2;
                            }
                            if (this.mDisplayMode != 102) {
                                this.repaint();
                            }
                            break block98;
                        }
                        if (szIncomingTask.mNextMove != 2) break block98;
                        szElapsedTime = szIncomingTask.mRemainingServiceTime;
                        if (szElapsedTime < 0.0) {
                            szElapsedTime = 0.0;
                            szIncomingTask.mRemainingServiceTime = 0.0;
                        }
                        if (this.mDisplayMode != 102) {
                            try {
                                double szSleepTime = szIncomingTask.mRemainingServiceTime * this.mZoomDelta;
                                Thread.currentThread();
                                Thread.sleep((int)szSleepTime);
                            }
                            catch (InterruptedException v2) {
                            }
                            catch (Exception v3) {}
                        }
                        boolean szShouldExit = false;
                        Object szExitTask = null;
                        Task szRecycleTask = null;
                        if (szIncomingTask.mDestTaskQueue == null) {
                            NJOB = JOBA - JOBS;
                            DELTAT = szIncomingTask.mEventTime - TIME;
                            JOBSEC += DELTAT * (double)NJOB;
                            TIME = szIncomingTask.mEventTime;
                            ++JOBS;
                            GraphCanvas.j = 0;
                            while (GraphCanvas.j < this.mDiskNumber + 2) {
                                if (this.mServerPointers[GraphCanvas.j].mCurrentTask != null) {
                                    this.mServerPointers[GraphCanvas.j].mTBUSY += DELTAT;
                                }
                                this.mServerPointers[GraphCanvas.j].mQueueLength += DELTAT * (double)this.mServerPointers[GraphCanvas.j].mTaskQVector.size();
                                ++GraphCanvas.j;
                            }
                            ++szIncomingTask.mSourceServer.mJobServed;
                            szIncomingTask.mSourceServer.mResponseTime += TIME - szIncomingTask.mRealArrivalTimeInQueue;
                            szSrcTaskQueue = szIncomingTask.mSourceServer.mTaskQVector;
                            szIndex2 = szSrcTaskQueue.indexOf(szIncomingTask);
                            if (szIndex2 >= 0) {
                                szSrcTaskQueue.removeElementAt(szIndex2);
                            }
                            szIncomingTask.mSourceServer.setRunningTask(null);
                            szIncomingTask.mNextMove = 2;
                            szIncomingTask.mIsBeingProcessed = false;
                            szIncomingTask.mRealArrivalTimeInQueue = TIME;
                            if (this.mDisplayMode != 102) {
                                this.moveTaskForward(szSrcTaskQueue, szSrcTaskQueue.mVisualQueue);
                            }
                            szShouldExit = true;
                            szRecycleTask = szIncomingTask;
                            TART += TIME - szRecycleTask.mSystemEntryTime;
                            szRecycleTask.mSystemEntryTime = TIME;
                        } else {
                            DELTAT = szIncomingTask.mEventTime - TIME;
                            TIME = szIncomingTask.mEventTime;
                            GraphCanvas.j = 0;
                            while (GraphCanvas.j < this.mDiskNumber + 2) {
                                if (this.mServerPointers[GraphCanvas.j].mCurrentTask != null) {
                                    this.mServerPointers[GraphCanvas.j].mTBUSY += DELTAT;
                                }
                                this.mServerPointers[GraphCanvas.j].mQueueLength += DELTAT * (double)this.mServerPointers[GraphCanvas.j].mTaskQVector.size();
                                ++GraphCanvas.j;
                            }
                            ++szIncomingTask.mSourceServer.mJobServed;
                            szIncomingTask.mSourceServer.mResponseTime += TIME - szIncomingTask.mRealArrivalTimeInQueue;
                            szSrcTaskQueue = szIncomingTask.mSourceServer.mTaskQVector;
                            szIndex2 = szSrcTaskQueue.indexOf(szIncomingTask);
                            if (szIndex2 >= 0) {
                                szSrcTaskQueue.removeElementAt(szIndex2);
                            }
                            szIncomingTask.mSourceServer.setRunningTask(null);
                            szIncomingTask.mNextMove = 2;
                            szIncomingTask.mIsBeingProcessed = false;
                            szIncomingTask.mRealArrivalTimeInQueue = TIME;
                            if (this.mDisplayMode != 102) {
                                this.moveTaskForward(szSrcTaskQueue, szSrcTaskQueue.mVisualQueue);
                            }
                            szShouldExit = false;
                            szRecycleTask = szIncomingTask;
                        }
                        int j3 = 0;
                        while (j3 < this.mTaskQVectorNum) {
                            int szServerNumInTaskQueue = this.mTaskQVector[j3].mConnectedServers.length;
                            int i3 = 0;
                            while (i3 < szServerNumInTaskQueue) {
                                if (this.mTaskQVector[j3].mConnectedServers[i3].mCurrentTask == null) {
                                    if (this.mTaskQVector[j3].size() > 0) {
                                        Task szFirstTask = null;
                                        szFirstTask = this.mTaskQVector[j3].getFirstUnprocessedElement();
                                        if (szFirstTask != null) {
                                            szFirstTask.mIsBeingProcessed = true;
                                            while ((TS = this.RNG(IDS, this.mTaskQVector[j3].mConnectedServers[i3].AS, this.mTaskQVector[j3].mConnectedServers[i3].SS)) <= 0.0) {
                                            }
                                            szFirstTask.mRemainingServiceTime = szFirstTask.mServiceTime = TS;
                                            szFirstTask.mEventTime = TIME + szFirstTask.mServiceTime;
                                            szFirstTask.mNextMove = 2;
                                            szFirstTask.mSourceServer = this.mTaskQVector[j3].mConnectedServers[i3];
                                            szFirstTask.mTimeInServers += szFirstTask.mServiceTime;
                                            this.mTaskQVector[j3].setJobDirection(szFirstTask);
                                            if (szFirstTask.mSourceServer == this.CPU[0]) {
                                                ++szFirstTask.mCpuVisits;
                                                szFirstTask.mDestTaskQueue = this.choice() == this.mDiskNumber ? null : szDummyTaskVector;
                                            }
                                            this.mEventVector.insertTaskByEventTime(szFirstTask);
                                            this.mTaskQVector[j3].mConnectedServers[i3].setRunningTask(szFirstTask);
                                        }
                                    }
                                } else {
                                    this.mTaskQVector[j3].mConnectedServers[i3].mCurrentTask.mRemainingServiceTime -= szElapsedTime;
                                }
                                ++i3;
                            }
                            ++j3;
                        }
                        if (this.mDisplayMode != 102) {
                            this.repaint();
                        }
                        if (szRecycleTask != null) {
                            if (this.mDisplayMode != 102) {
                                if (szRecycleTask.mSourceServer == this.mChannel) {
                                    this.mRightRecycleChannel.recycleTask(szRecycleTask);
                                    this.mRightRecycleChannel.appendToDestinationTaskVector(szRecycleTask);
                                    if (this.mRightRecycleChannel.mChoice >= 0) {
                                        this.moveTaskForward(this.mTaskQVector[this.mRightRecycleChannel.mChoice], this.mTaskQVector[this.mRightRecycleChannel.mChoice].mVisualQueue);
                                    }
                                    this.mRightRecycleChannel.mChoice = -1;
                                } else if (szRecycleTask.mSourceServer == this.CPU[0]) {
                                    if (!szShouldExit) {
                                        this.mMidRecycleChannel.recycleTask(szRecycleTask);
                                        this.mTaskQVector[this.mDiskNumber + 1].appendToList(szRecycleTask);
                                        this.moveTaskForward(this.mTaskQVector[this.mDiskNumber + 1], this.mTaskQVector[this.mDiskNumber + 1].mVisualQueue);
                                    } else {
                                        this.mTaskQVector[this.mDiskNumber].appendToList(szRecycleTask);
                                    }
                                } else {
                                    this.mLeftRecycleChannel.recycleTask(szRecycleTask);
                                    this.mTaskQVector[this.mDiskNumber].appendToList(szRecycleTask);
                                    this.moveTaskForward(this.mTaskQVector[this.mDiskNumber], this.mTaskQVector[this.mDiskNumber].mVisualQueue);
                                }
                            } else if (szRecycleTask.mSourceServer == this.mChannel) {
                                this.mRightRecycleChannel.appendToDestinationTaskVector(szRecycleTask);
                            } else if (szRecycleTask.mSourceServer == this.CPU[0]) {
                                if (!szShouldExit) {
                                    this.mTaskQVector[this.mDiskNumber + 1].appendToList(szRecycleTask);
                                } else {
                                    this.mTaskQVector[this.mDiskNumber].appendToList(szRecycleTask);
                                }
                            } else {
                                this.mTaskQVector[this.mDiskNumber].appendToList(szRecycleTask);
                            }
                        }
                        int j4 = 0;
                        while (j4 < this.mTaskQVectorNum) {
                            int szServerNumInTaskQueue = this.mTaskQVector[j4].mConnectedServers.length;
                            int i4 = 0;
                            while (i4 < szServerNumInTaskQueue) {
                                if (this.mTaskQVector[j4].mConnectedServers[i4].mCurrentTask == null && this.mTaskQVector[j4].size() > 0) {
                                    Task szFirstTask = null;
                                    szFirstTask = this.mTaskQVector[j4].getFirstUnprocessedElement();
                                    if (szFirstTask != null) {
                                        szFirstTask.mIsBeingProcessed = true;
                                        while ((TS = this.RNG(IDS, this.mTaskQVector[j4].mConnectedServers[i4].AS, this.mTaskQVector[j4].mConnectedServers[i4].SS)) <= 0.0) {
                                        }
                                        szFirstTask.mRemainingServiceTime = szFirstTask.mServiceTime = TS;
                                        szFirstTask.mEventTime = TIME + szFirstTask.mServiceTime;
                                        szFirstTask.mNextMove = 2;
                                        szFirstTask.mSourceServer = this.mTaskQVector[j4].mConnectedServers[i4];
                                        szFirstTask.mTimeInServers += szFirstTask.mServiceTime;
                                        this.mTaskQVector[j4].setJobDirection(szFirstTask);
                                        if (szFirstTask.mSourceServer == this.CPU[0]) {
                                            ++szFirstTask.mCpuVisits;
                                            szFirstTask.mDestTaskQueue = this.choice() == this.mDiskNumber ? null : szDummyTaskVector;
                                        }
                                        this.mEventVector.insertTaskByEventTime(szFirstTask);
                                        this.mTaskQVector[j4].mConnectedServers[i4].setRunningTask(szFirstTask);
                                    }
                                }
                                ++i4;
                            }
                            ++j4;
                        }
                        if (this.mDisplayMode != 102) {
                            this.repaint();
                        }
                        if (JOBS > (long)JMAX) {
                            return;
                        }
                        if (JOBS <= 0 || JOBS % (long)JPRINT > 0 && JOBS < (long)JMAX || JOBS <= szPreviousJOBS) continue;
                        szPreviousJOBS = JOBS;
                        XART = JOBS > 0 ? TART / (double)JOBS : 0.0;
                        X = (double)JOBS / TIME;
                        N = X * XART;
                        int k2 = 0;
                        while (k2 < this.mServerPointers.length) {
                            if (k2 == this.mServerPointers.length - 2) {
                                this.mServerAvgU[0] = this.mServerPointers[k2].mTBUSY / TIME;
                                this.mServerAvgQLen[0] = this.mServerPointers[k2].mQueueLength / TIME;
                                this.mServerAvgResponse[0] = this.mServerPointers[k2].mResponseTime / (double)this.mServerPointers[k2].mJobServed;
                            } else if (k2 == this.mServerPointers.length - 1) {
                                this.mServerAvgU[1] = this.mServerPointers[k2].mTBUSY / TIME;
                                this.mServerAvgQLen[1] = this.mServerPointers[k2].mQueueLength / TIME;
                                this.mServerAvgResponse[1] = this.mServerPointers[k2].mResponseTime / (double)this.mServerPointers[k2].mJobServed;
                            } else {
                                this.mServerAvgU[k2 + 2] = this.mServerPointers[k2].mTBUSY / TIME;
                                this.mServerAvgQLen[k2 + 2] = this.mServerPointers[k2].mQueueLength / TIME;
                                this.mServerAvgResponse[k2 + 2] = this.mServerPointers[k2].mResponseTime / (double)this.mServerPointers[k2].mJobServed;
                            }
                            ++k2;
                        }
                        this.mSystemR = XART;
                        this.mSystemX = X;
                        this.mSystemN = N;
                        this.mTime = TIME;
                        this.mJobNumber = JOBS;
                        if (this.parent.mResultDialog != null) {
                            this.updateResultDialogValues();
                        }
                        System.out.print(String.valueOf(this.mIntegerFormat.format(JOBS)) + "   " + this.mDoubleFormat.format(TIME) + "   " + this.mDoubleFormat.format(XART) + "   " + new Double(X).toString() + "    " + this.mDoubleFormat.format(N) + "    ");
                        int k3 = 0;
                        while (k3 < this.mServerPointers.length) {
                            System.out.print(String.valueOf(this.mIntegerFormat.format(this.mServerPointers[k3].mTBUSY / TIME)) + "   " + this.mDoubleFormat.format(this.mServerPointers[k3].mQueueLength / TIME) + "    " + this.mDoubleFormat.format(this.mServerPointers[k3].mResponseTime / (double)this.mServerPointers[k3].mJobServed));
                            ++k3;
                        }
                        System.out.println("\n==============================================================");
                        if (this.mDisplayMode == 102) {
                            try {
                                Thread.currentThread();
                                Thread.sleep(500);
                            }
                            catch (InterruptedException v4) {
                            }
                            catch (Exception v5) {}
                        }
                    }
                    catch (NoSuchElementException v6) {
                    }
                    catch (ArrayIndexOutOfBoundsException v7) {}
                }
                if (this.mStepByStepMode) {
                    this.mRunFlag = 1 - this.mRunFlag;
                }
                if (JOBS <= (long)JMAX) continue;
                break;
            }
            XART = JOBS > 0 ? TART / (double)JOBS : 0.0;
            X = (double)JOBS / TIME;
            N = X * XART;
            int k4 = 0;
            while (k4 < this.mServerPointers.length) {
                if (k4 == this.mServerPointers.length - 2) {
                    this.mServerAvgU[0] = this.mServerPointers[k4].mTBUSY / TIME;
                    this.mServerAvgQLen[0] = this.mServerPointers[k4].mQueueLength / TIME;
                    this.mServerAvgResponse[0] = this.mServerPointers[k4].mResponseTime / (double)this.mServerPointers[k4].mJobServed;
                } else if (k4 == this.mServerPointers.length - 1) {
                    this.mServerAvgU[1] = this.mServerPointers[k4].mTBUSY / TIME;
                    this.mServerAvgQLen[1] = this.mServerPointers[k4].mQueueLength / TIME;
                    this.mServerAvgResponse[1] = this.mServerPointers[k4].mResponseTime / (double)this.mServerPointers[k4].mJobServed;
                } else {
                    this.mServerAvgU[k4 + 2] = this.mServerPointers[k4].mTBUSY / TIME;
                    this.mServerAvgQLen[k4 + 2] = this.mServerPointers[k4].mQueueLength / TIME;
                    this.mServerAvgResponse[k4 + 2] = this.mServerPointers[k4].mResponseTime / (double)this.mServerPointers[k4].mJobServed;
                }
                ++k4;
            }
            this.mSystemR = XART;
            this.mSystemX = X;
            this.mSystemN = N;
            this.mTime = TIME;
            this.mJobNumber = JOBS;
            if (this.parent.mResultDialog != null) {
                this.updateResultDialogValues();
            }
            System.out.println(String.valueOf(this.mIntegerFormat.format(JOBS)) + "   " + this.mDoubleFormat.format(TIME) + "   " + this.mDoubleFormat.format(XART) + "   " + this.mDoubleFormat.format(X));
            System.out.println("==============================================================");
            while (this.mRunFlag == 0) {
                try {
                    Thread.currentThread();
                    Thread.sleep(100);
                    continue;
                }
                catch (InterruptedException v8) {
                    continue;
                }
                catch (Exception v9) {}
            }
        }
        this.algrthm = null;
    }

    public void runalg(boolean fromBeginning) {
        this.mFromBeginning = fromBeginning;
        if (this.mFromBeginning) {
            this.mDisplayMode = 100;
            this.mSingleStepMode_MoveOneStep = 0;
            if (this.algrthm != null) {
                this.algrthm.stop();
            }
            this.init();
            this.start();
        }
    }

    public void start() {
        this.algrthm.start();
    }

    public void stop() {
        if (this.algrthm != null) {
            this.algrthm.suspend();
        }
        this.clear();
        this.mDisplayMode = 100;
        this.mSingleStepMode_MoveOneStep = 0;
    }

    public final synchronized void update(Graphics g) {
        Dimension d = this.size();
        if (this.offScreenImage == null || d.width != this.offScreenSize.width || d.height != this.offScreenSize.height) {
            this.offScreenImage = this.createImage(d.width, d.height);
            this.offScreenSize = d;
            this.offScreenGraphics = this.offScreenImage.getGraphics();
        }
        this.offScreenGraphics.setColor(Color.white);
        this.offScreenGraphics.fillRect(0, 0, d.width, d.height);
        this.paint(this.offScreenGraphics);
        g.drawImage(this.offScreenImage, 0, 0, null);
    }

    public void updateResultDialogValues() {
        this.parent.mResultDialog.mSystemR.setText(this.mDoubleFormat.format(this.mSystemR));
        if (this.mSystemX >= 1.0) {
            this.parent.mResultDialog.mSystemX.setText(this.mDoubleFormat.format(this.mSystemX));
        } else {
            this.parent.mResultDialog.mSystemX.setText(this.mDoubleFormat7Digits.format(this.mSystemX));
        }
        this.parent.mResultDialog.mSystemN.setText(this.mDoubleFormat.format(this.mSystemN));
        this.parent.mResultDialog.mTime.setText(this.mDoubleFormat.format(this.mTime));
        this.parent.mResultDialog.mJobNumber.setText(this.mIntegerFormat.format(this.mJobNumber));
        int i = 0;
        while (i < this.mDiskNumber + 2) {
            this.parent.mResultDialog.mServerAvgU[i].setText(this.mDoubleFormat.format(this.mServerAvgU[i]));
            this.parent.mResultDialog.mServerAvgQLen[i].setText(this.mDoubleFormat.format(this.mServerAvgQLen[i]));
            this.parent.mResultDialog.mServerAvgResponse[i].setText(this.mDoubleFormat.format(this.mServerAvgResponse[i]));
            ++i;
        }
    }

    class SymMouse
    extends MouseAdapter {
        SymMouse() {
        }

        public void mouseClicked(MouseEvent event) {
            int click_X = event.getX();
            int click_Y = event.getY();
            if (click_X >= GraphCanvas.this.CPU[0].UpperLeftPoint.x && click_X <= GraphCanvas.this.CPU[0].UpperLeftPoint.x + 2 * GraphCanvas.this.CPU[0].radius && click_Y >= GraphCanvas.this.CPU[0].UpperLeftPoint.y && click_Y <= GraphCanvas.this.CPU[0].UpperLeftPoint.y + 2 * GraphCanvas.this.CPU[0].radius) {
                GraphCanvas.this.popup.getItem(0).setLabel("CPU");
                GraphCanvas.this.popup.show(GraphCanvas.this, click_X, click_Y);
                return;
            }
            if (click_X >= GraphCanvas.this.mChannel.UpperLeftPoint.x && click_X <= GraphCanvas.this.mChannel.UpperLeftPoint.x + 2 * GraphCanvas.this.mChannel.radius && click_Y >= GraphCanvas.this.mChannel.UpperLeftPoint.y && click_Y <= GraphCanvas.this.mChannel.UpperLeftPoint.y + 2 * GraphCanvas.this.mChannel.radius) {
                GraphCanvas.this.popup.getItem(0).setLabel("Channel");
                GraphCanvas.this.popup.show(GraphCanvas.this, click_X, click_Y);
                return;
            }
            int i = 0;
            while (i < GraphCanvas.this.mDiskNumber) {
                if (click_X >= GraphCanvas.this.mDisk[i].UpperLeftPoint.x && click_X <= GraphCanvas.this.mDisk[i].UpperLeftPoint.x + 2 * GraphCanvas.this.mDisk[i].radius && click_Y >= GraphCanvas.this.mDisk[i].UpperLeftPoint.y && click_Y <= GraphCanvas.this.mDisk[i].UpperLeftPoint.y + 2 * GraphCanvas.this.mDisk[i].radius) {
                    GraphCanvas.this.popup.getItem(0).setLabel("Disk " + (i + 1));
                    GraphCanvas.this.popup.show(GraphCanvas.this, click_X, click_Y);
                    return;
                }
                ++i;
            }
            Point queueLowerRight = new Point(GraphCanvas.this.mCpuQueue[0].UpperLeftPoint.x + GraphCanvas.this.mCpuQueue[0].height, GraphCanvas.this.mCpuQueue[0].UpperLeftPoint.y + GraphCanvas.this.mCpuQueue[0].height);
            Point mQueueUpperLeft = new Point(GraphCanvas.this.mCpuQueue[GraphCanvas.this.queueLen - 1].UpperLeftPoint.x, GraphCanvas.this.mCpuQueue[GraphCanvas.this.queueLen - 1].UpperLeftPoint.y);
            Point mQueueLowerRight = new Point(queueLowerRight.x, queueLowerRight.y);
            if (click_X >= mQueueUpperLeft.x && click_X <= mQueueLowerRight.x && click_Y >= mQueueUpperLeft.y && click_Y <= mQueueLowerRight.y) {
                GraphCanvas.this.popup.getItem(0).setLabel("CPU Queue");
                GraphCanvas.this.popup.show(GraphCanvas.this, click_X, click_Y);
                return;
            }
            queueLowerRight = new Point(GraphCanvas.this.mChannelQueue[0].UpperLeftPoint.x + GraphCanvas.this.mChannelQueue[0].height, GraphCanvas.this.mChannelQueue[0].UpperLeftPoint.y + GraphCanvas.this.mChannelQueue[0].height);
            mQueueUpperLeft = new Point(GraphCanvas.this.mChannelQueue[GraphCanvas.this.queueLen - 1].UpperLeftPoint.x, GraphCanvas.this.mChannelQueue[GraphCanvas.this.queueLen - 1].UpperLeftPoint.y);
            mQueueLowerRight = new Point(queueLowerRight.x, queueLowerRight.y);
            if (click_X >= mQueueUpperLeft.x && click_X <= mQueueLowerRight.x && click_Y >= mQueueUpperLeft.y && click_Y <= mQueueLowerRight.y) {
                GraphCanvas.this.popup.getItem(0).setLabel("Channel Queue");
                GraphCanvas.this.popup.show(GraphCanvas.this, click_X, click_Y);
                return;
            }
            int i2 = 0;
            while (i2 < GraphCanvas.this.mDiskNumber) {
                mQueueUpperLeft = new Point(GraphCanvas.this.mDiskQueue[i2][0].UpperLeftPoint.x, GraphCanvas.this.mDiskQueue[i2][0].UpperLeftPoint.y);
                queueLowerRight = new Point(GraphCanvas.this.mDiskQueue[i2][GraphCanvas.this.queueLen - 1].UpperLeftPoint.x + GraphCanvas.this.mDiskQueue[i2][GraphCanvas.this.queueLen - 1].height, GraphCanvas.this.mDiskQueue[i2][GraphCanvas.this.queueLen - 1].UpperLeftPoint.y + GraphCanvas.this.mDiskQueue[i2][GraphCanvas.this.queueLen - 1].height);
                mQueueLowerRight = new Point(queueLowerRight.x, queueLowerRight.y);
                if (click_X >= mQueueUpperLeft.x && click_X <= mQueueLowerRight.x && click_Y >= mQueueUpperLeft.y && click_Y <= mQueueLowerRight.y) {
                    GraphCanvas.this.popup.getItem(0).setLabel("Disk " + (i2 + 1) + "'s Queue");
                    GraphCanvas.this.popup.show(GraphCanvas.this, click_X, click_Y);
                    return;
                }
                ++i2;
            }
        }
    }

}

