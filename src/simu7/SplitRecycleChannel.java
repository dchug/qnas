/*
 * Decompiled with CFR 0_115.
 */
package simu7;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.io.PrintStream;
import simu7.GraphCanvas;
import simu7.Node;
import simu7.Task;
import simu7.TaskQVector;

class SplitRecycleChannel
extends Canvas {
    public Task[] mCurrentTask;
    public Point[] mPoint = new Point[4];
    public Point[] mShowPoint;
    private int mShowPointNumber = 3;
    final int mPointNumber = 4;
    static int mTaskPosIndex = 0;
    public boolean mNoMoreJobComing = false;
    public String mName;
    public GraphCanvas mParent;
    public TaskQVector[] mDestinationTaskQVector;
    public double[] mProbability = null;
    public int mChoice = -1;
    static long[] r;
    static long xmod;
    static double rnmax;
    static int i;
    static int j;
    static int k;
    static int n;
    static int n2;
    static int firstcall;
    static double[] table;

    static {
        long[] arrl = new long[18];
        arrl[0] = 145256;
        arrl[1] = 2345;
        arrl[2] = 568346;
        arrl[3] = 13347;
        arrl[4] = 23687;
        arrl[5] = 19751;
        arrl[6] = 73653;
        arrl[7] = 23487;
        arrl[8] = 4346785;
        arrl[9] = 89672;
        arrl[10] = 20304;
        arrl[11] = 345698;
        arrl[12] = 2348;
        arrl[13] = 5676;
        arrl[14] = 31234;
        arrl[15] = 68690;
        arrl[16] = 234085;
        r = arrl;
        xmod = 1049057;
        rnmax = xmod;
        i = 17;
        j = 16;
        k = 0;
        n = 18;
        n2 = 200;
        firstcall = 1;
        table = new double[n2];
    }

    public SplitRecycleChannel(Point p1, Point p2, Point p3, Point p4, TaskQVector[] destinationTaskQVector) {
        int i = 0;
        while (i < 4) {
            if (i == 0) {
                this.mPoint[i] = new Point(p1.x, p1.y);
            }
            if (i == 1) {
                this.mPoint[i] = new Point(p2.x, p2.y);
            }
            if (i == 2) {
                this.mPoint[i] = new Point(p3.x, p3.y);
            }
            if (i == 3) {
                this.mPoint[i] = new Point(p4.x, p4.y);
            }
            ++i;
        }
        this.setShowPoints();
        this.mCurrentTask = new Task[3];
        int i2 = 0;
        while (i2 < 3) {
            this.mCurrentTask[i2] = null;
            ++i2;
        }
        this.mDestinationTaskQVector = destinationTaskQVector;
    }

    double FINV(double U, double A, double S) {
        return (Math.sqrt(S * S + 2.0 * A * U) - S) / A;
    }

    double RNG(int ID, double A, double S) {
        double R = 0.0;
        double U = this.URNG();
        if (ID < 1 || ID > 5) {
            System.out.println("\n\nERROR: Incorrect selection of random number generator\n");
            System.exit(1);
        }
        switch (ID) {
            case 1: {
                int i = 2;
                while (i < 13) {
                    U += this.URNG();
                    ++i;
                }
                R = A + S * (U - 6.0);
                break;
            }
            case 2: {
                R = A + U * (S - A);
                break;
            }
            case 3: {
                R = (- A) * Math.log(U);
                break;
            }
            case 4: {
                R = this.FINV(U, A, S);
                break;
            }
            case 5: {
                R = A;
                break;
            }
        }
        return R;
    }

    double URNG() {
        SplitRecycleChannel.r[SplitRecycleChannel.i] = (r[j] + r[k]) % xmod;
        double rn = (double)r[i] / rnmax;
        i = (i + 1) % n;
        j = (j + 1) % n;
        k = (k + 1) % n;
        return rn;
    }

    public void appendToDestinationTaskVector(Task task) {
        if (task != null && this.mDestinationTaskQVector != null) {
            int szChoice;
            this.mChoice = szChoice = this.choice();
            if (this.mDestinationTaskQVector[szChoice] != null) {
                this.mDestinationTaskQVector[szChoice].appendToList(task);
            } else {
                System.out.println("In movePreviousJobsForward(): mDestinationTaskQVector[szChoice] == null");
            }
        }
    }

    public int choice() {
        if (this.mProbability.length == 1) {
            return 0;
        }
        int I = 0;
        double U = 0.0;
        double F = this.mProbability[0];
        while (U <= 0.0) {
            U = this.rng();
        }
        while (U > F) {
            F += this.mProbability[++I];
        }
        return I;
    }

    public void clearAllTasks() {
        int i = 0;
        while (i < this.mShowPointNumber) {
            this.mCurrentTask[i] = null;
            ++i;
        }
    }

    public void drawChannel(Graphics g) {
        this.drawRecycleChannel(g);
    }

    public void drawRecycleChannel(Graphics g) {
        g.setColor(Color.black);
        g.drawLine(this.mPoint[0].x, this.mPoint[0].y, this.mPoint[1].x, this.mPoint[1].y);
        g.drawLine(this.mPoint[1].x, this.mPoint[1].y, this.mPoint[2].x, this.mPoint[2].y);
        g.drawLine(this.mPoint[2].x, this.mPoint[2].y, this.mPoint[3].x, this.mPoint[3].y);
        int i = 0;
        while (i < this.mShowPointNumber) {
            if (this.mCurrentTask[i] != null) {
                this.mCurrentTask[i].drawTask(g);
            }
            ++i;
        }
    }

    public boolean hasJobInChannel() {
        int i = 0;
        while (i < this.mShowPointNumber) {
            if (this.mCurrentTask[i] != null) {
                return true;
            }
            ++i;
        }
        return false;
    }

    public void movePreviousJobsForward() {
        int i = 0;
        while (i < this.mShowPointNumber - 1) {
            this.mCurrentTask[i] = this.mCurrentTask[i + 1] != null ? new Task(this.mShowPoint[this.mShowPointNumber - 1 - i].x, this.mShowPoint[this.mShowPointNumber - 1 - i].y, this.mCurrentTask[i + 1].radius, this.mCurrentTask[i + 1].color, this.mCurrentTask[i + 1].mServiceTime, this.mCurrentTask[i + 1].mInterArrivalTime) : null;
            ++i;
        }
    }

    public void paint(Graphics g) {
        this.drawRecycleChannel(g);
    }

    public void recycleTask(Task task) {
        this.setCurrentTask(task);
        this.mParent.repaint();
        int i = 0;
        while (i < this.mShowPointNumber) {
            try {
                Thread.currentThread();
                Thread.sleep(25);
            }
            catch (InterruptedException v0) {
            }
            catch (Exception v1) {}
            this.setCurrentTask(null);
            this.mParent.repaint();
            ++i;
        }
    }

    double rng() {
        if (firstcall > 0) {
            int i = 0;
            while (i < n) {
                SplitRecycleChannel.table[i] = this.URNG();
                ++i;
            }
            firstcall = 0;
        }
        int itable = (int)((double)n * this.URNG());
        double rnumber = table[itable];
        SplitRecycleChannel.table[itable] = this.URNG();
        return rnumber;
    }

    public void setCurrentTask(Task task) {
        this.movePreviousJobsForward();
        if (task == null) {
            this.mCurrentTask[this.mShowPointNumber - 1] = null;
            return;
        }
        this.mCurrentTask[this.mShowPointNumber - 1] = new Task(this.mShowPoint[0].x, this.mShowPoint[0].y, task.radius, task.color, task.mServiceTime, task.mInterArrivalTime);
    }

    public void setName(String name) {
        this.mName = name;
    }

    public void setParent(GraphCanvas parent) {
        this.mParent = parent;
        this.setShowPoints();
    }

    public void setRules(double[] probability, TaskQVector[] destTaskQVector) {
        this.mProbability = new double[probability.length];
        int i = 0;
        while (i < probability.length) {
            this.mProbability[i] = probability[i];
            ++i;
        }
        this.mDestinationTaskQVector = destTaskQVector;
    }

    public void setShowPoints() {
        int radius = new Task().radius;
        this.mShowPoint = new Point[3];
        int i = 0;
        while (i < 3) {
            if (i == 0) {
                this.mShowPoint[i] = new Point((this.mPoint[0].x + this.mPoint[1].x) / 2 - radius, (this.mPoint[0].y + this.mPoint[1].y) / 2 - radius);
            }
            if (i == 1) {
                this.mShowPoint[i] = new Point((this.mPoint[1].x + this.mPoint[2].x) / 2 - radius, (this.mPoint[1].y + this.mPoint[2].y) / 2 - radius);
            }
            if (i == 2) {
                this.mShowPoint[i] = new Point((this.mPoint[2].x + this.mPoint[3].x) / 2 - radius, (this.mPoint[2].y + this.mPoint[3].y) / 2 - radius);
            }
            ++i;
        }
    }

    public void setShowPoints(Point[] point) {
        int radius = new Task().radius;
        this.mShowPointNumber = point.length;
        this.mShowPoint = new Point[this.mShowPointNumber];
        int i = 0;
        while (i < this.mShowPointNumber) {
            this.mShowPoint[i] = new Point(point[i].x - radius, point[i].y - radius);
            ++i;
        }
        this.mCurrentTask = null;
        this.mCurrentTask = new Task[this.mShowPointNumber];
        int i2 = 0;
        while (i2 < this.mShowPointNumber) {
            this.mCurrentTask[i2] = null;
            ++i2;
        }
    }
}

