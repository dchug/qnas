/*
 * Decompiled with CFR 0_115.
 */
package simu7;

import java.applet.Applet;
import java.applet.AppletContext;
import java.awt.Button;
import java.awt.Component;
import java.awt.Event;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.LayoutManager;
import java.awt.Panel;
import java.awt.TextField;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.NumberFormat;
import kcmdg.sntp.InfoStorage;
import kcmdg.sntp.SetupDialog;
import simu7.Applet1;
import simu7.GraphCanvas;
import simu7.ResultFrame;

public class Options
extends Panel
implements Runnable {
    Button b1 = new Button("Initialize");
    Button b2 = new Button("Run");
    Button b3 = new Button("Num. Results");
    Button b4 = new Button("End");
    Button b5 = new Button("Help");
    Label mQNAS = new Label("QNAS");
    static final int WAIT = 0;
    static final int RUN = 1;
    public int mRunFlag;
    public boolean mFromBeginning = true;
    Applet1 mParent;
    public InfoStorage mInfoStorage;
    int mCpuNumber;
    public SetupDialog szSetupDialog = null;

    Options(InfoStorage infoStorage, Applet1 myparent, int szCpuNumber) {
        this.mParent = myparent;
        this.mInfoStorage = infoStorage;
        this.mCpuNumber = szCpuNumber <= 0 ? 0 : szCpuNumber;
        this.mRunFlag = 0;
        this.setLayout(new GridLayout(6, 1, 0, 10));
        this.add(this.mQNAS);
        this.add(this.b5);
        this.add(this.b1);
        this.add(this.b2);
        this.add(this.b3);
        this.add(this.b4);
    }

    public boolean action(Event evt, Object arg) {
        if (evt.target instanceof Button) {
            NumberFormat mDoubleFormat;
            NumberFormat mIntegerFormat;
            if (((String)arg).equals("Initialize")) {
                if (this.szSetupDialog == null) {
                    this.szSetupDialog = new SetupDialog(this.mInfoStorage, true, (Applet)this.mParent);
                }
                this.szSetupDialog.pack();
                this.szSetupDialog.show();
            }
            if (((String)arg).equals("Run")) {
                if (this.mParent.mResultDialog == null) {
                    this.mParent.mResultDialog = new ResultFrame(true, this.mInfoStorage.mWSNum, this.mParent);
                    this.mParent.mResultTableThread = new Thread(this.mParent.mResultDialog);
                    this.mParent.mResultTableThread.start();
                }
                this.mParent.graphcanvas.mRunFlag = this.mRunFlag = 1;
                this.b2.setLabel("Stop");
                this.b1.setEnabled(false);
                if (this.mFromBeginning) {
                    mDoubleFormat = NumberFormat.getNumberInstance();
                    mDoubleFormat.setMaximumFractionDigits(3);
                    mDoubleFormat.setMinimumFractionDigits(3);
                    mDoubleFormat.setMaximumIntegerDigits(10);
                    mIntegerFormat = NumberFormat.getNumberInstance();
                    mIntegerFormat.setParseIntegerOnly(true);
                }
                this.mParent.graphcanvas.runalg(this.mFromBeginning);
                this.mFromBeginning = false;
            }
            if (((String)arg).equals("Stop")) {
                this.mParent.graphcanvas.mRunFlag = this.mRunFlag = 0;
                this.b2.setLabel("Run");
            }
            if (((String)arg).equals("Num. Results")) {
                mDoubleFormat = NumberFormat.getNumberInstance();
                mDoubleFormat.setMaximumFractionDigits(3);
                mDoubleFormat.setMinimumFractionDigits(3);
                mDoubleFormat.setMaximumIntegerDigits(10);
                mIntegerFormat = NumberFormat.getNumberInstance();
                mIntegerFormat.setParseIntegerOnly(true);
                if (this.mParent.mResultDialog == null) {
                    this.mParent.mResultDialog = new ResultFrame(true, this.mInfoStorage.mWSNum, this.mParent);
                    this.mParent.mResultTableThread = new Thread(this.mParent.mResultDialog);
                    this.mParent.mResultTableThread.start();
                }
                int i = 0;
                while (i < this.mInfoStorage.mWSNum + 2) {
                    this.mParent.mResultDialog.mServerAvgU[i].setText(mDoubleFormat.format(this.mParent.graphcanvas.mServerAvgU[i]));
                    this.mParent.mResultDialog.mServerAvgQLen[i].setText(mDoubleFormat.format(this.mParent.graphcanvas.mServerAvgQLen[i]));
                    this.mParent.mResultDialog.mServerAvgResponse[i].setText(mDoubleFormat.format(this.mParent.graphcanvas.mServerAvgResponse[i]));
                    ++i;
                }
                this.mParent.mResultDialog.mJobNumber.setText(mIntegerFormat.format(this.mParent.graphcanvas.mJobNumber));
                this.mParent.mResultDialog.mTime.setText(mDoubleFormat.format(this.mParent.graphcanvas.mTime));
                this.mParent.mResultDialog.mSystemR.setText(mDoubleFormat.format(this.mParent.graphcanvas.mSystemR));
                this.mParent.mResultDialog.mSystemX.setText(mDoubleFormat.format(this.mParent.graphcanvas.mSystemX));
                this.mParent.mResultDialog.mSystemN.setText(mDoubleFormat.format(this.mParent.graphcanvas.mSystemN));
                this.mParent.mResultDialog.setVisible(true);
            }
            if (((String)arg).equals("End")) {
                this.mFromBeginning = true;
                this.mRunFlag = 0;
                this.b2.setLabel("Run");
                this.b1.setEnabled(true);
                this.mParent.graphcanvas.clear();
                if (this.mParent.mResultDialog != null) {
                    this.mParent.mResultDialog.initResultDialog();
                }
            }
            if (((String)arg).equals("Help")) {
                try {
                    URL szCurrentAppletURL = this.mParent.getCodeBase();
                    String tempStr = szCurrentAppletURL.toString().trim();
                    tempStr = tempStr.concat("help.html");
                    szCurrentAppletURL = new URL(tempStr);
                    System.out.println("URL == " + tempStr);
                    this.mParent.getAppletContext().showDocument(szCurrentAppletURL, "Help");
                }
                catch (MalformedURLException me) {
                    System.out.println("Error == " + me);
                }
                catch (Exception e) {
                    System.out.println("Error == " + e);
                }
            }
        }
        return true;
    }

    public void resetInfoStorage(InfoStorage szInfoStorage) {
        if (szInfoStorage != null) {
            this.mInfoStorage.mQueueCapacity = szInfoStorage.mQueueCapacity;
            this.mInfoStorage.mSimulationModel = szInfoStorage.mSimulationModel;
            this.mInfoStorage.mSlowModelAvgServTime = szInfoStorage.mSlowModelAvgServTime;
            this.mInfoStorage.mNumJobs = szInfoStorage.mNumJobs;
            this.mInfoStorage.mJPRINT = szInfoStorage.mJPRINT;
            this.mInfoStorage.mArrivalDistribution = szInfoStorage.mArrivalDistribution;
            this.mInfoStorage.mAvgArrivalA = szInfoStorage.mAvgArrivalA;
            this.mInfoStorage.mAvgArrivalS = szInfoStorage.mAvgArrivalS;
            int i = 0;
            while (i < 10) {
                this.mInfoStorage.mServDistribution[i] = szInfoStorage.mServDistribution[i];
                this.mInfoStorage.mAvgServA[i] = szInfoStorage.mAvgServA[i];
                this.mInfoStorage.mAvgServS[i] = szInfoStorage.mAvgServS[i];
                this.mInfoStorage.mPV[i] = szInfoStorage.mPV[i];
                ++i;
            }
            this.mInfoStorage.mWSNum = szInfoStorage.mWSNum;
        }
        if (this.mParent.mResultDialog == null) {
            this.mParent.mResultDialog = new ResultFrame(true, this.mInfoStorage.mWSNum, this.mParent);
            this.mParent.mResultTableThread = new Thread(this.mParent.mResultDialog);
            this.mParent.mResultTableThread.start();
        }
        this.mParent.mResultDialog.enableDiskEntries(this.mInfoStorage.mWSNum);
        this.mParent.graphcanvas.init();
    }

    public void run() {
        try {
            Thread.currentThread();
            Thread.sleep(100);
        }
        catch (InterruptedException v0) {
        }
        catch (Exception v1) {}
        this.mParent.graphcanvas.mFromBeginning = true;
        this.mParent.graphcanvas.init();
    }
}

