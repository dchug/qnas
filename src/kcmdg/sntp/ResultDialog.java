package kcmdg.sntp;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.LayoutManager;
import java.awt.Panel;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import kcmdg.sntp.FancyTextField;
import moondog.widgets.BorderPanel;
import moondog.widgets.EnterButton;

public class ResultDialog
extends Dialog
implements ActionListener {
    EnterButton mEnterButtonCancel;
    public FancyTextField[] mServerAvgU;
    public FancyTextField[] mServerAvgQLen;
    public FancyTextField[] mServerAvgResponse;
    public FancyTextField mSystemR;
    public FancyTextField mSystemX;
    public FancyTextField mSystemN;
    public FancyTextField mJobNumber;
    public FancyTextField mTime;
    public boolean mShouldClose = false;
    int mCpuNumber = 10;
    int mActiveDiskNumber;
    boolean mBooleanCancelled;
    Frame mFrame;

    public ResultDialog(Frame frame, boolean isCentralServerModel, int activeDiskNumber) {
        super(frame, "result", true);
        this.mFrame = frame;
        this.mActiveDiskNumber = activeDiskNumber;
        this.setBackground(Color.lightGray);
        SysWindow aSysWindow = new SysWindow();
        this.addWindowListener(aSysWindow);
        BorderPanel borderpanel = new BorderPanel("Result");
        GridLayout gridbaglayout = new GridLayout(14, 1);
        borderpanel.setLayout(gridbaglayout);
        Panel szSystemResultPanel = new Panel();
        szSystemResultPanel.setLayout(new FlowLayout(0));
        szSystemResultPanel.add(new Label("System"));
        szSystemResultPanel.add(new Label("                                  JOBS"));
        szSystemResultPanel.add(new Label("                                          TIME"));
        szSystemResultPanel.add(new Label("                                 R"));
        szSystemResultPanel.add(new Label("                       X"));
        szSystemResultPanel.add(new Label("                 N (=XR)"));
        borderpanel.add(szSystemResultPanel);
        Panel szSystemPanel = new Panel();
        szSystemPanel.setLayout(new FlowLayout(0));
        this.mJobNumber = new FancyTextField("", 20);
        this.mJobNumber.setInteger(20);
        this.mTime = new FancyTextField("", 20);
        this.mTime.setFloat(20);
        this.mSystemR = new FancyTextField("", 10);
        this.mSystemR.setFloat(20);
        this.mSystemX = new FancyTextField("", 10);
        this.mSystemX.setFloat(20);
        this.mSystemN = new FancyTextField("", 10);
        this.mSystemN.setFloat(20);
        szSystemPanel.add(new Label("Whole System"));
        szSystemPanel.add(this.mJobNumber);
        szSystemPanel.add(this.mTime);
        szSystemPanel.add(this.mSystemR);
        szSystemPanel.add(this.mSystemX);
        szSystemPanel.add(this.mSystemN);
        borderpanel.add(szSystemPanel);
        Panel szServerResultPanel = new Panel();
        szServerResultPanel.setLayout(new FlowLayout(0));
        szServerResultPanel.add(new Label("Server              "));
        szServerResultPanel.add(new Label("  Average Utilization       "));
        szServerResultPanel.add(new Label("    Average Queue Length       "));
        szServerResultPanel.add(new Label("Average Response Time"));
        borderpanel.add(szServerResultPanel);
        this.mServerAvgU = new FancyTextField[this.mCpuNumber];
        this.mServerAvgQLen = new FancyTextField[this.mCpuNumber];
        this.mServerAvgResponse = new FancyTextField[this.mCpuNumber];
        int i = 0;
        while (i < this.mCpuNumber) {
            Panel szServerPanel = new Panel();
            szServerPanel.setLayout(new FlowLayout(0));
            String tempString = new String();
            tempString = i == 0 ? new String("Cpu:        ") : (i == 1 ? new String("Channel:") : new String("Disk " + (i - 1) + ":    "));
            Label szServerIdentificationLabel = new Label(tempString);
            this.mServerAvgU[i] = new FancyTextField("", 20);
            this.mServerAvgU[i].setFloat(20);
            this.mServerAvgQLen[i] = new FancyTextField("", 20);
            this.mServerAvgQLen[i].setFloat(20);
            this.mServerAvgResponse[i] = new FancyTextField("", 20);
            this.mServerAvgResponse[i].setFloat(20);
            szServerPanel.add(szServerIdentificationLabel);
            szServerPanel.add(this.mServerAvgU[i]);
            szServerPanel.add(this.mServerAvgQLen[i]);
            szServerPanel.add(this.mServerAvgResponse[i]);
            borderpanel.add(szServerPanel);
            ++i;
        }
        Panel szButtonPanel = new Panel();
        this.mEnterButtonCancel = new EnterButton("Cancel", this);
        szButtonPanel.add(this.mEnterButtonCancel);
        this.add("Center", borderpanel);
        this.add("South", szButtonPanel);
        this.enableDiskEntries(activeDiskNumber);
    }

    public void actionPerformed(ActionEvent actionevent) {
        Object obj = actionevent.getSource();
        if (obj.equals(this.mEnterButtonCancel)) {
            this.mBooleanCancelled = true;
            this.mShouldClose = true;
            this.setVisible(false);
            return;
        }
    }

    private void enableDiskEntries(int diskNumberToBeEnabled) {
        int i;
        this.mServerAvgU[0].setEnabled(true);
        this.mServerAvgU[0].setEditable(true);
        this.mServerAvgQLen[0].setEnabled(true);
        this.mServerAvgQLen[0].setEditable(true);
        this.mServerAvgResponse[0].setEnabled(true);
        this.mServerAvgResponse[0].setEditable(true);
        this.mServerAvgU[1].setEnabled(true);
        this.mServerAvgU[1].setEditable(true);
        this.mServerAvgQLen[1].setEnabled(true);
        this.mServerAvgQLen[1].setEditable(true);
        this.mServerAvgResponse[1].setEnabled(true);
        this.mServerAvgResponse[1].setEditable(true);
        this.mServerAvgU[2].setEnabled(true);
        this.mServerAvgU[2].setEditable(true);
        this.mServerAvgQLen[2].setEnabled(true);
        this.mServerAvgQLen[2].setEditable(true);
        this.mServerAvgResponse[2].setEnabled(true);
        this.mServerAvgResponse[2].setEditable(true);
        int start = 2;
        if (diskNumberToBeEnabled > 1) {
            i = 1;
            while (i < diskNumberToBeEnabled) {
                this.mServerAvgU[i + start].setEnabled(true);
                this.mServerAvgU[i + start].setEditable(true);
                this.mServerAvgQLen[i + start].setEnabled(true);
                this.mServerAvgQLen[i + start].setEditable(true);
                this.mServerAvgResponse[i + start].setEnabled(true);
                this.mServerAvgResponse[i + start].setEditable(true);
                ++i;
            }
        }
        if (diskNumberToBeEnabled < 8) {
            i = diskNumberToBeEnabled;
            while (i < 8) {
                this.mServerAvgU[i + start].setEnabled(false);
                this.mServerAvgU[i + start].setEditable(false);
                this.mServerAvgQLen[i + start].setEnabled(false);
                this.mServerAvgQLen[i + start].setEditable(false);
                this.mServerAvgResponse[i + start].setEnabled(false);
                this.mServerAvgResponse[i + start].setEditable(false);
                ++i;
            }
        }
    }

    public Insets getInsets() {
        Insets insets = super.getInsets();
        return new Insets(insets.top + 5, insets.left + 10, insets.bottom + 5, insets.right + 10);
    }

    public void initResultDialog() {
        int i = 0;
        while (i < this.mCpuNumber) {
            this.mServerAvgU[i].setText("0.0");
            this.mServerAvgQLen[i].setText("0.0");
            this.mServerAvgResponse[i].setText("0.0");
            ++i;
        }
    }

    public void setVisible(boolean flag) {
        if (flag) {
            this.pack();
            this.setLocation(this.mFrame.getLocation().x + 10, this.mFrame.getLocation().y + 50);
        }
        super.setVisible(flag);
    }

    private final class SysWindow
    extends WindowAdapter {
        SysWindow() {
        }

        public void windowClosing(WindowEvent windowevent) {
            ResultDialog.this.mBooleanCancelled = true;
            ResultDialog.this.setVisible(false);
        }
    }

}