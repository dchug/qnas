package kcmdg.sntp;

import java.applet.Applet;
import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.LayoutManager;
import java.awt.Panel;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.PrintStream;
import kcmdg.sntp.FancyTextField;
import kcmdg.sntp.InfoStorage;
import moondog.dialogs.MessageBox;
import moondog.widgets.BorderPanel;
import moondog.widgets.EnterButton;
import simu1.Options;
import simu6.Applet1;

public class SetupDialog
extends Frame
implements ActionListener,
ItemListener {
    Label label;
    EnterButton mEnterButtonCancel;
    EnterButton mEnterButtonOK;
    Checkbox mCheckboxPoissonDistribution;
    Checkbox mCheckboxUniformDistribution;
    Checkbox mCheckboxConstantDistribution;
    Checkbox[] mCheckboxCpuPoissonDistribution;
    Checkbox[] mCheckboxCpuUniformDistribution;
    Checkbox[] mCheckboxCpuConstantDistribution;
    FancyTextField mTxfAvgArrivA;
    FancyTextField mTxfAvgArrivS;
    FancyTextField mTxfRecycleProbability = null;
    FancyTextField[] mCpuAvgServA;
    FancyTextField[] mCpuAvgServS;
    FancyTextField[] mTxfPV;
    FancyTextField mSlowMotionAvgServTime;
    FancyTextField mTxfJPRINT;
    FancyTextField mTxfWSNum;
    FancyTextField mTxfProcessorNum;
    FancyTextField mTxfNumJobs;
    public boolean mShouldClose = false;
    int mCpuNumber;
    boolean mBooleanCancelled;
    InfoStorage mInfoStorage;
    Frame mFrame;
    Applet mParent = null;

    public SetupDialog(InfoStorage infoStorage, int cpuNumber, Applet parent) {
        super("setup");
        this.mFrame = this;
        this.setBackground(Color.lightGray);
        this.mCpuNumber = cpuNumber;
        SysWindow aSysWindow = new SysWindow();
        this.addWindowListener(aSysWindow);
        BorderPanel borderpanel = new BorderPanel("Setup");
        GridLayout gridbaglayout = new GridLayout(3 + this.mCpuNumber, 1);
        borderpanel.setLayout(gridbaglayout);
        Panel panel_1 = new Panel();
        panel_1.setLayout(new GridLayout(1, 3));
        Panel panel_1_1 = new Panel();
        panel_1_1.setLayout(new GridLayout(1, 3));
        Label label1 = new Label("  Number of events between displaying numerical results:");
        panel_1.add(label1);
        Panel panel_2 = new Panel();
        panel_2.setLayout(new FlowLayout(0));
        Panel panel_4 = new Panel();
        panel_4.setLayout(new FlowLayout(0));
        this.mTxfJPRINT = new FancyTextField("", 10);
        this.mTxfJPRINT.setInteger(5);
        panel_2.add(this.mTxfJPRINT);
        panel_2.add(new Label(""));
        panel_1.add(panel_2);
        panel_1.add(new Label("                                 "));
        Label label2 = new Label("  Average service time used for animation:");
        panel_1_1.add(label2);
        this.mSlowMotionAvgServTime = new FancyTextField("", 10);
        this.mSlowMotionAvgServTime.setInteger(10);
        Label label3 = new Label("milliseconds");
        panel_4.add(this.mSlowMotionAvgServTime);
        panel_4.add(label3);
        panel_1_1.add(panel_4);
        panel_1_1.add(new Label("                                 "));
        borderpanel.add(panel_1);
        borderpanel.add(panel_1_1);
        Panel panel_3 = new Panel();
        panel_3.setLayout(new FlowLayout(0));
        Label label4 = new Label("Interarrival distribution:");
        this.mCheckboxPoissonDistribution = new Checkbox("Exponential (A=mean)");
        this.mCheckboxPoissonDistribution.addItemListener(this);
        this.mCheckboxUniformDistribution = new Checkbox("Uniform (from A to B)", true);
        this.mCheckboxUniformDistribution.addItemListener(this);
        this.mCheckboxConstantDistribution = new Checkbox("Constant (A)               ", true);
        this.mCheckboxConstantDistribution.addItemListener(this);
        Label label6 = new Label("A = ");
        this.mTxfAvgArrivA = new FancyTextField("", 14);
        this.mTxfAvgArrivA.setFloat(8);
        Label label8 = new Label("B = ");
        this.mTxfAvgArrivS = new FancyTextField("", 14);
        this.mTxfAvgArrivS.setFloat(8);
        panel_3.add(label4);
        panel_3.add(this.mCheckboxPoissonDistribution);
        panel_3.add(this.mCheckboxUniformDistribution);
        panel_3.add(this.mCheckboxConstantDistribution);
        panel_3.add(label6);
        panel_3.add(this.mTxfAvgArrivA);
        panel_3.add(new Label("        "));
        panel_3.add(label8);
        panel_3.add(this.mTxfAvgArrivS);
        borderpanel.add(panel_3);
        if (this.mCpuNumber > 0) {
            this.mCpuAvgServA = new FancyTextField[this.mCpuNumber];
            this.mCpuAvgServS = new FancyTextField[this.mCpuNumber];
            this.mCheckboxCpuPoissonDistribution = new Checkbox[this.mCpuNumber];
            this.mCheckboxCpuUniformDistribution = new Checkbox[this.mCpuNumber];
            this.mCheckboxCpuConstantDistribution = new Checkbox[this.mCpuNumber];
        }
        int i = 0;
        while (i < this.mCpuNumber) {
            Panel panel = new Panel();
            panel.setLayout(new FlowLayout(0));
            String tempString = new String("Server " + new Integer(i + 1).toString() + " distribution:    ");
            Label label9 = new Label(tempString);
            this.mCheckboxCpuPoissonDistribution[i] = new Checkbox("Exponential (A=mean)");
            this.mCheckboxCpuPoissonDistribution[i].addItemListener(this);
            this.mCheckboxCpuUniformDistribution[i] = new Checkbox("Uniform (from A to B)", true);
            this.mCheckboxCpuUniformDistribution[i].addItemListener(this);
            this.mCheckboxCpuConstantDistribution[i] = new Checkbox("Constant (A)               ", true);
            this.mCheckboxCpuConstantDistribution[i].addItemListener(this);
            Label label100 = new Label("A = ");
            this.mCpuAvgServA[i] = new FancyTextField("", 14);
            this.mCpuAvgServA[i].setFloat(10);
            Label label101 = new Label("B = ");
            this.mCpuAvgServS[i] = new FancyTextField("", 14);
            this.mCpuAvgServS[i].setFloat(10);
            panel.add(label9);
            panel.add(this.mCheckboxCpuPoissonDistribution[i]);
            panel.add(this.mCheckboxCpuUniformDistribution[i]);
            panel.add(this.mCheckboxCpuConstantDistribution[i]);
            panel.add(label100);
            panel.add(this.mCpuAvgServA[i]);
            panel.add(new Label("        "));
            panel.add(label101);
            panel.add(this.mCpuAvgServS[i]);
            borderpanel.add(panel);
            ++i;
        }
        Panel panel1 = new Panel();
        this.mEnterButtonOK = new EnterButton("Accept", this);
        panel1.add(this.mEnterButtonOK);
        this.mEnterButtonCancel = new EnterButton("Cancel", this);
        panel1.add(this.mEnterButtonCancel);
        this.add("Center", borderpanel);
        this.add("South", panel1);
        System.out.println("before useInfoStorage");
        this.mParent = parent;
        this.useInfoStorage(infoStorage);
    }

    public SetupDialog(InfoStorage infoStorage, Applet parent) {
        super("setup");
        this.mCpuNumber = 1;
        this.mFrame = this;
        this.mParent = parent;
        this.setBackground(Color.lightGray);
        this.mCpuNumber = 1;
        SysWindow aSysWindow = new SysWindow();
        this.addWindowListener(aSysWindow);
        BorderPanel borderpanel = new BorderPanel("Setup");
        GridLayout gridbaglayout = new GridLayout(6, 1);
        borderpanel.setLayout(gridbaglayout);
        Panel panel_1 = new Panel();
        panel_1.setLayout(new GridLayout(1, 3));
        Panel panel_1_1 = new Panel();
        panel_1_1.setLayout(new GridLayout(1, 3));
        Panel panel_workstation_Num = new Panel();
        panel_workstation_Num.setLayout(new GridLayout(1, 3));
        Panel panel_processor_Num = new Panel();
        panel_processor_Num.setLayout(new GridLayout(1, 3));
        Panel panel_2 = new Panel();
        panel_2.setLayout(new FlowLayout(0));
        Panel panel_4 = new Panel();
        panel_4.setLayout(new FlowLayout(0));
        Panel panel4_workstation = new Panel();
        panel4_workstation.setLayout(new FlowLayout(0));
        Panel panel4_processor = new Panel();
        panel4_processor.setLayout(new FlowLayout(0));
        Label label1 = new Label("  Number of events between displaying numerical results:");
        panel_1.add(label1);
        this.mTxfJPRINT = new FancyTextField("", 10);
        this.mTxfJPRINT.setInteger(5);
        panel_2.add(this.mTxfJPRINT);
        panel_2.add(new Label(""));
        panel_1.add(panel_2);
        panel_1.add(new Label("                                 "));
        Label label2 = new Label("  Average service time used for animation:");
        panel_1_1.add(label2);
        this.mSlowMotionAvgServTime = new FancyTextField("", 10);
        this.mSlowMotionAvgServTime.setInteger(10);
        Label label3 = new Label("milliseconds");
        panel_4.add(this.mSlowMotionAvgServTime);
        panel_4.add(label3);
        panel_1_1.add(panel_4);
        panel_1_1.add(new Label("                                 "));
        panel_workstation_Num.add(new Label("  Number of workstations(1-8):"));
        this.mTxfWSNum = new FancyTextField("", 10);
        this.mTxfWSNum.setInteger(1);
        panel4_workstation.add(this.mTxfWSNum);
        panel4_workstation.add(new Label(""));
        panel_workstation_Num.add(panel4_workstation);
        panel_workstation_Num.add(new Label("                                 "));
        panel_processor_Num.add(new Label("  Number of processors(1-4):"));
        this.mTxfProcessorNum = new FancyTextField("", 10);
        this.mTxfProcessorNum.setInteger(1);
        panel4_processor.add(this.mTxfProcessorNum);
        panel4_processor.add(new Label(""));
        panel_processor_Num.add(panel4_processor);
        panel_processor_Num.add(new Label("                                 "));
        borderpanel.add(panel_1);
        borderpanel.add(panel_1_1);
        borderpanel.add(panel_workstation_Num);
        borderpanel.add(panel_processor_Num);
        Panel panel_3 = new Panel();
        panel_3.setLayout(new FlowLayout(0));
        Label label4 = new Label("Think time distribution:");
        this.mCheckboxPoissonDistribution = new Checkbox("Exponential (A=mean)");
        this.mCheckboxPoissonDistribution.addItemListener(this);
        this.mCheckboxUniformDistribution = new Checkbox("Uniform (from A to B)", true);
        this.mCheckboxUniformDistribution.addItemListener(this);
        this.mCheckboxConstantDistribution = new Checkbox("Constant (A)               ", true);
        this.mCheckboxConstantDistribution.addItemListener(this);
        Label label6 = new Label("A = ");
        this.mTxfAvgArrivA = new FancyTextField("", 14);
        this.mTxfAvgArrivA.setFloat(8);
        Label label8 = new Label("B = ");
        this.mTxfAvgArrivS = new FancyTextField("", 14);
        this.mTxfAvgArrivS.setFloat(8);
        panel_3.add(label4);
        panel_3.add(this.mCheckboxPoissonDistribution);
        panel_3.add(this.mCheckboxUniformDistribution);
        panel_3.add(this.mCheckboxConstantDistribution);
        panel_3.add(label6);
        panel_3.add(this.mTxfAvgArrivA);
        panel_3.add(new Label("        "));
        panel_3.add(label8);
        panel_3.add(this.mTxfAvgArrivS);
        borderpanel.add(panel_3);
        if (this.mCpuNumber > 0) {
            this.mCpuAvgServA = new FancyTextField[this.mCpuNumber];
            this.mCpuAvgServS = new FancyTextField[this.mCpuNumber];
            this.mCheckboxCpuPoissonDistribution = new Checkbox[this.mCpuNumber];
            this.mCheckboxCpuUniformDistribution = new Checkbox[this.mCpuNumber];
            this.mCheckboxCpuConstantDistribution = new Checkbox[this.mCpuNumber];
        }
        int i = 0;
        while (i < this.mCpuNumber) {
            Panel panel = new Panel();
            panel.setLayout(new FlowLayout(0));
            String tempString = new String("Processor distribution:");
            Label label9 = new Label(tempString);
            this.mCheckboxCpuPoissonDistribution[i] = new Checkbox("Exponential (A=mean)");
            this.mCheckboxCpuPoissonDistribution[i].addItemListener(this);
            this.mCheckboxCpuUniformDistribution[i] = new Checkbox("Uniform (from A to B)", true);
            this.mCheckboxCpuUniformDistribution[i].addItemListener(this);
            this.mCheckboxCpuConstantDistribution[i] = new Checkbox("Constant (A)               ", true);
            this.mCheckboxCpuConstantDistribution[i].addItemListener(this);
            Label label100 = new Label("A = ");
            this.mCpuAvgServA[i] = new FancyTextField("", 14);
            this.mCpuAvgServA[i].setFloat(10);
            Label label101 = new Label("B = ");
            this.mCpuAvgServS[i] = new FancyTextField("", 14);
            this.mCpuAvgServS[i].setFloat(10);
            panel.add(label9);
            panel.add(this.mCheckboxCpuPoissonDistribution[i]);
            panel.add(this.mCheckboxCpuUniformDistribution[i]);
            panel.add(this.mCheckboxCpuConstantDistribution[i]);
            panel.add(label100);
            panel.add(this.mCpuAvgServA[i]);
            panel.add(new Label("        "));
            panel.add(label101);
            panel.add(this.mCpuAvgServS[i]);
            borderpanel.add(panel);
            ++i;
        }
        Panel panel1 = new Panel();
        this.mEnterButtonOK = new EnterButton("Accept", this);
        panel1.add(this.mEnterButtonOK);
        this.mEnterButtonCancel = new EnterButton("Cancel", this);
        panel1.add(this.mEnterButtonCancel);
        this.add("Center", borderpanel);
        this.add("South", panel1);
        this.mParent = parent;
        this.useInfoStorage(infoStorage);
    }

    public SetupDialog(InfoStorage infoStorage, boolean needProbability, int cpuNumber, Applet parent) {
        super("setup");
        this.mFrame = this;
        this.setBackground(Color.lightGray);
        this.mCpuNumber = cpuNumber;
        SysWindow aSysWindow = new SysWindow();
        this.addWindowListener(aSysWindow);
        BorderPanel borderpanel = new BorderPanel("Setup");
        GridLayout gridbaglayout = new GridLayout(5 + this.mCpuNumber, 1);
        borderpanel.setLayout(gridbaglayout);
        Panel panel_1 = new Panel();
        panel_1.setLayout(new GridLayout(1, 3));
        Panel panel_1_1 = new Panel();
        panel_1_1.setLayout(new GridLayout(1, 3));
        Panel panel_1_2 = new Panel();
        panel_1_2.setLayout(new GridLayout(1, 3));
        Panel panel_2 = new Panel();
        panel_2.setLayout(new FlowLayout(0));
        Panel panel_4 = new Panel();
        panel_4.setLayout(new FlowLayout(0));
        Panel panel_4_1 = new Panel();
        panel_4_1.setLayout(new FlowLayout(0));
        Label label1 = new Label("  Number of events between displaying numerical results:");
        this.mTxfJPRINT = new FancyTextField("", 10);
        this.mTxfJPRINT.setInteger(5);
        panel_2.add(this.mTxfJPRINT);
        panel_2.add(new Label(""));
        panel_1.add(label1);
        panel_1.add(panel_2);
        panel_1.add(new Label("                                 "));
        Label label2 = new Label("  Average service time used for animation:");
        this.mSlowMotionAvgServTime = new FancyTextField("", 10);
        this.mSlowMotionAvgServTime.setInteger(10);
        panel_4.add(this.mSlowMotionAvgServTime);
        panel_4.add(new Label("milliseconds"));
        panel_1_1.add(label2);
        panel_1_1.add(panel_4);
        panel_1_1.add(new Label("                                 "));
        Label label10 = new Label("  Recycling probability:");
        this.mTxfRecycleProbability = new FancyTextField("", 10);
        this.mTxfRecycleProbability.setFloat(10);
        panel_4_1.add(this.mTxfRecycleProbability);
        panel_4_1.add(new Label("%"));
        panel_1_2.add(label10);
        panel_1_2.add(panel_4_1);
        panel_1_2.add(new Label("                                 "));
        borderpanel.add(panel_1);
        borderpanel.add(panel_1_1);
        borderpanel.add(panel_1_2);
        Panel panel_3 = new Panel();
        panel_3.setLayout(new FlowLayout(0));
        Label label4 = new Label("Interarrival distribution:");
        this.mCheckboxPoissonDistribution = new Checkbox("Exponential (A=mean)");
        this.mCheckboxPoissonDistribution.addItemListener(this);
        this.mCheckboxUniformDistribution = new Checkbox("Uniform (from A to B)", true);
        this.mCheckboxUniformDistribution.addItemListener(this);
        this.mCheckboxConstantDistribution = new Checkbox("Constant (A)               ", true);
        this.mCheckboxConstantDistribution.addItemListener(this);
        Label label6 = new Label("A = ");
        this.mTxfAvgArrivA = new FancyTextField("", 14);
        this.mTxfAvgArrivA.setFloat(8);
        Label label8 = new Label("B = ");
        this.mTxfAvgArrivS = new FancyTextField("", 14);
        this.mTxfAvgArrivS.setFloat(8);
        panel_3.add(label4);
        panel_3.add(this.mCheckboxPoissonDistribution);
        panel_3.add(this.mCheckboxUniformDistribution);
        panel_3.add(this.mCheckboxConstantDistribution);
        panel_3.add(label6);
        panel_3.add(this.mTxfAvgArrivA);
        panel_3.add(new Label("        "));
        panel_3.add(label8);
        panel_3.add(this.mTxfAvgArrivS);
        borderpanel.add(panel_3);
        if (this.mCpuNumber > 0) {
            this.mCpuAvgServA = new FancyTextField[this.mCpuNumber];
            this.mCpuAvgServS = new FancyTextField[this.mCpuNumber];
            this.mCheckboxCpuPoissonDistribution = new Checkbox[this.mCpuNumber];
            this.mCheckboxCpuUniformDistribution = new Checkbox[this.mCpuNumber];
            this.mCheckboxCpuConstantDistribution = new Checkbox[this.mCpuNumber];
        }
        int i = 0;
        while (i < this.mCpuNumber) {
            Panel panel = new Panel();
            panel.setLayout(new FlowLayout(0));
            String tempString = new String("Server " + new Integer(i + 1).toString() + " distribution:    ");
            Label label9 = new Label(tempString);
            this.mCheckboxCpuPoissonDistribution[i] = new Checkbox("Exponential (A=mean)");
            this.mCheckboxCpuPoissonDistribution[i].addItemListener(this);
            this.mCheckboxCpuUniformDistribution[i] = new Checkbox("Uniform (from A to B)", true);
            this.mCheckboxCpuUniformDistribution[i].addItemListener(this);
            this.mCheckboxCpuConstantDistribution[i] = new Checkbox("Constant (A)               ", true);
            this.mCheckboxCpuConstantDistribution[i].addItemListener(this);
            Label label100 = new Label("A = ");
            this.mCpuAvgServA[i] = new FancyTextField("", 14);
            this.mCpuAvgServA[i].setFloat(10);
            Label label101 = new Label("B = ");
            this.mCpuAvgServS[i] = new FancyTextField("", 14);
            this.mCpuAvgServS[i].setFloat(10);
            panel.add(label9);
            panel.add(this.mCheckboxCpuPoissonDistribution[i]);
            panel.add(this.mCheckboxCpuUniformDistribution[i]);
            panel.add(this.mCheckboxCpuConstantDistribution[i]);
            panel.add(label100);
            panel.add(this.mCpuAvgServA[i]);
            panel.add(new Label("        "));
            panel.add(label101);
            panel.add(this.mCpuAvgServS[i]);
            borderpanel.add(panel);
            ++i;
        }
        Panel panel1 = new Panel();
        this.mEnterButtonOK = new EnterButton("Accept", this);
        panel1.add(this.mEnterButtonOK);
        this.mEnterButtonCancel = new EnterButton("Cancel", this);
        panel1.add(this.mEnterButtonCancel);
        this.add("Center", borderpanel);
        this.add("South", panel1);
        this.mParent = parent;
        this.useInfoStorage(infoStorage);
    }

    public SetupDialog(InfoStorage infoStorage, boolean isCentralServerModel, Applet parent) {
        super("setup");
        this.mCpuNumber = 10;
        this.mFrame = this;
        this.setBackground(Color.lightGray);
        SysWindow aSysWindow = new SysWindow();
        this.addWindowListener(aSysWindow);
        BorderPanel borderpanel = new BorderPanel("Setup");
        GridLayout gridbaglayout = new GridLayout(14, 1);
        borderpanel.setLayout(gridbaglayout);
        Panel panel_1 = new Panel();
        panel_1.setLayout(new GridLayout(1, 3));
        Panel panel_1_1 = new Panel();
        panel_1_1.setLayout(new GridLayout(1, 3));
        Panel panel_workstation_Num = new Panel();
        panel_workstation_Num.setLayout(new GridLayout(1, 3));
        Panel panel_jobs_Num = new Panel();
        panel_jobs_Num.setLayout(new GridLayout(1, 3));
        Panel panel_2 = new Panel();
        panel_2.setLayout(new FlowLayout(0));
        Panel panel_4 = new Panel();
        panel_4.setLayout(new FlowLayout(0));
        Panel panel4_workstation = new Panel();
        panel4_workstation.setLayout(new FlowLayout(0));
        Panel panel4_jobnum = new Panel();
        panel4_jobnum.setLayout(new FlowLayout(0));
        Label label1 = new Label("  Number of events between displaying numerical results:");
        panel_1.add(label1);
        this.mTxfJPRINT = new FancyTextField("", 10);
        this.mTxfJPRINT.setInteger(5);
        panel_2.add(this.mTxfJPRINT);
        panel_2.add(new Label(""));
        panel_1.add(panel_2);
        panel_1.add(new Label("                                 "));
        Label label2 = new Label("  Average service time used for animation:");
        panel_1_1.add(label2);
        this.mSlowMotionAvgServTime = new FancyTextField("", 10);
        this.mSlowMotionAvgServTime.setInteger(10);
        Label label3 = new Label("milliseconds");
        panel_4.add(this.mSlowMotionAvgServTime);
        panel_4.add(label3);
        panel_1_1.add(panel_4);
        panel_1_1.add(new Label("                                 "));
        panel_workstation_Num.add(new Label("  Number of disks(1-8):"));
        this.mTxfWSNum = new FancyTextField("", 10);
        this.mTxfWSNum.setInteger(1);
        panel4_workstation.add(this.mTxfWSNum);
        panel4_workstation.add(new Label(""));
        panel_workstation_Num.add(panel4_workstation);
        panel_workstation_Num.add(new Label("                                 "));
        panel_jobs_Num.add(new Label("  Number of jobs:"));
        this.mTxfNumJobs = new FancyTextField("", 10);
        this.mTxfNumJobs.setInteger(10);
        panel4_jobnum.add(this.mTxfNumJobs);
        panel4_jobnum.add(new Label(""));
        panel_jobs_Num.add(panel4_jobnum);
        panel_jobs_Num.add(new Label("                                 "));
        borderpanel.add(panel_1);
        borderpanel.add(panel_1_1);
        borderpanel.add(panel_workstation_Num);
        borderpanel.add(panel_jobs_Num);
        Panel panel_3 = new Panel();
        panel_3.setLayout(new FlowLayout(0));
        Label label4 = new Label("Cpu distribution:        ");
        this.mCheckboxPoissonDistribution = new Checkbox("Exponential (A=mean)");
        this.mCheckboxPoissonDistribution.addItemListener(this);
        this.mCheckboxUniformDistribution = new Checkbox("Uniform (from A to B)", true);
        this.mCheckboxUniformDistribution.addItemListener(this);
        this.mCheckboxConstantDistribution = new Checkbox("Constant (A)               ", true);
        this.mCheckboxConstantDistribution.addItemListener(this);
        Label label6 = new Label("A = ");
        this.mTxfAvgArrivA = new FancyTextField("", 14);
        this.mTxfAvgArrivA.setFloat(8);
        Label label8 = new Label("B = ");
        this.mTxfAvgArrivS = new FancyTextField("", 14);
        this.mTxfAvgArrivS.setFloat(8);
        panel_3.add(label4);
        panel_3.add(this.mCheckboxPoissonDistribution);
        panel_3.add(this.mCheckboxUniformDistribution);
        panel_3.add(this.mCheckboxConstantDistribution);
        panel_3.add(label6);
        panel_3.add(this.mTxfAvgArrivA);
        panel_3.add(new Label("        "));
        panel_3.add(label8);
        panel_3.add(this.mTxfAvgArrivS);
        this.mCpuAvgServA = new FancyTextField[this.mCpuNumber];
        this.mCpuAvgServS = new FancyTextField[this.mCpuNumber];
        this.mTxfPV = new FancyTextField[this.mCpuNumber];
        this.mCheckboxCpuPoissonDistribution = new Checkbox[this.mCpuNumber];
        this.mCheckboxCpuUniformDistribution = new Checkbox[this.mCpuNumber];
        this.mCheckboxCpuConstantDistribution = new Checkbox[this.mCpuNumber];
        SymKey aSymKey = new SymKey();
        int i = 0;
        while (i < this.mCpuNumber) {
            Panel panel = new Panel();
            panel.setLayout(new FlowLayout(0));
            String tempString = new String();
            tempString = i == 0 ? new String("Cpu:        ") : (i == 1 ? new String("Channel:") : new String("Disk " + (i - 1) + ":    "));
            Label label9 = new Label(tempString);
            this.mCheckboxCpuPoissonDistribution[i] = new Checkbox("Exp. (A=mean)");
            this.mCheckboxCpuPoissonDistribution[i].addItemListener(this);
            this.mCheckboxCpuUniformDistribution[i] = new Checkbox("Uni. (A to B)", true);
            this.mCheckboxCpuUniformDistribution[i].addItemListener(this);
            this.mCheckboxCpuConstantDistribution[i] = new Checkbox("Const. (A)           ", true);
            this.mCheckboxCpuConstantDistribution[i].addItemListener(this);
            Label label100 = new Label("A = ");
            this.mCpuAvgServA[i] = new FancyTextField("", 10);
            this.mCpuAvgServA[i].setFloat(10);
            Label label101 = new Label("B = ");
            this.mCpuAvgServS[i] = new FancyTextField("", 10);
            this.mCpuAvgServS[i].setFloat(10);
            Label label102 = new Label("Visits ");
            this.mTxfPV[i] = new FancyTextField("", 10);
            this.mTxfPV[i].setInteger(10);
            panel.add(label9);
            panel.add(this.mCheckboxCpuPoissonDistribution[i]);
            panel.add(this.mCheckboxCpuUniformDistribution[i]);
            panel.add(this.mCheckboxCpuConstantDistribution[i]);
            panel.add(label100);
            panel.add(this.mCpuAvgServA[i]);
            panel.add(new Label("        "));
            panel.add(label101);
            panel.add(this.mCpuAvgServS[i]);
            panel.add(new Label("        "));
            panel.add(label102);
            if (i == 0 || i == 1) {
                this.mTxfPV[i].setInteger(12);
                this.mTxfPV[i].setEnabled(false);
                this.mTxfPV[i].setEditable(false);
                panel.add(this.mTxfPV[i]);
            } else {
                panel.add(this.mTxfPV[i]);
                this.mTxfPV[i].addKeyListener(aSymKey);
            }
            borderpanel.add(panel);
            ++i;
        }
        Panel panel1 = new Panel();
        this.mEnterButtonOK = new EnterButton("Accept", this);
        panel1.add(this.mEnterButtonOK);
        this.mEnterButtonCancel = new EnterButton("Cancel", this);
        panel1.add(this.mEnterButtonCancel);
        this.add("Center", borderpanel);
        this.add("South", panel1);
        this.mTxfWSNum.addKeyListener(aSymKey);
        this.useInfoStorage(infoStorage);
        try {
            Integer szDiskNumber = Integer.valueOf(this.mTxfWSNum.getText().trim());
            if (szDiskNumber < 1 || szDiskNumber > 8) {
                MessageBox.showError(this.mFrame, "Disk number should be between 1 and 8.");
            } else {
                this.enableDiskEntries(szDiskNumber);
            }
        }
        catch (NumberFormatException v0) {
            MessageBox.showError(this.mFrame, "NumberFormatException in szDiskNumber.");
            this.mTxfWSNum.requestFocus();
        }
        this.mParent = parent;
    }

    public void actionPerformed(ActionEvent actionevent) {
        Object obj = actionevent.getSource();
        if (obj.equals(this.mEnterButtonCancel)) {
            this.mBooleanCancelled = true;
            this.mShouldClose = true;
            this.setVisible(false);
            this.dispose();
        }
        if (obj.equals(this.mEnterButtonOK)) {
            this.mBooleanCancelled = false;
            this.mShouldClose = true;
            if (this.resetInfoStorage()) {
                if (this.mParent instanceof simu1.Applet1) {
                    ((simu1.Applet1)this.mParent).options.resetInfoStorage(this.getData());
                } else if (this.mParent instanceof simu3.Applet1) {
                    ((simu3.Applet1)this.mParent).options.resetInfoStorage(this.getData());
                } else if (this.mParent instanceof simu4.Applet1) {
                    ((simu4.Applet1)this.mParent).options.resetInfoStorage(this.getData());
                } else if (this.mParent instanceof Applet1) {
                    ((Applet1)this.mParent).options.resetInfoStorage(this.getData());
                } else if (this.mParent instanceof simu7.Applet1) {
                    ((simu7.Applet1)this.mParent).options.resetInfoStorage(this.getData());
                }
                this.setVisible(false);
            }
        } else {
            this.mShouldClose = false;
        }
    }

    private void enableDiskEntries(int diskNumberToBeEnabled) {
        int i;
        this.mCpuAvgServA[0].setEnabled(true);
        this.mCpuAvgServA[0].setEditable(true);
        this.mCpuAvgServS[0].setEnabled(true);
        this.mCpuAvgServS[0].setEditable(true);
        this.mCheckboxCpuPoissonDistribution[0].setEnabled(true);
        this.mCheckboxCpuUniformDistribution[0].setEnabled(true);
        this.mCheckboxCpuConstantDistribution[0].setEnabled(true);
        this.mCpuAvgServA[1].setEnabled(true);
        this.mCpuAvgServA[1].setEditable(true);
        this.mCpuAvgServS[1].setEnabled(true);
        this.mCpuAvgServS[1].setEditable(true);
        this.mCheckboxCpuPoissonDistribution[1].setEnabled(true);
        this.mCheckboxCpuUniformDistribution[1].setEnabled(true);
        this.mCheckboxCpuConstantDistribution[1].setEnabled(true);
        this.mCpuAvgServA[2].setEnabled(true);
        this.mCpuAvgServA[2].setEditable(true);
        this.mCpuAvgServS[2].setEnabled(true);
        this.mCpuAvgServS[2].setEditable(true);
        this.mCheckboxCpuPoissonDistribution[2].setEnabled(true);
        this.mCheckboxCpuUniformDistribution[2].setEnabled(true);
        this.mCheckboxCpuConstantDistribution[2].setEnabled(true);
        int start = 2;
        if (diskNumberToBeEnabled > 1) {
            i = 1;
            while (i < diskNumberToBeEnabled) {
                this.mCpuAvgServA[i + start].setEnabled(true);
                this.mCpuAvgServA[i + start].setEditable(true);
                this.mCpuAvgServS[i + start].setEnabled(true);
                this.mCpuAvgServS[i + start].setEditable(true);
                this.mTxfPV[i + start].setEnabled(true);
                this.mTxfPV[i + start].setEditable(true);
                this.mCheckboxCpuPoissonDistribution[i + start].setEnabled(true);
                this.mCheckboxCpuUniformDistribution[i + start].setEnabled(true);
                this.mCheckboxCpuConstantDistribution[i + start].setEnabled(true);
                ++i;
            }
        }
        if (diskNumberToBeEnabled < 8) {
            i = diskNumberToBeEnabled;
            while (i < 8) {
                this.mCpuAvgServA[i + start].setEnabled(false);
                this.mCpuAvgServA[i + start].setEditable(false);
                this.mCpuAvgServS[i + start].setEnabled(false);
                this.mCpuAvgServS[i + start].setEditable(false);
                this.mTxfPV[i + start].setEnabled(false);
                this.mTxfPV[i + start].setEditable(false);
                this.mCheckboxCpuPoissonDistribution[i + start].setEnabled(false);
                this.mCheckboxCpuUniformDistribution[i + start].setEnabled(false);
                this.mCheckboxCpuConstantDistribution[i + start].setEnabled(false);
                ++i;
            }
        }
    }

    public InfoStorage getData() {
        if (this.mBooleanCancelled) {
            this.mInfoStorage = null;
        }
        return this.mInfoStorage;
    }

    public Insets getInsets() {
        Insets insets = super.getInsets();
        return new Insets(insets.top + 5, insets.left + 10, insets.bottom + 5, insets.right + 10);
    }

    public void itemStateChanged(ItemEvent itemevent) {
        Object obj = itemevent.getSource();
        if (obj.equals(this.mCheckboxPoissonDistribution)) {
            this.mCheckboxPoissonDistribution.setState(true);
            this.mCheckboxUniformDistribution.setState(false);
            this.mCheckboxConstantDistribution.setState(false);
            return;
        }
        if (obj.equals(this.mCheckboxUniformDistribution)) {
            this.mCheckboxUniformDistribution.setState(true);
            this.mCheckboxPoissonDistribution.setState(false);
            this.mCheckboxConstantDistribution.setState(false);
            return;
        }
        if (obj.equals(this.mCheckboxConstantDistribution)) {
            this.mCheckboxPoissonDistribution.setState(false);
            this.mCheckboxUniformDistribution.setState(false);
            this.mCheckboxConstantDistribution.setState(true);
            return;
        }
        int i = 0;
        while (i < this.mCpuNumber) {
            if (obj.equals(this.mCheckboxCpuPoissonDistribution[i])) {
                this.mCheckboxCpuPoissonDistribution[i].setState(true);
                this.mCheckboxCpuUniformDistribution[i].setState(false);
                this.mCheckboxCpuConstantDistribution[i].setState(false);
                return;
            }
            if (obj.equals(this.mCheckboxCpuUniformDistribution[i])) {
                this.mCheckboxCpuPoissonDistribution[i].setState(false);
                this.mCheckboxCpuUniformDistribution[i].setState(true);
                this.mCheckboxCpuConstantDistribution[i].setState(false);
                return;
            }
            if (obj.equals(this.mCheckboxCpuConstantDistribution[i])) {
                this.mCheckboxCpuPoissonDistribution[i].setState(false);
                this.mCheckboxCpuUniformDistribution[i].setState(false);
                this.mCheckboxCpuConstantDistribution[i].setState(true);
                return;
            }
            ++i;
        }
    }

    public void recalculateCPUvisits() {
        int totalDiskVisits = 0;
        int totalCPUVisits = 0;
        int i = 2;
        while (i < this.mCpuNumber) {
            if (this.mTxfPV[i].isEnabled() && this.mTxfPV[i].getText().trim().length() > 0) {
                totalDiskVisits += Integer.parseInt(this.mTxfPV[i].getText().trim());
            }
            ++i;
        }
        totalCPUVisits = totalDiskVisits + 1;
        this.mTxfPV[0].setEditable(true);
        this.mTxfPV[0].setText(Integer.toString(totalCPUVisits));
        this.mTxfPV[0].setEditable(false);
        this.mTxfPV[1].setEditable(true);
        this.mTxfPV[1].setText(Integer.toString(totalDiskVisits));
        this.mTxfPV[1].setEditable(false);
    }

    private boolean resetInfoStorage() {
        Integer szJPRINT;
        this.mInfoStorage = new InfoStorage();
        if (this.mTxfRecycleProbability != null) {
            Double szRecycleProbability;
            try {
                szRecycleProbability = Double.valueOf(this.mTxfRecycleProbability.getText().trim());
            }
            catch (NumberFormatException v0) {
                MessageBox.showError(this.mFrame, "NumberFormatException in recycleProbability.");
                this.mTxfRecycleProbability.requestFocus();
                return false;
            }
            double szIntRecycleProbability = szRecycleProbability.intValue();
            if (szIntRecycleProbability < 0.0 || szIntRecycleProbability > 100.0) {
                MessageBox.showError(this.mFrame, "The Recycle Probability should be between [0.0--100.0].");
                this.mTxfRecycleProbability.requestFocus();
                return false;
            }
            this.mInfoStorage.setRecycleProbability(szIntRecycleProbability);
        }
        if (this.mTxfNumJobs != null) {
            Integer szNumJobs;
            try {
                szNumJobs = Integer.valueOf(this.mTxfNumJobs.getText().trim());
            }
            catch (NumberFormatException v1) {
                MessageBox.showError(this.mFrame, "NumberFormatException in job num.");
                this.mTxfNumJobs.requestFocus();
                return false;
            }
            int szIntNumJobs = szNumJobs;
            if (szIntNumJobs <= 0) {
                MessageBox.showError(this.mFrame, "The job number should be positive.");
                this.mTxfNumJobs.requestFocus();
                return false;
            }
            this.mInfoStorage.mNumJobs = szIntNumJobs;
        }
        try {
            this.mInfoStorage.setRates(this.mTxfAvgArrivA.getText().trim(), this.mTxfAvgArrivS.getText().trim());
        }
        catch (Exception ex) {
            MessageBox.showError(this.mFrame, ex.getMessage());
            this.mTxfAvgArrivA.requestFocus();
            return false;
        }
        this.mInfoStorage.mArrivalDistribution = this.mCheckboxPoissonDistribution.getState() ? 3 : (this.mCheckboxUniformDistribution.getState() ? 2 : 5);
        try {
            this.mInfoStorage.setModeNumericalValues(this.mSlowMotionAvgServTime.getText().trim());
        }
        catch (Exception ex) {
            MessageBox.showError(this.mFrame, ex.getMessage());
            return false;
        }
        int i = 0;
        while (i < this.mCpuNumber) {
            try {
                this.mInfoStorage.setServiceTime(this.mCpuAvgServA[i].getText().trim(), this.mCpuAvgServS[i].getText().trim(), i);
            }
            catch (Exception ex) {
                MessageBox.showError(this.mFrame, ex.getMessage());
                this.mCpuAvgServA[i].requestFocus();
                return false;
            }
            if (this.mTxfPV != null) {
                try {
                    Double szPV = Double.valueOf(this.mTxfPV[i].getText().trim());
                    this.mInfoStorage.mPV[i] = szPV.intValue();
                }
                catch (NumberFormatException v2) {
                    MessageBox.showError(this.mFrame, "NumberFormatException in #i disk's P or V parameter.");
                    this.mTxfPV[i].requestFocus();
                    return false;
                }
                catch (Exception ex) {
                    MessageBox.showError(this.mFrame, ex.getMessage());
                    this.mTxfPV[i].requestFocus();
                    return false;
                }
            }
            this.mInfoStorage.mServDistribution[i] = this.mCheckboxCpuPoissonDistribution[i].getState() ? 3 : (this.mCheckboxCpuUniformDistribution[i].getState() ? 2 : 5);
            ++i;
        }
        try {
            szJPRINT = Integer.valueOf(this.mTxfJPRINT.getText().trim());
        }
        catch (NumberFormatException v3) {
            MessageBox.showError(this.mFrame, "NumberFormatException in JPRINT.");
            this.mTxfJPRINT.requestFocus();
            return false;
        }
        this.mInfoStorage.mJPRINT = szJPRINT;
        if (this.mTxfWSNum != null) {
            Integer szWorkstationNum;
            if (this.mTxfWSNum.getText().trim().length() <= 0) {
                MessageBox.showError(this.mFrame, "Line 3 must be filled.");
                return false;
            }
            try {
                szWorkstationNum = Integer.valueOf(this.mTxfWSNum.getText().trim());
            }
            catch (NumberFormatException v4) {
                MessageBox.showError(this.mFrame, "NumberFormatException in szWorkstationNum.");
                this.mTxfWSNum.requestFocus();
                return false;
            }
            if (szWorkstationNum < 1 || szWorkstationNum > 8) {
                MessageBox.showError(this.mFrame, "Workstation number should be between 1 and 8.");
                this.mTxfWSNum.requestFocus();
                return false;
            }
            this.mInfoStorage.mWSNum = szWorkstationNum;
        }
        if (this.mTxfProcessorNum != null) {
            Integer szProcessorNum;
            try {
                szProcessorNum = Integer.valueOf(this.mTxfProcessorNum.getText().trim());
            }
            catch (NumberFormatException v5) {
                MessageBox.showError(this.mFrame, "NumberFormatException in szProcessorNum.");
                this.mTxfProcessorNum.requestFocus();
                return false;
            }
            if (szProcessorNum < 1 || szProcessorNum > 4) {
                MessageBox.showError(this.mFrame, "Processor number should be between 1 and 4.");
                this.mTxfProcessorNum.requestFocus();
                return false;
            }
            this.mInfoStorage.mProcessorNum = szProcessorNum;
        }
        this.mInfoStorage.printInfo();
        return true;
    }

    public void setVisible(boolean flag) {
        if (flag) {
            this.pack();
            this.setLocation(this.mFrame.getLocation().x + 10, this.mFrame.getLocation().y + 50);
        }
        super.setVisible(flag);
    }

    private void useInfoStorage(InfoStorage infoStorage) {
        switch (infoStorage.mArrivalDistribution) {
            case 2: {
                this.mCheckboxPoissonDistribution.setState(false);
                this.mCheckboxUniformDistribution.setState(true);
                this.mCheckboxConstantDistribution.setState(false);
                break;
            }
            case 3: {
                this.mCheckboxPoissonDistribution.setState(true);
                this.mCheckboxUniformDistribution.setState(false);
                this.mCheckboxConstantDistribution.setState(false);
                break;
            }
            case 5: {
                this.mCheckboxPoissonDistribution.setState(false);
                this.mCheckboxUniformDistribution.setState(false);
                this.mCheckboxConstantDistribution.setState(true);
                break;
            }
        }
        if (this.mTxfRecycleProbability != null) {
            this.mTxfRecycleProbability.setText(String.valueOf(infoStorage.mRecycleProbability));
        }
        this.mTxfAvgArrivA.setText(String.valueOf(infoStorage.mAvgArrivalA));
        this.mTxfAvgArrivS.setText(String.valueOf(infoStorage.mAvgArrivalS));
        this.mSlowMotionAvgServTime.setText(String.valueOf(infoStorage.mSlowModelAvgServTime));
        if (this.mTxfNumJobs != null) {
            this.mTxfNumJobs.setText(String.valueOf(infoStorage.mNumJobs));
        }
        int i = 0;
        while (i < this.mCpuNumber) {
            switch (infoStorage.mServDistribution[i]) {
                case 2: {
                    this.mCheckboxCpuPoissonDistribution[i].setState(false);
                    this.mCheckboxCpuUniformDistribution[i].setState(true);
                    this.mCheckboxCpuConstantDistribution[i].setState(false);
                    break;
                }
                case 3: {
                    this.mCheckboxCpuPoissonDistribution[i].setState(true);
                    this.mCheckboxCpuUniformDistribution[i].setState(false);
                    this.mCheckboxCpuConstantDistribution[i].setState(false);
                    break;
                }
                case 5: {
                    this.mCheckboxCpuPoissonDistribution[i].setState(false);
                    this.mCheckboxCpuUniformDistribution[i].setState(false);
                    this.mCheckboxCpuConstantDistribution[i].setState(true);
                    break;
                }
            }
            this.mCpuAvgServA[i].setText(String.valueOf(infoStorage.mAvgServA[i]));
            this.mCpuAvgServS[i].setText(String.valueOf(infoStorage.mAvgServS[i]));
            if (this.mTxfPV != null) {
                this.mTxfPV[i].setText(String.valueOf(infoStorage.mPV[i]));
            }
            ++i;
        }
        this.mTxfJPRINT.setText(String.valueOf(infoStorage.mJPRINT));
        if (this.mTxfWSNum != null) {
            this.mTxfWSNum.setText(String.valueOf(infoStorage.mWSNum));
        }
        if (this.mTxfProcessorNum != null) {
            this.mTxfProcessorNum.setText(String.valueOf(infoStorage.mProcessorNum));
        }
    }

    private final class SysWindow
    extends WindowAdapter {
        SysWindow() {
        }

        public void windowClosing(WindowEvent windowevent) {
            SetupDialog.this.mBooleanCancelled = true;
            SetupDialog.this.mShouldClose = true;
            SetupDialog.this.setVisible(false);
            SetupDialog.this.dispose();
        }
    }

    class SymKey
    extends KeyAdapter {
        SymKey() {
        }

        public void keyReleased(KeyEvent event) {
            Object object = event.getSource();
            if (object == SetupDialog.this.mTxfWSNum) {
                if (SetupDialog.this.mTxfWSNum.getText().trim().length() <= 0) {
                    return;
                }
                try {
                    Integer szDiskNumber = Integer.valueOf(SetupDialog.this.mTxfWSNum.getText().trim());
                    if (szDiskNumber < 1 || szDiskNumber > 8) {
                        MessageBox.showError(SetupDialog.this.mFrame, "Disk number should be between 1 and 8.");
                        SetupDialog.this.mTxfWSNum.setText("1");
                        SetupDialog.this.enableDiskEntries(1);
                    } else {
                        SetupDialog.this.enableDiskEntries(szDiskNumber);
                    }
                }
                catch (NumberFormatException v0) {
                    MessageBox.showError(SetupDialog.this.mFrame, "NumberFormatException in szDiskNumber.");
                    SetupDialog.this.mTxfWSNum.requestFocus();
                    return;
                }
            }
            if (object == SetupDialog.this.mTxfWSNum) {
                SetupDialog.this.recalculateCPUvisits();
                SetupDialog.this.mTxfWSNum.requestFocus();
            } else if (object == SetupDialog.this.mTxfPV[2] || object == SetupDialog.this.mTxfPV[3] || object == SetupDialog.this.mTxfPV[4] || object == SetupDialog.this.mTxfPV[5] || object == SetupDialog.this.mTxfPV[6] || object == SetupDialog.this.mTxfPV[7] || object == SetupDialog.this.mTxfPV[8] || object == SetupDialog.this.mTxfPV[9]) {
                String visitsStringValue = null;
                visitsStringValue = ((FancyTextField)object).getText();
                if (visitsStringValue != null && visitsStringValue.trim().length() > 0) {
                    try {
                        Integer nDiskVisitsNumber = Integer.valueOf(visitsStringValue.trim());
                        if (nDiskVisitsNumber < 0) {
                            MessageBox.showError(SetupDialog.this.mFrame, "Disk Visits number should be positive integer number.");
                            ((FancyTextField)object).setText("0");
                            ((FancyTextField)object).requestFocus();
                            return;
                        }
                    }
                    catch (NumberFormatException v1) {
                        MessageBox.showError(SetupDialog.this.mFrame, "Disk Visits number should be positive integer number.");
                        ((FancyTextField)object).setText("0");
                        ((FancyTextField)object).requestFocus();
                        return;
                    }
                }
                SetupDialog.this.recalculateCPUvisits();
            }
        }
    }



}