package kcmdg.sntp;

public interface StaticVariable {
    public static final int CONSTANT_ARRIVALDISTRIBUTION = 5;
    public static final int POISSON_ARRIVALDISTRIBUTION = 3;
    public static final int UNIFORM_ARRIVALDISTRIBUTION = 2;
    public static final int QUEUECAPACITY = 10;
    public static final double AVGARRIVALTIME_A = 1000.0;
    public static final double AVGARRIVALTIME_S = 0.0;
    public static final double AVGSERVICETIME_A = 500.0;
    public static final double AVGSERVICETIME_S = 0.0;
    public static final int SLOW_SIMULATIONMODEL = 1001;
    public static final int INTERRUPT_SIMULATIONMODEL = 1002;
    public static final double RECYCLE_PROBABILITY = 0.0;
    public static final int CPU_NUMBER = 30;
    public static final int SLOW_MODE_STANDARD_AVG_SERV_TIME = 500;
    public static final int JPRINT = 1000;
    public static final int WORKSTATIONNUM = 5;
    public static final int PROCESSORNUM = 2;
    public static final int JOBNUM = 12;
    public static final double PV = 1.0;
}

