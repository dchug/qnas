package kcmdg.sntp;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Event;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Panel;
import java.awt.TextArea;

@SuppressWarnings("serial")
public class ConfirmDialog
extends Dialog {
    protected Button btnYes;
    protected Button btnNo;
    protected static Frame createdFrame;
    protected String message;
    protected TextArea messageLabel;
    protected Panel top;
    protected Panel down;

    @SuppressWarnings("deprecation")
	public ConfirmDialog(String msg, Frame parent) {
        super(parent, true);
        this.message = msg;
        this.btnYes = new Button("CORRECT");
        this.btnNo = new Button("WRONG");
        this.messageLabel = new TextArea(this.message);
        this.setBackground(Color.yellow);
        this.setLayout(new BorderLayout());
        this.setFont(new Font("TimesRoman", 1, 18));
        this.message = msg;
        this.top = new Panel();
        this.down = new Panel();
        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints constraints = new GridBagConstraints();
        this.top.setLayout(gridbag);
        this.top.setFont(new Font("TimesRoman", 1, 18));
        this.messageLabel.setEditable(false);
        constraints.fill = 1;
        constraints.anchor = 10;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
        constraints.gridwidth = 0;
        gridbag.setConstraints(this.messageLabel, constraints);
        this.top.add(this.messageLabel);
        this.add("North", this.top);
        this.down.setLayout(new FlowLayout());
        this.down.setFont(new Font("TimesRoman", 1, 18));
        this.btnYes.setBackground(Color.red);
        this.btnNo.setBackground(Color.red);
        this.down.add(this.btnYes);
        this.down.add(this.btnNo);
        this.add("South", this.down);
        this.reshape(200, 200, 500, 500);
    }

    @SuppressWarnings("deprecation")
	public boolean action(Event evt, Object whichAction) {
        this.hide();
        if (createdFrame != null) {
            createdFrame.hide();
        }
        return true;
    }

    @SuppressWarnings("deprecation")
	public static void createConfirmDialog(String dialogString) {
        if (createdFrame == null) {
            createdFrame = new Frame("Dialog");
        }
        ConfirmDialog conDialog = new ConfirmDialog(dialogString, createdFrame);
        createdFrame.resize(conDialog.size().width, conDialog.size().height);
        conDialog.show();
    }
}

