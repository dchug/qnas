package kcmdg.sntp;

import java.awt.Color;
import java.awt.Frame;
import java.awt.TextField;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.PrintStream;
import kcmdg.sntp.ConfirmDialog;

public class FancyTextField
extends TextField {
    protected int nCount;
    protected int nLength;
    protected boolean isInteger = false;
    protected boolean isDate = false;
    protected boolean isFloat = false;
    protected boolean isString = false;
    protected boolean bSecondTwoFlag = false;
    protected boolean bFirstOneFlag = false;
    protected boolean bThirdThreeFlag = false;
    protected boolean bSixthOneFlag = false;
    protected boolean bSixthNineFlag = false;
    protected boolean bAllow31Flag = false;
    protected boolean bThirdTwoFlag = false;
    protected boolean b29Flag = false;

    FancyTextField() {
        SymKey aSymKey = new SymKey();
        this.addKeyListener(aSymKey);
    }

    FancyTextField(String s, int len) {
        super(s, len);
        SymKey aSymKey = new SymKey();
        this.addKeyListener(aSymKey);
    }

    void FancyTextField_KeyPress(KeyEvent event) {
        if (this.isString) {
            this.checkLength(event);
        }
        if (this.isInteger && this.checkDigit(event)) {
            this.checkLength(event);
        }
        if (this.isFloat && this.checkFloat(event)) {
            this.checkLength(event);
        }
        if (this.isDate && this.checkDate(event)) {
            this.checkLength(event);
        }
    }

    public void SetUnEditable() {
        if (this.isEditable()) {
            this.setBackground(Color.white);
            this.setEditable(false);
        }
    }

    boolean checkDate(KeyEvent event) {
        String szWhole;
        char cUserType = event.getKeyChar();
        int nKeyCode = event.getKeyCode();
        int nPos = this.getCaretPosition();
        if (nPos == 0 && nKeyCode != 8 && nKeyCode != 127) {
            if (cUserType == '1' || cUserType == '0') {
                if (!this.isEditable()) {
                    this.setEditable(true);
                }
                this.bFirstOneFlag = cUserType == '1';
                return true;
            }
            this.SetUnEditable();
            return false;
        }
        if (nPos == 1 && nKeyCode != 8 && nKeyCode != 127) {
            if (super.getText().charAt(0) == '0' && cUserType == '0') {
                this.SetUnEditable();
                return false;
            }
            if (this.bFirstOneFlag) {
                if (cUserType == '1' || cUserType == '2' || cUserType == '0') {
                    if (!this.isEditable()) {
                        this.setEditable(true);
                    }
                    this.bAllow31Flag = cUserType == '0' || cUserType == '2';
                    return true;
                }
                this.SetUnEditable();
                return false;
            }
            this.bSecondTwoFlag = cUserType == '2';
            this.bAllow31Flag = cUserType == '1' || cUserType == '3' || cUserType == '5' || cUserType == '7' || cUserType == '8';
            return this.checkDigit(event);
        }
        if (nPos == 3 && nKeyCode != 8 && nKeyCode != 127) {
            if (cUserType == '0' || cUserType == '1' || cUserType == '2' || cUserType == '3') {
                if (this.bSecondTwoFlag) {
                    if (cUserType == '3') {
                        if (this.isEditable()) {
                            this.setEditable(false);
                        }
                        return false;
                    }
                    this.bThirdTwoFlag = cUserType == '2';
                }
                if (!this.isEditable()) {
                    this.setEditable(true);
                }
                this.bThirdThreeFlag = cUserType == '3';
                return true;
            }
            this.SetUnEditable();
            return false;
        }
        if (nPos == 4 && nKeyCode != 8 && nKeyCode != 127) {
            if (super.getText().charAt(3) == '0' && cUserType == '0') {
                this.SetUnEditable();
                return false;
            }
            if (this.bThirdThreeFlag) {
                if (cUserType == '0') {
                    if (!this.isEditable()) {
                        this.setEditable(true);
                    }
                    return true;
                }
                if (cUserType == '1' && this.bAllow31Flag) {
                    if (!this.isEditable()) {
                        this.setEditable(true);
                    }
                    return true;
                }
                this.SetUnEditable();
                return false;
            }
            if (cUserType == '9' && this.bSecondTwoFlag && this.bThirdTwoFlag) {
                if (!this.isEditable()) {
                    this.setEditable(true);
                }
                this.b29Flag = true;
                return true;
            }
            this.b29Flag = false;
            return this.checkDigit(event);
        }
        if (nPos == 6 && nKeyCode != 8 && nKeyCode != 127) {
            if (cUserType == '1' || cUserType == '2' || cUserType == '9' || cUserType == '8' || cUserType == '7' || cUserType == '6' || cUserType == '5') {
                this.bSixthOneFlag = cUserType == '1';
                this.bSixthNineFlag = cUserType == '9' || cUserType == '8' || cUserType == '7' || cUserType == '6' || cUserType == '5';
                this.nLength = !this.bSixthNineFlag ? 10 : 8;
                if (!this.isEditable()) {
                    this.setEditable(true);
                }
                return true;
            }
            this.SetUnEditable();
            return false;
        }
        if (nPos == 7 && nKeyCode != 8 && nKeyCode != 127) {
            if (this.bSixthOneFlag) {
                if (cUserType != '9') {
                    if (this.isEditable()) {
                        this.setEditable(false);
                    }
                    return false;
                }
            } else {
                szWhole = super.getText();
                if (this.checkDigit(event)) {
                    String year = "19" + szWhole.charAt(6) + cUserType;
                    int nYear = new Integer(year);
                    if (nYear % 4 != 0 && this.b29Flag) {
                        this.SetUnEditable();
                        return false;
                    }
                    if (!this.isEditable()) {
                        this.setEditable(true);
                    }
                    return true;
                }
            }
        }
        if (nPos == 9 && nKeyCode != 8 && nKeyCode != 127) {
            szWhole = super.getText();
            if (this.checkDigit(event)) {
                String year = "" + szWhole.charAt(6) + szWhole.charAt(7) + szWhole.charAt(8) + cUserType;
                int nYear = new Integer(year);
                if (nYear % 4 != 0 && this.b29Flag) {
                    this.SetUnEditable();
                    return false;
                }
                if (!this.isEditable()) {
                    this.setEditable(true);
                }
                return true;
            }
        }
        if ((nPos == 2 || nPos == 5) && nKeyCode != 8 && nKeyCode != 127) {
            if (cUserType == '/') {
                if (!this.isEditable()) {
                    this.setEditable(true);
                }
                return true;
            }
            this.SetUnEditable();
            return false;
        }
        return this.checkDigit(event);
    }

    boolean checkDigit(KeyEvent event) {
        char cUserTyped = event.getKeyChar();
        int nKeyCode = event.getKeyCode();
        if (!Character.isDigit(cUserTyped) && nKeyCode != 8 && nKeyCode != 127) {
            this.SetUnEditable();
            return false;
        }
        if (!this.isEditable()) {
            this.setEditable(true);
        }
        return true;
    }

    boolean checkFloat(KeyEvent event) {
        char cUserType = event.getKeyChar();
        if (cUserType == '.') {
            if (this.isPeriodTypedAlready()) {
                this.SetUnEditable();
                return false;
            }
            if (!this.isEditable()) {
                this.setEditable(true);
            }
            return true;
        }
        return this.checkDigit(event);
    }

    boolean checkLength(KeyEvent event) {
        if (this.getText() != null) {
            this.nCount = this.getText().length();
        }
        int nPos = this.getCaretPosition();
        int nKeyCode = event.getKeyCode();
        if (nKeyCode == 8) {
            if (nPos > 0 && this.nCount > 0) {
                --this.nCount;
                if (!this.isEditable()) {
                    this.setEditable(true);
                }
            }
        } else if (nKeyCode == 127) {
            if (nPos < this.nCount && this.nCount > 0) {
                --this.nCount;
                if (!this.isEditable()) {
                    this.setEditable(true);
                }
            }
        } else if (nKeyCode != 18 && nKeyCode != 3 && nKeyCode != 20 && nKeyCode != 12 && nKeyCode != 17 && nKeyCode != 28 && nKeyCode != 35 && nKeyCode != 10 && nKeyCode != 27 && nKeyCode != 156 && nKeyCode != 36 && nKeyCode != 155 && nKeyCode != 21 && nKeyCode != 25 && nKeyCode != 37 && nKeyCode != 157 && nKeyCode != 31 && nKeyCode != 29 && nKeyCode != 144 && nKeyCode != 34 && nKeyCode != 33 && nKeyCode != 19 && nKeyCode != 154 && nKeyCode != 39 && nKeyCode != 145 && nKeyCode != 16 && nKeyCode != 9 && nKeyCode != 0 && nKeyCode != 40 && nKeyCode != 38) {
            if (this.getSelectedText() != null && this.getSelectedText().length() > 0) {
                this.nCount -= this.getSelectedText().length();
            }
            if (this.nCount < this.nLength) {
                if (!this.isEditable()) {
                    this.setEditable(true);
                }
                ++this.nCount;
            } else if (this.nCount >= this.nLength) {
                this.SetUnEditable();
                return false;
            }
        }
        return true;
    }

    void clear() {
        this.setText("");
    }

    /*
     * Enabled aggressive block sorting
     */
    boolean correctDecimal(float target, Frame windowFrame, int real, int decimal, String valueChecked) {
        DecimalErrorWindow error = new DecimalErrorWindow();
        int length = this.getText().trim().length();
        if (length == 0) {
            error.errorTemplate(windowFrame, "User Error", valueChecked, "Enter some decimal value !");
            return false;
        }
        if (this.invalidCharacters(windowFrame, length, error, valueChecked)) {
            error.errorTemplate(windowFrame, "User Error", valueChecked, "Invalid characters !");
            return false;
        }
        int dotIndex = this.getText().trim().indexOf(46);
        if (dotIndex > -1) {
            if (length == 1) {
                target = new Float("0.00").floatValue();
                return true;
            }
            if (dotIndex == 0) {
                if (this.correctDecimalPart(dotIndex, length, true, decimal)) {
                    target = new Float(this.getText().trim()).floatValue();
                    return true;
                }
                this.errorDecimalPart(windowFrame, error, decimal, valueChecked);
                return false;
            }
            if (length - 1 == dotIndex) {
                if (this.correctRealPart(dotIndex, length, true, real)) {
                    target = new Float(this.getText().trim()).floatValue();
                    return true;
                }
                this.errorRealPart(windowFrame, error, real, valueChecked);
                return false;
            }
            boolean realPart = this.correctRealPart(dotIndex, length, false, real);
            boolean decimalPart = this.correctDecimalPart(dotIndex, length, false, decimal);
            if (!realPart) {
                this.errorRealPart(windowFrame, error, real, valueChecked);
                return false;
            }
            if (!decimalPart) {
                this.errorDecimalPart(windowFrame, error, decimal, valueChecked);
                return false;
            }
            target = new Float(this.getText().trim()).floatValue();
            return true;
        }
        if (length <= real) {
            target = new Float(this.getText().trim()).floatValue();
            return true;
        }
        this.errorRealPart(windowFrame, error, real, valueChecked);
        return false;
    }

    boolean correctDecimalPart(int dotAt, int size, boolean begins, int allowed) {
        return size - 1 - dotAt <= allowed;
    }

    boolean correctRealPart(int dotAt, int size, boolean ends, int allowed) {
        return dotAt >= 1 && dotAt <= allowed;
    }

    void errorDecimalPart(Frame window, DecimalErrorWindow err, int allowedDigits, String variable) {
        err.errorTemplate(window, "User Error", variable, "No more than " + allowedDigits + " digits (after decimal point) are accepted!");
    }

    void errorRealPart(Frame window, DecimalErrorWindow err, int allowedDigits, String variable) {
        err.errorTemplate(window, "User Error", variable, "No more than " + allowedDigits + " digits (before decimal point) are accepted!");
    }

    boolean fullDate(Frame f, String type, String varia, String mess) {
        int slashOne = -2;
        int slashTwo = -2;
        int year1 = 2;
        int year2 = 4;
        int maxTwo = year1;
        if (this.getText().trim().length() == 0) {
            return true;
        }
        DecimalErrorWindow error = new DecimalErrorWindow();
        try {
            slashOne = this.getText().trim().indexOf(47);
        }
        catch (NullPointerException v0) {
            System.out.println("NullPointerException in FancyTextField.fullDate");
            return false;
        }
        if (slashOne == -1 || slashOne == 0 || slashOne > maxTwo) {
            error.errorTemplate(f, type, varia, mess);
            return false;
        }
        try {
            slashTwo = this.getText().trim().indexOf(47, slashOne + 1);
        }
        catch (NullPointerException v1) {
            System.out.println("NullPointerException#2 in FancyTextField.fullDate");
            return false;
        }
        int slashDifference = slashTwo - slashOne;
        if (slashTwo == -1 || slashDifference > maxTwo + 1 || slashDifference < maxTwo) {
            error.errorTemplate(f, type, varia, mess);
            return false;
        }
        int length = this.getText().trim().length();
        int yearLength = length - 1 - slashTwo;
        if (yearLength == year1 || yearLength == year2) {
            return true;
        }
        error.errorTemplate(f, type, varia, mess);
        return false;
    }

    public String getText() {
        String szOriginal = super.getText();
        String szNew = szOriginal.replace('?', '^');
        return szNew;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    boolean invalidCharacters(Frame window, int stringSize, DecimalErrorWindow err, String variable) {
        boolean seenDot = false;
        int index = 0;
        while (index < stringSize) {
            char element;
            try {
                element = this.getText().trim().charAt(index);
            }
            catch (IndexOutOfBoundsException v0) {
                System.out.println("IndexOutOfBoundsException");
                System.out.print(" inside FancyTextField.invalidCharacters !");
                try {
                    System.exit(-1);
                    return true;
                }
                catch (SecurityException v1) {
                    System.out.println("SecurityException !");
                }
                return true;
            }
            switch (element) {
                case '.': {
                    if (!seenDot) {
                        return true;
                    }
                    err.errorTemplate(window, "User Error", variable, "More than 1 dot in decimal Total Cube is not allowed");
                    return true;
                }
                default: {
                    return true;
                }
                case '0': 
                case '1': 
                case '2': 
                case '3': 
                case '4': 
                case '5': 
                case '6': 
                case '7': 
                case '8': 
                case '9': 
            }
            ++index;
        }
        return false;
    }

    boolean isPeriodTypedAlready() {
        String userTyped = this.getText().trim();
        boolean bFound = false;
        int index = 0;
        while (index < userTyped.length() && !bFound) {
            if (userTyped.charAt(index) == '.') {
                bFound = true;
            }
            ++index;
        }
        return bFound;
    }

    public void setDate() {
        this.isString = false;
        this.isInteger = false;
        this.isDate = true;
        this.isFloat = false;
        this.nLength = 10;
    }

    public void setFloat(int nLength) {
        this.isString = false;
        this.isInteger = false;
        this.isDate = false;
        this.isFloat = true;
        this.nLength = nLength;
    }

    public void setInteger(int nLength) {
        this.isString = false;
        this.isInteger = true;
        this.isDate = false;
        this.isFloat = false;
        this.nLength = nLength;
    }

    public void setString(int nStringLength) {
        this.isString = true;
        this.isInteger = false;
        this.isDate = false;
        this.isFloat = false;
        this.nLength = nStringLength;
    }

    public void setText(String szOriginal) {
        String szNew = szOriginal.replace('^', '?');
        super.setText(szNew);
    }

    class DecimalErrorWindow
    extends Frame {
        DecimalErrorWindow() {
        }

        void errorTemplate(Frame windowFrame, String errorType, String variable, String message) {
            ConfirmDialog okPromptDialog = new ConfirmDialog(message, windowFrame);
            okPromptDialog.show();
        }
    }

    class SymKey
    extends KeyAdapter {
        SymKey() {
        }

        public void keyPressed(KeyEvent event) {
            Object object = event.getSource();
            if (object == FancyTextField.this) {
                FancyTextField.this.FancyTextField_KeyPress(event);
            }
        }
    }

}
