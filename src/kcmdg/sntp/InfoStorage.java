package kcmdg.sntp;

import java.io.PrintStream;
import java.io.Serializable;

public class InfoStorage
implements Serializable {
    public int mQueueCapacity = 10;
    public double mRecycleProbability = 0.0;
    public int mSimulationModel = 1001;
    public int mSlowModelAvgServTime = 500;
    public int mArrivalDistribution = 3;
    public double mAvgArrivalA = 1000.0;
    public double mAvgArrivalS = 0.0;
    public int[] mServDistribution = new int[30];
    public double[] mAvgServA = new double[30];
    public double[] mAvgServS = new double[30];
    public int[] mPV = new int[10];
    public int mJPRINT = 1000;
    public int mWSNum = 5;
    public int mProcessorNum = 2;
    public int mNumJobs = 12;

    public InfoStorage() {
        int i = 0;
        while (i < 10) {
            this.mAvgServA[i] = 500.0;
            this.mAvgServS[i] = 0.0;
            this.mServDistribution[i] = 3;
            this.mPV[i] = 0;
            ++i;
        }
    }

    public void printInfo() {
        System.out.println("mArrivalDistribution = " + this.mArrivalDistribution);
        System.out.println("mQueueCapacity = " + this.mQueueCapacity);
        System.out.println("mAvgArrivalA = " + this.mAvgArrivalA);
        System.out.println("mAvgArrivalS = " + this.mAvgArrivalS);
        System.out.println("mSimulationModel = " + this.mSimulationModel);
        System.out.println("mSlowModelAvgServTime = " + this.mSlowModelAvgServTime);
        System.out.println("mRecycleProbability = " + this.mRecycleProbability);
        System.out.println("mJPRINT = " + this.mJPRINT);
        System.out.println("mWSNum = " + this.mWSNum);
        System.out.println("mProcessorNum = " + this.mProcessorNum);
        System.out.println("mNumJobs = " + this.mNumJobs);
    }

    public void setModeNumericalValues(String slowMotionAvgServTime) throws Exception {
        Integer szSlowMotionAvgServTime;
        try {
            szSlowMotionAvgServTime = Integer.valueOf(slowMotionAvgServTime);
        }
        catch (NumberFormatException v0) {
            throw new Exception("NumberFormatException in SlowMotionAvgServTime.");
        }
        this.mSlowModelAvgServTime = szSlowMotionAvgServTime;
    }

    public void setRates(String avgArrivalA, String avgArrivalS) throws Exception {
        Double szAvgArrivalA;
        Double szAvgArrivalS;
        try {
            szAvgArrivalA = Double.valueOf(avgArrivalA);
        }
        catch (NumberFormatException v0) {
            throw new Exception("NumberFormatException in inter-arrival rate's A parameter.");
        }
        this.mAvgArrivalA = szAvgArrivalA;
        try {
            szAvgArrivalS = Double.valueOf(avgArrivalS);
        }
        catch (NumberFormatException v1) {
            throw new Exception("NumberFormatException in inter-arrival rate's S parameter.");
        }
        this.mAvgArrivalS = szAvgArrivalS;
    }

    public void setRecycleProbability(double recycleProbability) {
        this.mRecycleProbability = recycleProbability;
    }

    public void setServiceTime(String avgServiceA, String avgServiceS, int i) throws Exception {
        Double szAvgServiceS;
        Double szAvgServiceA;
        try {
            szAvgServiceA = Double.valueOf(avgServiceA);
        }
        catch (NumberFormatException v0) {
            throw new Exception("NumberFormatException in service time parameter A.");
        }
        this.mAvgServA[i] = szAvgServiceA;
        try {
            szAvgServiceS = Double.valueOf(avgServiceS);
        }
        catch (NumberFormatException v1) {
            throw new Exception("NumberFormatException in service time parameter S.");
        }
        this.mAvgServS[i] = szAvgServiceS;
    }
}
