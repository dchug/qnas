package simu3;

import java.util.Vector;
import simu3.Task;

public class EventVector
extends Vector {
    static final int FOLLOW = 1;
    static final int PRECEDE = 2;

    EventVector() {
    }

    void insertTaskByEventTime(Task task) {
        if (task == null) {
            return;
        }
        if (this.size() <= 0) {
            this.addElement(task);
            return;
        }
        int szSize = this.size();
        int i = 0;
        System.out.println("New Task created");
        while (i < szSize) {
            if (((Task)this.elementAt((int)i)).mEventTime >= task.mEventTime) {
                this.insertElementAt(task, i);
                return;
            }
            ++i;
        }
        this.addElement(task);
    }
}

