package simu3;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import simu3.GraphCanvas;
import simu3.Task;

public class Disk
extends Canvas {
    public Point UpperLeftPoint;
    public Color color;
    public Point mCenter;
    public int mHeight;
    public int mWidth;
    public Point mLeftMidPoint;
    public Point mRightMidPoint;
    public boolean mHasTask;
    public Task mCurrentTask = null;
    public boolean noWaitTasks = false;
    public String mName;
    public int mProcessTime;
    GraphCanvas mParent;

    public Disk(int UpperLeftPoint_X, int UpperLeftPoint_Y, Color COLOR, GraphCanvas parent, int width, int height) {
        this.UpperLeftPoint = new Point(UpperLeftPoint_X, UpperLeftPoint_Y);
        this.color = COLOR;
        this.mHeight = height;
        this.mWidth = width;
        this.mCenter = new Point(UpperLeftPoint_X + (int)((double)this.mWidth / 2.0), UpperLeftPoint_Y + (int)((double)this.mHeight / 2.0));
        this.mHasTask = false;
        this.mParent = parent;
        this.mLeftMidPoint = new Point(UpperLeftPoint_X, this.mCenter.y);
        this.mRightMidPoint = new Point(UpperLeftPoint_X + this.mWidth, this.mCenter.y);
    }

    public Disk(Point c, Color COLOR, GraphCanvas parent, int width, int height) {
        this.UpperLeftPoint = new Point(c.x, c.y);
        this.color = COLOR;
        this.mHeight = height;
        this.mWidth = width;
        this.mCenter = new Point(c.x + (int)((double)this.mWidth / 2.0), c.y + (int)((double)this.mHeight / 2.0));
        this.mHasTask = false;
        this.mParent = parent;
        this.mLeftMidPoint = new Point(c.x, this.mCenter.y);
        this.mRightMidPoint = new Point(c.x + this.mWidth, this.mCenter.y);
    }

    public void drawDisk(Graphics g) {
        g.setColor(this.color);
        g.fillOval(this.UpperLeftPoint.x, this.UpperLeftPoint.y, this.mWidth, this.mHeight);
        g.setColor(Color.black);
        g.drawOval(this.UpperLeftPoint.x, this.UpperLeftPoint.y, this.mWidth, this.mHeight);
    }

    public Color getColor() {
        return this.color;
    }

    public Point getUpperLeftPoint() {
        return this.UpperLeftPoint;
    }

    public void paint(Graphics g) {
        this.drawDisk(g);
    }

    public void setColor(Color COLOR) {
        this.color = COLOR;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public void setProcessTime(int processTime) {
        this.mProcessTime = processTime;
    }

    public void setRunningTask(Task currentTask) {
        this.mCurrentTask = null;
        if (currentTask == null) {
            this.mHasTask = false;
            return;
        }
        this.mCurrentTask = currentTask;
        this.mHasTask = true;
    }

    public void setUpperLeftPoint(int X, int Y) {
        this.UpperLeftPoint.x = X;
        this.UpperLeftPoint.y = Y;
    }

    public void setUpperLeftPoint(Point c) {
        this.UpperLeftPoint.x = c.x;
        this.UpperLeftPoint.y = c.y;
    }
}
