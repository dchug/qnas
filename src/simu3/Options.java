package simu3;

import java.applet.Applet;
import java.applet.AppletContext;
import java.awt.Button;
import java.awt.Component;
import java.awt.Event;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.LayoutManager;
import java.awt.Panel;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.NumberFormat;
import kcmdg.sntp.InfoStorage;
import kcmdg.sntp.SetupDialog;
import simu3.Applet1;
import simu3.GraphCanvas;
import simu3.ResultFrame;

public class Options extends Panel implements Runnable 
{
    Button b1 = new Button("Initialize");
    Button b2 = new Button("Run");
    Button b3 = new Button("Num. Results");
    Button b4 = new Button("End");
    Button b5 = new Button("Help");
    Label mQNAS = new Label("QNAS");
    public SetupDialog szSetupDialog = null;
    static final int WAIT = 0;
    static final int RUN = 1;
    public int mRunFlag;
    public boolean mFromBeginning = true;
    Applet1 mParent;
    public InfoStorage mInfoStorage;
    int mCpuNumber;

    Options(InfoStorage infoStorage, Applet1 myparent, int szCpuNumber) 
    {
        this.mParent = myparent;
        this.mInfoStorage = infoStorage;
        this.mCpuNumber = szCpuNumber <= 0 ? 0 : szCpuNumber;
        this.mRunFlag = 0;
        this.setLayout(new GridLayout(6, 1, 0, 10));
        this.add(this.mQNAS);
        this.add(this.b5);
        this.add(this.b1);
        this.add(this.b2);
        this.add(this.b3);
        this.add(this.b4);
    }

    public boolean action(Event evt, Object arg) 
    {
        if (evt.target instanceof Button) 
        {
            if (((String)arg).equals("Initialize")) 
            {
                if (this.szSetupDialog == null) 
                {
                    this.szSetupDialog = new SetupDialog(this.mInfoStorage, true, this.mCpuNumber, this.mParent);
                }
                this.szSetupDialog.pack();
                this.szSetupDialog.show();
            }
            if (((String)arg).equals("Run")) 
            {
                this.mParent.graphcanvas.mRunFlag = this.mRunFlag = 1;
                this.b2.setLabel("Stop");
                this.b1.setEnabled(false);
                if (this.mFromBeginning) 
                {
                    NumberFormat mDoubleFormat = NumberFormat.getNumberInstance();
                    mDoubleFormat.setMaximumFractionDigits(3);
                    mDoubleFormat.setMinimumFractionDigits(3);
                    mDoubleFormat.setMaximumIntegerDigits(10);
                    NumberFormat mIntegerFormat = NumberFormat.getNumberInstance();
                    mIntegerFormat.setParseIntegerOnly(true);
                    this.mParent.mTableSorterFrame.setValueAt(mIntegerFormat.format(0), 0, 0);
                    this.mParent.mTableSorterFrame.setValueAt(mDoubleFormat.format(0.0), 0, 1);
                    this.mParent.mTableSorterFrame.setValueAt(mDoubleFormat.format(0.0), 0, 2);
                    this.mParent.mTableSorterFrame.setValueAt(mDoubleFormat.format(0.0), 0, 3);
                    this.mParent.mTableSorterFrame.setValueAt(mDoubleFormat.format(0.0), 0, 4);
                    this.mParent.mTableSorterFrame.setValueAt(mDoubleFormat.format(0.0), 0, 5);
                    this.mParent.mTableSorterFrame.setValueAt(mDoubleFormat.format(0.0), 0, 6);
                    this.mParent.mTableSorterFrame.setValueAt(mDoubleFormat.format(0.0), 0, 7);
                    this.mParent.mTableSorterFrame.setValueAt(mDoubleFormat.format(0.0), 0, 8);
                    this.mParent.mTableSorterFrame.setValueAt(mDoubleFormat.format(0.0), 0, 9);
                    this.mParent.mTableSorterFrame.setValueAt(mDoubleFormat.format(0.0), 0, 10);
                    this.mParent.mTableSorterFrame.setValueAt(mDoubleFormat.format(0.0), 0, 11);
                }
                this.mParent.mTableSorterFrame.show();
                this.mParent.graphcanvas.runalg(this.mFromBeginning);
                this.mFromBeginning = false;
            }
            if (((String)arg).equals("Stop")) 
            {
                this.mParent.graphcanvas.mRunFlag = this.mRunFlag = 0;
                this.b2.setLabel("Run");
            }
            if (((String)arg).equals("Num. Results")) 
            {
                if (this.mParent.mTableSorterFrame == null) 
                {
                    this.mParent.mTableSorterFrame = new ResultFrame(this.mParent);
                    this.mParent.mTableSorterFrame.setVisible(true);
                }
                this.mParent.mTableSorterFrame.show();
            }
            if (((String)arg).equals("End")) 
            {
                this.mFromBeginning = true;
                this.mRunFlag = 0;
                this.b2.setLabel("Run");
                this.b1.setEnabled(true);
                this.mParent.graphcanvas.clear();
            }
            if (((String)arg).equals("Help")) 
            {
                try 
                {
                    URL szCurrentAppletURL = this.mParent.getCodeBase();
                    String tempStr = szCurrentAppletURL.toString().trim();
                    tempStr = tempStr.concat("help.html");
                    szCurrentAppletURL = new URL(tempStr);
                    System.out.println("URL == " + tempStr);
                    this.mParent.getAppletContext().showDocument(szCurrentAppletURL, "Help");
                }
                catch (MalformedURLException me) 
                {
                    System.out.println("Error == " + me);
                }
                catch (Exception e) 
                {
                    System.out.println("Error == " + e);
                }
            }
        }
        return true;
    }

    public void resetInfoStorage(InfoStorage szInfoStorage) 
    {
        if (szInfoStorage != null) 
        {
            this.mInfoStorage.mQueueCapacity = szInfoStorage.mQueueCapacity;
            this.mInfoStorage.mSimulationModel = szInfoStorage.mSimulationModel;
            this.mInfoStorage.mSlowModelAvgServTime = szInfoStorage.mSlowModelAvgServTime;
            this.mInfoStorage.mArrivalDistribution = szInfoStorage.mArrivalDistribution;
            this.mInfoStorage.mAvgArrivalA = szInfoStorage.mAvgArrivalA;
            this.mInfoStorage.mAvgArrivalS = szInfoStorage.mAvgArrivalS;
            this.mInfoStorage.mRecycleProbability = szInfoStorage.mRecycleProbability;
            this.mInfoStorage.mJPRINT = szInfoStorage.mJPRINT;
            System.out.println("mInfoStorage.mSimulationModel == " + this.mInfoStorage.mSimulationModel);
            System.out.println("mInfoStorage.mSlowModelAvgServTime == " + this.mInfoStorage.mSlowModelAvgServTime);
            System.out.println("mInfoStorage.mArrivalDistribution == " + this.mInfoStorage.mArrivalDistribution);
            System.out.println("mInfoStorage.mAvgArrivalA == " + this.mInfoStorage.mAvgArrivalA);
            System.out.println("mInfoStorage.mAvgArrivalS == " + this.mInfoStorage.mAvgArrivalS);
            System.out.println("mInfoStorage.mRecycleProbability == " + this.mInfoStorage.mRecycleProbability);
            int i = 0;
            while (i < this.mCpuNumber) 
            {
                this.mInfoStorage.mServDistribution[i] = szInfoStorage.mServDistribution[i];
                this.mInfoStorage.mAvgServA[i] = szInfoStorage.mAvgServA[i];
                this.mInfoStorage.mAvgServS[i] = szInfoStorage.mAvgServS[i];
                System.out.println("mInfoStorage.mServDistribution[i] == " + this.mInfoStorage.mServDistribution[i]);
                System.out.println("mInfoStorage.mAvgServA[i] == " + this.mInfoStorage.mAvgServA[i]);
                System.out.println("mInfoStorage.mAvgServS[i] == " + this.mInfoStorage.mAvgServS[i]);
                ++i;
            }
        }
    }

    public void run() 
    {
        try 
        {
            Thread.currentThread();
            Thread.sleep(100);
        }
        catch (InterruptedException v0) 
        {
        }
        catch (Exception v1) {}
        this.mParent.graphcanvas.mFromBeginning = true;
        this.mParent.graphcanvas.init();
    }
}