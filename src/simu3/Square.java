package simu3;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import simu3.Node;
import simu3.Task;

public class Square
extends Canvas {
    public int height;
    public Point UpperLeftPoint;
    public Point mCenter;
    public Color color;
    public boolean mHasTask = false;
    public Task mCurrentTask = null;

    public Square(int LB_X, int LB_Y, int H, Color COLOR) {
        this.UpperLeftPoint = new Point(LB_X, LB_Y);
        this.height = H;
        this.color = COLOR;
        this.mCenter = new Point(this.UpperLeftPoint.x + this.height / 2, this.UpperLeftPoint.y + this.height / 2);
    }

    public Square(Point LB, int H, Color COLOR) {
        this.UpperLeftPoint = new Point(LB.x, LB.y);
        this.height = H;
        this.color = COLOR;
        this.mCenter = new Point(this.UpperLeftPoint.x + this.height / 2, this.UpperLeftPoint.y + this.height / 2);
    }

    public void drawSquare(Graphics g) {
        int upperLeft_x = this.UpperLeftPoint.x;
        int upperLeft_y = this.UpperLeftPoint.y;
        g.drawLine(upperLeft_x, upperLeft_y, upperLeft_x + this.height, upperLeft_y);
        g.setColor(this.color);
        g.drawRect(upperLeft_x, upperLeft_y, this.height, this.height);
        if (this.mHasTask && this.mCurrentTask != null) {
            this.mCurrentTask.setUpperLeftPoint(this.mCenter.x - this.mCurrentTask.radius, this.mCenter.y - this.mCurrentTask.radius);
            this.mCurrentTask.drawTask(g);
        }
    }

    public Color getColor() {
        return this.color;
    }

    public int getHeight() {
        return this.height;
    }

    public Point getUpperLeftPoint() {
        return this.UpperLeftPoint;
    }

    public void paint(Graphics g) {
        this.drawSquare(g);
    }

    public void setColor(Color COLOR) {
        this.color = COLOR;
    }

    public void setCurrTask(Task currTask) {
        this.mCurrentTask = null;
        if (currTask == null) {
            this.mHasTask = false;
            this.repaint();
            return;
        }
        this.mCurrentTask = currTask;
        this.mHasTask = true;
        this.repaint();
    }

    public void setHeight(int H) {
        this.height = H;
    }

    public void setUpperLeftPoint(int LB_X, int LB_Y) {
        this.UpperLeftPoint.x = LB_X;
        this.UpperLeftPoint.y = LB_Y;
    }

    public void setUpperLeftPoint(Point LB) {
        this.UpperLeftPoint.x = LB.x;
        this.UpperLeftPoint.y = LB.y;
    }
}
