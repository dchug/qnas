package simu3;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

public class Node {
    public final int STANDARD_RADIUS = 10;
    public Point UpperLeftPoint;
    public int radius;
    public Color color;

    public Node(int UpperLeftPoint_X, int UpperLeftPoint_Y, int RADIUS, Color COLOR) {
        if (RADIUS <= 0) {
            RADIUS = 10;
        }
        this.UpperLeftPoint = new Point(UpperLeftPoint_X, UpperLeftPoint_Y);
        this.radius = RADIUS;
        this.color = COLOR;
    }

    public Node(Point c, int RADIUS, Color COLOR) {
        if (RADIUS <= 0) {
            RADIUS = 10;
        }
        this.UpperLeftPoint = new Point(c.x, c.y);
        this.radius = RADIUS;
        this.color = COLOR;
    }

    public void drawNode(Graphics g) {
        g.setColor(this.color);
        int ovalHeight = 2 * this.radius;
        g.fillOval(this.UpperLeftPoint.x, this.UpperLeftPoint.y, ovalHeight, ovalHeight);
    }

    public Color getColor() {
        return this.color;
    }

    public int getRadius() {
        return this.radius;
    }

    public Point getUpperLeftPoint() {
        return this.UpperLeftPoint;
    }

    public void setColor(Color COLOR) {
        this.color = COLOR;
    }

    public void setRadius(int RADIUS) {
        if (RADIUS <= 0) {
            RADIUS = 10;
        }
        this.radius = RADIUS;
    }

    public void setUpperLeftPoint(int X, int Y) {
        this.UpperLeftPoint.x = X;
        this.UpperLeftPoint.y = Y;
    }

    public void setUpperLeftPoint(Point c) {
        this.UpperLeftPoint.x = c.x;
        this.UpperLeftPoint.y = c.y;
    }
}