package moondog.choice;

import java.awt.Button;
//import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
//import java.awt.LayoutManager;
import java.awt.Panel;
//import java.awt.Point;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
//import java.awt.event.FocusListener;
import java.util.Vector;
//import moondog.choice._1;
import moondog.choice._2;

@SuppressWarnings("serial")
public class ChoiceButton extends Panel implements _2,ActionListener {
    Button _0;
    int _1;
    String _2;
    @SuppressWarnings("rawtypes")
	Vector _3;
    TextField _4;
    String _5;
ChoiceButton choiceButton = null;
    public void actionPerformed(ActionEvent actionEvent) {
        
        if (actionEvent.getSource().equals(choiceButton._0)) {
            
            Container container = choiceButton.getParent();
            int n = choiceButton.getLocation().x;
            int n2 = choiceButton.getLocation().y;
            while (!(container instanceof Frame)) {
                n += container.getLocation().x;
                n2 += container.getLocation().y;
                container = container.getParent();
            }
            _1 _12 = new _1((Frame)container, choiceButton._5, choiceButton._3, choiceButton._1);
            _12.setLocation(container.getLocation().x + n, container.getLocation().y + n2);
            _12.setListener(choiceButton);
            _12.setVisible(true);
            _12.dispose();
        }
    }

   /* This is the original decompiler copy of source code
    * Not sure about var0 object and why it was used.
    * public void addItem(String string) {
        if (string.length() > 0) {
            if (!var0._3.contains(string)) 
            {
                var0._3.addElement(string);
                if (var0._2 == null) {
                    var0._4.setText(string);
                    var0._2 = string;
                    var0._1 = 0;
                }
            }
            var0._0.setEnabled(true);
        }
    }*/

    //This is the updated code from above
    @SuppressWarnings("unchecked")
	public void addItem(String string) {
        if (string.length() > 0) {
            if (!_3.contains(string)) 
            {
                _3.addElement(string);
                if (_2 == null) {
                    _4.setText(string);
                    _2 = string;
                    _1 = 0;
                }
            }
            _0.setEnabled(true);
        }
    }
    
   /* This is the original decompiler copy of source code
    * Not sure about var0 object and why it was used.
    * public void choiceAction(String string, int n) {
        var0._2 = string;
        var0._4.setText(string);
        var0._1 = n;
    }*/
    //Updated code of above method.
    public void choiceAction(String string, int n) {
        _2 = string;
        _4.setText(string);
        _1 = n;
    }

    @SuppressWarnings("rawtypes")
	public ChoiceButton(int n, String string) {
        //ChoiceButton choiceButton = null;
        choiceButton._5 = ChoiceButton._0(";\u00041\n\bp;>|8\u000ew");
        GridBagLayout gridBagLayout = new GridBagLayout();
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        choiceButton.setLayout(gridBagLayout);
        gridBagConstraints.insets = new Insets(2, 2, 2, 2);
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.gridwidth = 1;
        choiceButton._0 = new Button(string);
        choiceButton._0.setEnabled(false);
        choiceButton._4 = new TextField("", n);
        gridBagLayout.setConstraints(choiceButton._4, gridBagConstraints);
        choiceButton.add(choiceButton._4);
        gridBagConstraints.weighty = 0.0;
        gridBagConstraints.weightx = 0.0;
        gridBagConstraints.fill = 0;
        gridBagLayout.setConstraints(choiceButton._0, gridBagConstraints);
        choiceButton.add(choiceButton._0);
        choiceButton._3 = new Vector();
        choiceButton._0.addActionListener(choiceButton);
        //1 var5_5 = choiceButton.new 1();
        _1 var5_5 = choiceButton.new _1(); //modified above code from above line.
        choiceButton._4.addFocusListener(var5_5);
    }

    public ChoiceButton()
    {
        //ChoiceButton choiceButton = new ChoiceButton(20, ChoiceButton._0("\u0019Ey"));
    	choiceButton = new ChoiceButton(20, ChoiceButton._0("\u0019Ey")); //above line modified
    }

    public ChoiceButton(int n) {
        //ChoiceButton choiceButton = new ChoiceButton(n, ChoiceButton._0("\u0019Ey"));
    	choiceButton = new ChoiceButton(n, ChoiceButton._0("\u0019Ey")); //above line modified
    }

    /*public int getSelectedIndex() {
        return var0._1;
    }*/
    //Above method modified
    public int getSelectedIndex() {
        return _1;
    }

    
    /*public String getSelectedItem() {
        return var0._2;
    }*/
    
  //Above method modified
    public String getSelectedItem() {
        return _2;
    }

   
	public String getText() {
        //ChoiceButton choiceButton = null; //Removed unnecessary declaration.
        String string = choiceButton._4.getText();
        choiceButton.setText(string);
        return choiceButton._4.getText();
    }

    /*public void select(String string) {
        int n = 0;
        while (n < var0._3.size()) {
            if (string.equals((String)var0._3.elementAt(n))) {
                var0._2 = (String)var0._3.elementAt(n);
                var0._1 = n;
                var0._4.setText(var0._2);
                return;
            }
            ++n;
        }
    }*/
	//Modified the code from above
	public void select(String string) {
        int n = 0;
        while (n < _3.size()) {
            if (string.equals((String) _3.elementAt(n))) {
                _2 = (String) _3.elementAt(n);
                _1 = n;
                _4.setText(_2);
                return;
            }
            ++n;
        }
    }
	

   /* public void select(int n) {
        var0._2 = (String)var0._3.elementAt(n);
        var0._1 = n;
        var0._4.setText(var0._2);
    }*/
	
	//Modified code from above
	 public void select(int n) {
	        _2 = (String) _3.elementAt(n);
	        _1 = n;
	        _4.setText(_2);
	    }

    public void setEnabled(boolean bl) {
        //var0._4.setEnabled(bl); 
        _4.setEnabled(bl); //Removed var0
    }

    public void setText(String string) {
        if (string.length() > 0) {
            //ChoiceButton choiceButton = null; //removed unnecessary declaration.
            choiceButton.addItem(string);
            choiceButton.select(string);
            return;
        }
        choiceButton._4.setText("");
        choiceButton._1 = -1;
    }

    public void setTitle(String string) {
        //var0._5 = string;
        _5 = string; //removed var0
    }

    private static String _0(String string) {
        char[] arrc = string.toCharArray();
        char[] arrc2 = "j\u0017K(\u0014\u000b\u001b".toCharArray();
        int n = arrc.length;
        int n2 = arrc2.length;
        int n3 = 0;
        while (n3 < n) {
            int n4 = n3 % n2;
            int n5 = arrc[n3] - arrc2[n4];
            if (n5 <= 0) {
                n5 += 127;
            }
            arrc[n3] = (char)n5;
            ++n3;
        }
        return new String(arrc);
    }

    private final class _1 extends FocusAdapter
    {
        @SuppressWarnings("unused")
		_1 var0;
        
        //following constructor added
        public _1()
        {
        	var0 = new _1();
        }
       
        @SuppressWarnings("rawtypes")
		private _1(Frame frame, String _5, Vector _3, int _1) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        
        public void focusLost(FocusEvent focusEvent) {
            //var0.focusEvent.this.getText();
        }

        private void setLocation(int i, int i0) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        private void setListener(ChoiceButton choiceButton) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        private void setVisible(boolean b) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        private void dispose() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

}