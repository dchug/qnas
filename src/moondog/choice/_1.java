
package moondog.choice;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
//import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
//import java.awt.LayoutManager;
import java.awt.List;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
//import java.awt.event.KeyEvent;
//import java.awt.event.KeyListener;
import java.util.Enumeration;
import java.util.Vector;
import moondog.choice._2;

@SuppressWarnings("serial")
class _1 extends Dialog implements ActionListener {
    Button _0;
    List _1;
    _2 _2;
    Button _3;
    _1 _12 = null; //Declaration moved here from constructor

    @SuppressWarnings("deprecation")
	public _1(Frame frame, String string, @SuppressWarnings("rawtypes") Vector vector, int n) {
    	super(frame, string, true);
    	 //_1 _12 = null; declaration moved outside from here
        _12.setBackground(Color.lightGray);
        _12.setLayout(new BorderLayout());
        _12._1 = new List(Math.min(8, vector.size()), false);
        @SuppressWarnings("rawtypes")
		Enumeration enumeration = vector.elements();
        
        while (enumeration.hasMoreElements()) {
            _12._1.addItem((String)enumeration.nextElement());
        }
        //_12.add( _1._0("\u0002\u001cGg8\t"), _12._1);// _0 is method defined below inside this file.
        _12.add( _0("\u0002\u001cGg8\t"), _12._1); //Edited from above line.
        _12._1.select(n);
        _12._1.makeVisible(n);
        _12._3 = new Button("OK");
        //_12._0 = new Button(_1._0("\u0002\u0018GV8\u0003"));
        _12._0 = new Button(_0("\u0002\u0018GV8\u0003")); //Above line edited.
        Panel panel = new Panel();
        panel.add(_12._3);
        panel.add(_12._0);
        //_12.add(_1._0("\u0012&Ng;"), panel);
        _12.add(_0("\u0012&Ng;"), panel); //Above line edited
        _12.setSize(250, 200);
        _12._1.addActionListener(_12);
        _12._3.addActionListener(_12);
        _12._0.addActionListener(_12);
        //_1 var7_7 = _12.new _1(); //This line is updated to the line below
        key var7_7 = new key();
        _12._1.addKeyListener(var7_7);
    }

    public void actionPerformed(ActionEvent actionEvent) {
       // _1 _12; removed this line
        Object object = actionEvent.getSource();
        if (object.equals(_12._3) || object.equals(_12._1)) {
            String string = _12._1.getSelectedItem();
            int n = _12._1.getSelectedIndex();
            if (_12._2 != null) {
                _12._2.choiceAction(string, n);
            }
            _12.setVisible(false);
            return;
        }
        if (object.equals(_12._0)) {
            _12.setVisible(false);
        }
    }

   /* public void setListener(_2 _22) {
        var0._2 = _22;
    }
*/
    private static String _0(String string) {
        char[] arrc = string.toCharArray();
        char[] arrc2 = ">6XrR\u0016b2\u0005".toCharArray();
        int n = arrc.length;
        int n2 = arrc2.length;
        int n3 = 0;
        while (n3 < n) {
            int n4 = n3 % n2;
            int n5 = arrc[n3] - arrc2[n4];
            if (n5 <= 0) {
                n5 += 127;
            }
            arrc[n3] = (char)n5;
            ++n3;
        }
        return new String(arrc);
    }

   // private final class _1 extends KeyAdapter { //Changed the name of class from _1 to key. all the key object used inside the
   //class were _1. Name is changed because this private class is declared inside public class named _1. It is most probably confusing
   
    private final class key extends KeyAdapter {
    	//key var0;
       

      /*      @Override
       * Original copy of code. I modified the one below only.
        public void keyPressed(KeyEvent keyEvent) {
            if (keyEvent.getKeyCode() == 10) {
                String string = var0.key.this.key.getSelectedItem();
                int n = var0.key.this._1.getSelectedIndex();
                if (var0.key.this._2 != null) {
                    var0.key.this._2.choiceAction(string, n);
                }
                var0.key.this.dispose();
            }
        }*/

        /*      @Override
          public void keyPressed(KeyEvent keyEvent) {
              if (keyEvent.getKeyCode() == 10) {
                  String string = var0.key.this.key.getSelectedItem();
                  int n = var0.key.this._1.getSelectedIndex();
                  if (var0.key.this._2 != null) {
                      var0.key.this._2.choiceAction(string, n);
                  }
                  var0.key.this.dispose();
              }
          }*/
    }

}
