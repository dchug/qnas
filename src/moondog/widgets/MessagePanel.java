package moondog.widgets;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Panel;
//import java.awt.Toolkit;
import java.util.StringTokenizer;

@SuppressWarnings("serial")
public class MessagePanel extends Panel {
    int _0;
    public static final int _1 = 2;
    public static final int _2 = 0;
    public static final int _3 = 1;
    int _4;
    int _5;
    boolean _6;
    String[] _7;
    int _8;
    int _9;
    int _10;
    int _11;
    MessagePanel messagePanel = this;
    @SuppressWarnings("deprecation")
	private void _0() {
        //MessagePanel messagePanel = null;
        FontMetrics fontMetrics = messagePanel.getToolkit().getFontMetrics(messagePanel.getFont());
        messagePanel._5 = fontMetrics.getHeight();
        messagePanel._4 = fontMetrics.getAscent();
        messagePanel._8 = 0;
        int n = 0;
        while (n < messagePanel._9) {
            messagePanel._8 = Math.max(messagePanel._8, fontMetrics.stringWidth(messagePanel._7[n]));
            ++n;
        }
        messagePanel._6 = true;
    }

    public Dimension getMinimumSize() {
        //MessagePanel messagePanel = null;
        return messagePanel.getPreferredSize();
    }

    public Dimension getPreferredSize() {
        if (!_6) {
            _0();
        }
        return new Dimension(_8 + 2 * _10, _9*_5 + 2*_11);
    }

    public MessagePanel(String string) {
         messagePanel = new MessagePanel(string, 0);
        
    }

    public MessagePanel(String string, int n) {
        
        _0 = n;
        _11 = 10;
        _10 = 10;
        _0(string);
        if (this.getFont() == null) {
            this.setFont(new Font(MessagePanel._1("\u000e:duKuOvj"), 1, 12));
        }
    }

    public void paint(Graphics graphics) {
      
        if (!_6) {
            _0();
        }
        int n = this.getSize().width;
        int n2 = this.getSize().height;
        if (_0 == 1) {
            graphics.setColor(Color.darkGray);
            graphics.drawLine(2, 2, n - 5, 2);
            graphics.drawLine(2, 2, 2, n2 - 5);
            graphics.setColor(Color.white);
            graphics.drawLine(2, n2 - 5, n - 5, n2 - 5);
            graphics.drawLine(n - 5, n2 - 5, n - 5, 2);
        } else if (_0 == 2) {
            graphics.setColor(Color.darkGray);
            graphics.drawRect(0, 0, n - 2, n2 - 2);
            graphics.setColor(Color.white);
            graphics.drawRect(1, 1, n - 2, n2 - 2);
        }
        int n3 = _4 + (n2 - _9 *_5) / 2;
        graphics.setColor(this.getForeground());
        int n4 = 0;
        while (n4 < this._9) {
            graphics.drawString(this._7[n4], this._10, n3);
            ++n4;
            n3 += this._5;
        }
    }

    private void _0(String string) {
        StringTokenizer stringTokenizer = new StringTokenizer(string, "\n");
        _9 = stringTokenizer.countTokens();
        _7 = new String[_9];
        int n = 0;
        while (n < _9) {
            _7[n] = stringTokenizer.nextToken();
            ++n;
        }
    }

    public void setFont(Font font) {
        //MessagePanel messagePanel;
        super.setFont(font);
        _6 = false;
        repaint();
    }

    public void update(Graphics graphics) {
       
        paint(graphics);
    }

    private static String _1(String string) {
        char[] arrc = string.toCharArray();
        char[] arrc2 = "\u001aXu\u0002W\u0010\\\r\u0004".toCharArray();
        int n = arrc.length;
        int n2 = arrc2.length;
        int n3 = 0;
        while (n3 < n) {
            int n4 = n3 % n2;
            int n5 = arrc[n3] - arrc2[n4];
            if (n5 <= 0) {
                n5 += 127;
            }
            arrc[n3] = (char)n5;
            ++n3;
        }
        return new String(arrc);
    }
}