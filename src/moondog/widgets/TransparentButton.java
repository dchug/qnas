package moondog.widgets;

//import java.awt.Color;
//import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import moondog.widgets.PButton;

@SuppressWarnings("serial")
public class TransparentButton
extends PButton {
	  TransparentButton transparentButton = this;
    public void paint(Graphics graphics) {
      
        int n = transparentButton.getSize().width - 1;
        int n2 = transparentButton.getSize().height - 1;
        graphics.setClip(0, 0, transparentButton.getSize().width, transparentButton.getSize().height);
        if (transparentButton.isPressed()) {
            graphics.setColor(transparentButton.getBackground().darker().darker().darker().darker());
        } else {
            graphics.setColor(transparentButton.getBackground().brighter().brighter().brighter());
        }
        graphics.drawRect(0, 0, n, n2);
        graphics.drawRect(1, 1, n - 2, n2 - 2);
        if (!transparentButton.isPressed()) {
            graphics.setColor(transparentButton.getBackground().darker().darker().darker().darker());
        } else {
            graphics.setColor(transparentButton.getBackground().brighter().brighter().brighter());
        }
        graphics.drawLine(0, n2, n - 1, n2);
        graphics.drawLine(1, n2 - 1, n, n2 - 1);
        graphics.drawLine(n, 0, n, n2);
        graphics.drawLine(n - 1, 1, n - 1, n2 - 1);
        Font font = transparentButton.getFont();
        graphics.setFont(font);
        if (font != null) {
            FontMetrics fontMetrics = transparentButton.getFontMetrics(font);
            int n3 = n / 2 - fontMetrics.stringWidth(transparentButton._2) / 2;
            int n4 = n2 / 2 + fontMetrics.getMaxDescent() + 2;
            if (!transparentButton.isPressed()) {
                graphics.setColor(transparentButton.getBackground().brighter().brighter());
                graphics.drawString(transparentButton._2, n3 - 1, n4);
                if (transparentButton.isEnabled()) {
                    graphics.setColor(transparentButton.getForeground());
                } else {
                    graphics.setColor(transparentButton.getBackground().darker().darker());
                }
                graphics.drawString(transparentButton._2, n3, n4);
            } else {
                graphics.setColor(transparentButton.getBackground().brighter().brighter());
                graphics.drawString(transparentButton._2, n3, n4);
                graphics.setColor(transparentButton.getForeground());
                graphics.drawString(transparentButton._2, n3 - 1, n4);
            }
            if (transparentButton.isFocused()) {
                graphics.setColor(transparentButton.getForeground());
                graphics.drawRect(0, 0, n, n2);
            }
        }
    }

    public TransparentButton(String string, boolean bl) {
        
        super(string, bl);
    }

    public TransparentButton(String string, ActionListener actionListener) {
      
        super(string,false);
        transparentButton.addActionListener(actionListener);
    }

    public TransparentButton(String string) {
       
        super(string,false);
    }
}
