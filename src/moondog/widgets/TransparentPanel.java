package moondog.widgets;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.LayoutManager;

@SuppressWarnings("serial")
public class TransparentPanel
extends Container {
	TransparentPanel transparentPanel= this;
    public void paint(Graphics graphics) {
        
        transparentPanel.paintComponents(graphics);
    }

  /*  public TransparentPanel() {
        TransparentPanel transparentPanel;
    }*/

    public TransparentPanel(LayoutManager layoutManager) {
        
        transparentPanel.setLayout(layoutManager);
    }

    public void update(Graphics graphics) {
      
        transparentPanel.paint(graphics);
    }
}

