package moondog.widgets;

//import java.awt.Color;
//import java.awt.Component;
//import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
//import java.awt.Toolkit;
//import java.awt.image.ImageObserver;
import java.net.URL;

@SuppressWarnings("serial")
public class PFrame
extends Frame {
    Image _0;
    boolean _1 = true;
    Image _2;
    boolean _3;
    PFrame pFrame = this; 

    public void paint(Graphics graphics) {
       
        Graphics graphics2;
        if (pFrame._0 == null || pFrame._3) {
            if (pFrame._0 != null) {
                pFrame._0.flush();
            }
            pFrame._0 = pFrame.createImage(pFrame.getSize().width, pFrame.getSize().height);
        }
        if (pFrame._1) {
            int n = pFrame._2.getWidth(null);
            int n2 = pFrame._2.getHeight(null);
            graphics2 = pFrame._0.getGraphics();
            int n3 = 0;
            while (n3 < pFrame.getSize().width) {
                int n4 = 0;
                while (n4 < pFrame.getSize().height) {
                    graphics2.drawImage(pFrame._2, n3, n4, null);
                    n4 += n2;
                }
                n3 += n;
            }
        } else {
            graphics2 = pFrame._0.getGraphics();
            graphics2.setColor(pFrame.getBackground());
            graphics2.fillRect(0, 0, pFrame.getSize().width, pFrame.getSize().height);
        }
        graphics2.dispose();
        graphics.drawImage(pFrame._0, 0, 0, null);
        try {
            pFrame.paintComponents(graphics);
            return;
        }
        catch (Exception v0) {
            return;
        }
    }

    public PFrame(String string, String string2) {
       
        super(string);
        try {
            URL uRL = pFrame.getClass().getResource(string2);
            pFrame._2 = pFrame.getToolkit().getImage(uRL);
            MediaTracker mediaTracker = new MediaTracker(pFrame);
            mediaTracker.addImage(pFrame._2, 0);
            mediaTracker.waitForAll();
            return;
        }
        catch (Exception v0) {
            pFrame._2 = null;
            return;
        }
    }

    public PFrame(String string, URL uRL) {
        
        super(string);
        try {
            pFrame._2 = pFrame.getToolkit().getImage(uRL);
            MediaTracker mediaTracker = new MediaTracker(pFrame);
            mediaTracker.addImage(pFrame._2, 0);
            mediaTracker.waitForAll();
            return;
        }
        catch (Exception v0) {
            pFrame._2 = null;
            return;
        }
    }

    public void setBitmapEnabled(boolean bl) {
      
        pFrame._1 = bl;
        pFrame.repaint();
    }

    public void setBounds(int n, int n2, int n3, int n4) {
       
        super.setBounds(n, n2, n3, n4);
        pFrame._3 = true;
    }

    public void update(Graphics graphics) {
        
        pFrame.paint(graphics);
    }
}

