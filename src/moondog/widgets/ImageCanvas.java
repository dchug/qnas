package moondog.widgets;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
//import java.awt.image.ImageObserver;

@SuppressWarnings("serial")
public class ImageCanvas
extends Canvas {
    int _0;
    Image _1;
    int _2;
    ImageCanvas imageCanvas = this;
    public Dimension getMinimumSize() {
        //ImageCanvas imageCanvas = null;
        return imageCanvas.getPreferredSize();
    }

    public Dimension getPreferredSize() {
        return new Dimension(_2,_0);
    }

    public ImageCanvas(Image image) {
        //ImageCanvas imageCanvas = null;
        imageCanvas._1 = image;
        if (image != null) {
            imageCanvas._2 = image.getWidth(imageCanvas);
            imageCanvas._0 = image.getHeight(imageCanvas);
            return;
        }
        imageCanvas._2 = 0;
        imageCanvas._0 = 0;
    }

    public void paint(Graphics graphics) {
        if (_1 != null) {
            //ImageCanvas imageCanvas;
            graphics.drawImage(_1, 0, 0, this);
        }
    }
}

