package moondog.widgets;

import java.awt.AWTEventMulticaster;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
//import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
//import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;

@SuppressWarnings("serial")
public abstract class PButton
extends Component {
    ActionListener _0;
    protected boolean _1;
    String _2;
    protected boolean _3;
    
    public void addActionListener(ActionListener actionListener) {
        _0 = AWTEventMulticaster.add(_0, actionListener);
    }

    public Dimension getMinimumSize() {
        //PButton pButton; //removed declaration
        //return pButton.getPreferredSize(); 
    	return this.getPreferredSize();  //modification on a
    }

    /*
     * (non-Javadoc)
     * @see java.awt.Component#getPreferredSize()
     * In method below removed the PButton declaration. 
     * replaced pButton with this.
     */
    public Dimension getPreferredSize() {
       // PButton pButton;
        //Font font = pButton.getFont();
    	Font font = this.getFont();
        if (font != null) {
            //FontMetrics fontMetrics = pButton.getFontMetrics(pButton.getFont());
        	FontMetrics fontMetrics = this.getFontMetrics(this.getFont());
            //return new Dimension(fontMetrics.stringWidth(pButton._2) + 20, fontMetrics.getHeight() + 10);
        	return new Dimension(fontMetrics.stringWidth(this._2) + 20, fontMetrics.getHeight() + 10);
        }
        return new Dimension(100, 40);
    }

    public boolean isFocused() {
        return _1;
    }

    public boolean isFocusTraversable() {
        return true;
    }

    public boolean isPressed() {
        return _3;
    }

    public abstract void paint(Graphics var1);

    /*
     * In method below replaces pButton with this.
     */
    protected void _0() {
       // if (pButton._0 != null) { 
    	if (this._0 != null) {
            //PButton pButton;
            //pButton._0.actionPerformed(new ActionEvent(pButton, 1001, pButton._2));
            this._0.actionPerformed(new ActionEvent(this, 1001, this._2));
        }
    }
   /* public PButton(String string) {
        
        PButton  pButton  = new PButton(string, false);
    }*/

    /*
     * In method below replaces pButton with this.
     */
    public PButton(String string, boolean bl) {
        //PButton pButton;  //removed unnecessary declaration
    	
        this._3 = false;
        this._1 = false;
        this._2 = string;
        this.enableEvents(16);
        this.setFont(new Font(PButton._0("N\u000f5e\"eK:A"), 1, 12));
        _1 var3_3 = this.new _1();
        this.addFocusListener(var3_3);
        _2 var4_4 = this.new _2();
        if (bl) {
        	this.addKeyListener(var4_4);
        }
    }

    
    /*
     * In method below replaces pButton with this.
     */
    public void processMouseEvent(MouseEvent mouseEvent) {
       // PButton pButton;
        if (!this.isEnabled()) {
            return;
        }
        if (mouseEvent.getSource() != this) {
            return;
        }
        switch (mouseEvent.getID()) {
            case 501: {
                this.requestFocus();
                this._3 = true;
                this.repaint();
                return;
            }
            case 502: {
                if (this._3) {
                    this._3 = false;
                    this.repaint();
                }
                this._0();
                return;
            }
            case 505: {
                if (!this._3) break;
                	this._3 = false;
                this.repaint();
                return;
            }
        }
    }

    public void removeActionListener(ActionListener actionListener) {
        //var0._0 = AWTEventMulticaster.remove(var0._0, actionListener);
    	_0 = AWTEventMulticaster.remove(_0, actionListener);
    }
    /*
     * In method below replaces pButton with this.
     */
    public void setEnabled(boolean bl) {
        //PButton pButton;
        if (this._1) {
            this.transferFocus();
        }
        super.setEnabled(bl);
        this.repaint();
    }

    public void setLabel(String string) {
        //PButton pButton;
        this._2 = string;
        this.invalidate();
    }

    private static String _0(String string) {
        char[] arrc = string.toCharArray();
        char[] arrc2 = "Z-Fq.XP".toCharArray();
        int n = arrc.length;
        int n2 = arrc2.length;
        int n3 = 0;
        while (n3 < n) {
            int n4 = n3 % n2;
            int n5 = arrc[n3] - arrc2[n4];
            if (n5 <= 0) {
                n5 += 127;
            }
            arrc[n3] = (char)n5;
            ++n3;
        }
        return new String(arrc);
    }

    private final class _1
    extends FocusAdapter {
    	
    	 
        public void focusGained(FocusEvent focusEvent) {
            /*var0.PButton.this._1 = true;
            var0.PButton.this.repaint();*/
        	PButton.this._1 = true;
            PButton.this.repaint();
        }

        public void focusLost(FocusEvent focusEvent) {
            /*var0.PButton.this._1 = false;
            var0.PButton.this.repaint();*/
        	PButton.this._1 = false;
            PButton.this.repaint();
        }
        /* synthetic */ _1() {
            //_1 var0 ;
       }
       
    }

    private final class _2
    extends KeyAdapter {
        public void keyPressed(KeyEvent keyEvent) {
            if (keyEvent.getKeyCode() == 10) {
                //var0.PButton.this._0();
            	PButton.this._0();
            }
        }

        /* synthetic */ _2() {
           // _2 var0;
        }
    }

}
