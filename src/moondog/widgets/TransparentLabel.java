package moondog.widgets;

import java.awt.Component;
//import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

@SuppressWarnings("serial")
public class TransparentLabel
extends Component {
    String _0;
    Font _1;
    TransparentLabel transparentLabel = this;
    public Font getFont() {
        return _1;
    }

    public void paint(Graphics graphics) {
        if (_1 != null) {
            graphics.setFont(_1);
        }
        FontMetrics fontMetrics = graphics.getFontMetrics();
        graphics.drawString(_0, 5, getSize().height / 2 + fontMetrics.getAscent() / 2);
    }

    public void setFont(Font font) {
        _1 = font;
    }

    public TransparentLabel(String string) {
       
        transparentLabel._0 = string;
    }

    public void update(Graphics graphics) {
        
        transparentLabel.paint(graphics);
    }
}