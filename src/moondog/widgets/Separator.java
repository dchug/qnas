package moondog.widgets;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

@SuppressWarnings("serial")
public class Separator
extends Canvas {
    int _0;
    Separator separator = this;
    public Dimension getMinimumSize() {
        
        return separator.getPreferredSize();
    }

    public Dimension getPreferredSize() {
        return new Dimension(_0, 2);
    }

    public void paint(Graphics graphics) {
       
        separator._0 = separator.getSize().width;
        int n = separator.getSize().height;
        graphics.setColor(separator.getBackground());
        graphics.fillRect(0, 0, separator._0, n);
        graphics.setColor(Color.darkGray);
        graphics.drawLine(0, 0, separator._0 - 1, 0);
        graphics.setColor(Color.white);
        graphics.drawLine(0, 1, separator._0 - 1, 1);
    }

    /*public Separator() {
        Separator separator;
    }*/

    public void setBounds(int n, int n2, int n3, int n4) {
        
        separator._0 = n3;
        super.setBounds(n, n2, n3, n4);
    }

    public void update(Graphics graphics) {
       
        separator.paint(graphics);
    }
}

