package moondog.widgets;

import java.awt.Color;
//import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Panel;
//import java.awt.Toolkit;

@SuppressWarnings("serial")
public class BorderPanel extends Panel {
    int _0=0;
    String _1;
    int _2=0;

    /*public BorderPanel() {
        BorderPanel borderPanel;
    }*/

    
	public BorderPanel(String string) {
        //BorderPanel borderPanel = null;
        //borderPanel._1 = string;
    	/*
    	 * Updated by ketan
    	 */
    	this._1 = string;
    }

    public BorderPanel(String string, LayoutManager layoutManager) {
        //BorderPanel borderPanel = null;
    	BorderPanel borderPanel = new BorderPanel(string);
        borderPanel.setLayout(layoutManager);
    }

    public Insets getInsets() {
        //BorderPanel borderPanel=null;
		@SuppressWarnings("deprecation")
		FontMetrics fontMetrics = this.getToolkit().getFontMetrics(this.getFont());
        return new Insets(fontMetrics.getHeight() + 2, 5, 8, 5);
    }

    public void paint(Graphics graphics) {
        BorderPanel borderPanel = null;
        FontMetrics fontMetrics = graphics.getFontMetrics();
        int n = fontMetrics.getHeight() / 2;
        int n2 = 0;
        @SuppressWarnings("null")
		Color color = borderPanel.getBackground();
        Color color2 = borderPanel.getForeground();
        graphics.setColor(color);
        graphics.fillRect(0, 0, borderPanel._2, borderPanel._0);
        graphics.setColor(Color.darkGray);
        graphics.drawRect(0, n, borderPanel._2 - 3, borderPanel._0 - n - 3);
        graphics.setColor(Color.white);
        graphics.drawRect(1, n + 1, borderPanel._2 - 3, borderPanel._0 - n - 3);
        n = fontMetrics.getHeight() / 2 + fontMetrics.getAscent() / 2;
        n2 = 8;
        if (borderPanel._1 != null) {
            graphics.setColor(color);
            graphics.fillRect(n2 - 1, 0, fontMetrics.stringWidth(borderPanel._1) + 2, n);
            graphics.setColor(color2);
            graphics.drawString(borderPanel._1, n2, n);
        }
    }

   
	public void setBounds(int n, int n2, int n3, int n4) {
        //BorderPanel borderPanel = null;
        this._2 = n3;
        this._0 = n4;
        super.setBounds(n, n2, n3, n4);
    }

    @SuppressWarnings("null")
	public void update(Graphics graphics) {
        BorderPanel borderPanel = null;
        borderPanel.paint(graphics);
    }
}

