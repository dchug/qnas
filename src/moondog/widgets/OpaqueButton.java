package moondog.widgets;

import java.awt.Color;
//import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import moondog.widgets.PButton;

@SuppressWarnings("serial")
public class OpaqueButton
extends PButton {
	String str;
    public OpaqueButton(String string) {
        //OpaqueButton opaqueButton;
    	
        super(string,false);
    }

    public void paint(Graphics graphics) {
       OpaqueButton opaqueButton = new OpaqueButton("");
        int n = opaqueButton.getSize().width - 1;
        int n2 = opaqueButton.getSize().height - 1;
        graphics.setColor(opaqueButton.getBackground());
        graphics.fillRect(0, 0, n, n2);
        if (opaqueButton.isPressed()) {
            graphics.setColor(opaqueButton.getBackground().darker().darker().darker());
        } else {
            graphics.setColor(Color.white);
        }
        graphics.drawRect(0, 0, n, n2);
        graphics.drawRect(1, 1, n - 2, n2 - 2);
        if (!opaqueButton.isPressed()) {
            graphics.setColor(opaqueButton.getBackground().darker().darker().darker());
        } else {
            graphics.setColor(Color.white);
        }
        graphics.drawLine(0, n2, n - 1, n2);
        graphics.drawLine(1, n2 - 1, n, n2 - 1);
        graphics.drawLine(n, 0, n, n2);
        graphics.drawLine(n - 1, 1, n - 1, n2 - 1);
        Font font = opaqueButton.getFont();
        graphics.setFont(font);
        if (font != null) {
            FontMetrics fontMetrics = opaqueButton.getFontMetrics(font);
            int n3 = n / 2 - fontMetrics.stringWidth(opaqueButton._2) / 2;
            int n4 = n2 / 2 + fontMetrics.getMaxDescent() + 2;
            if (!opaqueButton.isPressed()) {
                graphics.setColor(Color.white);
                graphics.drawString(opaqueButton._2, n3 - 1, n4);
                if (opaqueButton.isEnabled()) {
                    graphics.setColor(opaqueButton.getForeground());
                } else {
                    graphics.setColor(opaqueButton.getBackground().darker());
                }
                graphics.drawString(opaqueButton._2, n3, n4);
            } else {
                graphics.setColor(Color.white);
                graphics.drawString(opaqueButton._2, n3, n4);
                graphics.setColor(opaqueButton.getForeground());
                graphics.drawString(opaqueButton._2, n3 - 1, n4);
            }
            if (opaqueButton.isFocused()) {
                graphics.setColor(opaqueButton.getForeground());
                graphics.drawRect(0, 0, n, n2);
            }
        }
    }
}