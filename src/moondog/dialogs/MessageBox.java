/*
 * Decompiled with CFR 0_115.
 */
package moondog.dialogs;

import java.awt.Color;
//import java.awt.Component;
import java.awt.Dialog;
//import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
//import java.awt.LayoutManager;
import java.awt.Panel;
//import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
//import java.awt.event.WindowListener;
import java.util.ResourceBundle;
//import moondog.dialogs._1;
import moondog.widgets.EnterButton;
import moondog.widgets.ImageCanvas;
import moondog.widgets.MessagePanel;
import moondog.widgets.Separator;

@SuppressWarnings("serial")
public class MessageBox extends Dialog implements ActionListener {
    public static final int _0 = 1;
    EnterButton _1;
    EnterButton _2;
    EnterButton _3;
    EnterButton _4;
    public static final int _5 = 8;
    public static final int _6 = 0;
    public static final int _7 = 1;
    int _8;
    ImageCanvas _9;
    public static final int _10 = 3;
    private KeyListener _11;
    Font _12;
    public static final int _13 = 4;
    public static final int _14 = 2;
    public static final int _15 = -1;
    public static final int _16 = 4;
    public static final int _17 = 9;
    public static final int _18 = 1;
    Panel _19;
    Frame _20;
    public static final int _21 = 2;
    static ResourceBundle _22 = ResourceBundle.getBundle(MessageBox._1("8\u000b-~]8[x'qe8[>I\u0002yZ5c2m#\u0004h>f.\u00011"));
    int _23;
    public static final int _24 = 0;
    int _25;
    public static final int _26 = 6;
    public static final int _27 = 14;
    public static final int _28 = 2;
    MessageBox messageBox = this;

    public void actionPerformed(ActionEvent actionEvent) {
    	//MessageBox messageBox = null;
        Object object = actionEvent.getSource();
        if (object.equals(messageBox._4) || object.equals(messageBox._3)) {
            messageBox._23 = 1;
            messageBox.setVisible(false);
            return;
        }
        if (object.equals(messageBox._2)) {
            messageBox._23 = 2;
            messageBox.setVisible(false);
            return;
        }
        if (object.equals(messageBox._1)) {
            messageBox._23 = 0;
            messageBox.setVisible(false);
        }
    }

//    KeyListener _0() {
//        return var0._11;
//    }

    Panel _0(String string) {
        return new MessagePanel(string, 1);
    }

    int _1() {
        return _23;
    }

    private void _0(Image image, String string, int n) {
        //MessageBox messageBox = null;
        messageBox.setBackground(Color.lightGray);
        messageBox.setResizable(false);
        messageBox._11 = messageBox.new _1();
        messageBox.addKeyListener(messageBox._11);
        _3 var4_4 = messageBox.new _3();
        messageBox.addWindowListener(var4_4);
        messageBox._4 = new EnterButton(_22.getString("k5"), messageBox, messageBox._11);
        messageBox._3 = new EnterButton(_22.getString("k7"), messageBox, messageBox._11);
        messageBox._2 = new EnterButton(_22.getString("k6"), messageBox, messageBox._11);
        messageBox._1 = new EnterButton(_22.getString("k8"), messageBox, messageBox._11);
        GridBagLayout gridBagLayout = new GridBagLayout();
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        messageBox.setLayout(gridBagLayout);
        if (image != null) {
            messageBox._9 = new ImageCanvas(image);
            gridBagConstraints.insets = new Insets(5, 10, 5, 5);
            gridBagConstraints.weightx = 0.0;
            gridBagConstraints.weighty = 0.0;
            gridBagConstraints.gridwidth = 1;
            gridBagConstraints.fill = 0;
            gridBagLayout.setConstraints(messageBox._9, gridBagConstraints);
            messageBox.add(messageBox._9);
            messageBox._8 = messageBox._9.getPreferredSize().width + gridBagConstraints.insets.right + gridBagConstraints.insets.left;
            messageBox._25 = messageBox._9.getPreferredSize().height + gridBagConstraints.insets.top + gridBagConstraints.insets.bottom;
        }
        gridBagConstraints.insets = new Insets(5, 10, 5, 10);
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 10;
        messageBox._19 = messageBox._0(string);
        gridBagLayout.setConstraints(messageBox._19, gridBagConstraints);
        messageBox.add(messageBox._19);
        messageBox._8 += messageBox._19.getPreferredSize().width + gridBagConstraints.insets.right + gridBagConstraints.insets.left;
        messageBox._25 = Math.max(messageBox._25, messageBox._19.getPreferredSize().height + gridBagConstraints.insets.top + gridBagConstraints.insets.bottom);
        gridBagConstraints.insets = new Insets(5, 0, 2, 0);
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.0;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 10;
        Separator separator = new Separator();
        gridBagLayout.setConstraints(separator, gridBagConstraints);
        messageBox.add(separator);
        messageBox._25 += separator.getPreferredSize().height + gridBagConstraints.insets.top + gridBagConstraints.insets.bottom;
        Panel panel = new Panel();
        if ((n & 1) > 0) {
            panel.add(messageBox._3);
        }
        if ((n & 2) > 0) {
            panel.add(messageBox._4);
        }
        if ((n & 4) > 0) {
            panel.add(messageBox._2);
        }
        if ((n & 8) > 0) {
            panel.add(messageBox._1);
        }
        gridBagConstraints.insets = new Insets(2, 2, 2, 2);
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.0;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 0;
        gridBagConstraints.anchor = 13;
        gridBagLayout.setConstraints(panel, gridBagConstraints);
        messageBox.add(panel);
        messageBox._8 = Math.max(messageBox._8, panel.getPreferredSize().width + gridBagConstraints.insets.right + gridBagConstraints.insets.left);
        messageBox._25 += panel.getPreferredSize().height + gridBagConstraints.insets.top + gridBagConstraints.insets.bottom;
        messageBox._8 += 50;
        messageBox._25 += 75;
    }

    MessageBox(Frame frame, String string, String string2, int n, int n2) {
        super(frame, string, true);
        Image image;
        //MessageBox messageBox = null;
        
        messageBox._20 = frame;
        switch (n2) {
            case 0: {
                image = _1._4(frame);
                break;
            }
            case 1: {
                image = _1._0(frame);
                break;
            }
            case 2: {
                image = _1._3(frame);
                break;
            }
            case 3: {
                image = _1._1(frame);
                break;
            }
            case 4: {
                image = _1._2(frame);
                break;
            }
            default: {
                image = null;
            }
        }
        if (messageBox.getFont() == null) {
            messageBox.setFont(new Font(MessageBox._1(">|,\u0004l.f4\u0002"), 0, 12));
        }
        messageBox._0(image, string2, n);
    }

    MessageBox(Frame frame, String string, String string2, int n, Image image) {
        super(frame, string, true);
       // MessageBox messageBox = null;
        messageBox._20 = frame;
        messageBox._0(image, string2, n);
    }

//    MessageBox(Frame frame, String string, String string2, int n){
//        
//        MessageBox messageBox = new MessageBox(frame, string, string2, n, -1);
//    }

    @SuppressWarnings("deprecation")
	public void show() {
        //MessageBox messageBox = null;
        messageBox.setSize(messageBox._8, messageBox._25);
        messageBox.setLocation(messageBox._20.getLocation().x + 10, messageBox._20.getLocation().y + 10);
        super.show();
    }

    public static void showAlert(Frame frame, String string) {
        MessageBox.showMessage(frame, _22.getString("k3"), string, 1, 1);
    }

    public static boolean showConfirm(Frame frame, String string) {
        int n = MessageBox.showMessage(frame, _22.getString("k2"), string, 6, 2);
        if (n != 1) {
            return false;
        }
        return true;
    }

    public static void showError(Frame frame, String string) {
        MessageBox.showMessage(frame, _22.getString("k4"), string, 1, 0);
    }

    public static void showInfo(Frame frame, String string) {
        MessageBox.showMessage(frame, _22.getString("k1"), string, 1, 3);
    }

    public static int showMessage(Frame frame, String string, String string2, int n, int n2) {
        MessageBox messageBox = new MessageBox(frame, string, string2, n, n2);
        messageBox.show();
        int n3 = messageBox._1();
        messageBox.dispose();
        return n3;
    }

    public static int showMessage(Frame frame, String string, String string2, int n, Image image) {
        MessageBox messageBox = new MessageBox(frame, string, string2, n, image);
        messageBox.show();
        int n2 = messageBox._1();
        messageBox.dispose();
        return n2;
    }

    public static int showMessage(Frame frame, String string, String string2, int n) {
        return MessageBox.showMessage(frame, string, string2, n, -1);
    }

    public static boolean showWarning(Frame frame, String string) {
        int n = MessageBox.showMessage(frame, _22.getString("k0"), string, 6, 0);
        if (n != 1) {
            return false;
        }
        return true;
    }

    private static String _1(String string) {
        char[] arrc = string.toCharArray();
        char[] arrc2 = "J\u001b=\u0010xHs".toCharArray();
        int n = arrc.length;
        int n2 = arrc2.length;
        int n3 = 0;
        while (n3 < n) {
            int n4 = n3 % n2;
            int n5 = arrc[n3] - arrc2[n4];
            if (n5 <= 0) {
                n5 += 127;
            }
            arrc[n3] = (char)n5;
            ++n3;
        }
        return new String(arrc);
    }

    private final class _1 extends KeyAdapter {
        //_1 var0;
        public void keyPressed(KeyEvent keyEvent) {
            if (keyEvent.getKeyCode() == 27) {
               // var0.messageBox.this._23 = 0;
              //  var0.MessageBox.this.setVisible(false);
            }
        }
    }

    private final class _3 extends WindowAdapter {
      
            //_3 var0;
        

        public void windowClosing(WindowEvent windowEvent) {
           // var0.MessageBox.this._23 = 0;
           // var0.MessageBox.this.setVisible(false);
        }
    }

}

