package moondog.dialogs;

import moondog.dialogs.DialogResources;
import moondog.dialogs._2;

public class DialogResources_en_US
extends DialogResources
implements _2 {
    static final Object[][] _0 = new Object[][]{{"k0", DialogResources_en_US._0("|4\t[xS\r")}, {"k1", DialogResources_en_US._0("nA|\\\u0002R\u0007G\\}")}, {"k2", DialogResources_en_US._0("u?{N\u0003JE\u0016\u0006[uN\u0018@")}, {"k3", DialogResources_en_US._0("f?{_\u0004")}, {"k4", DialogResources_en_US._0("jE\t\\\u0002")}, {"k5", DialogResources_en_US._0("~8\n")}, {"k6", "No"}, {"k7", "OK"}, {"k8", DialogResources_en_US._0("h4\u0005PtQ")}, {"k9", DialogResources_en_US._0("g?wPz")}, {DialogResources_en_US._0("\u0011\u0004F"), DialogResources_en_US._0("i4\tX/7\u000b7")}, {DialogResources_en_US._0("\u0011\u0004G"), DialogResources_en_US._0("i4\tX/,\u00188{[")}, {DialogResources_en_US._0("\u0011\u0004H"), DialogResources_en_US._0("i4\tX/>\u000b?\u0003\\\u0007")}, {DialogResources_en_US._0("\u0011\u0004I"), DialogResources_en_US._0("i4\tX/'\u0012H{")}, {DialogResources_en_US._0("\u0011\u0004J"), DialogResources_en_US._0("r4}R}Y\u0007")}, {DialogResources_en_US._0("\u0011\u0004K"), DialogResources_en_US._0("q<}U\u0004\u0005g?\fR")}, {DialogResources_en_US._0("\u0011\u0004L"), DialogResources_en_US._0("lEwf")}, {DialogResources_en_US._0("\u0011\u0004M"), DialogResources_en_US._0("i4\tX/,\u00184\u0010")}, {DialogResources_en_US._0("\u0011\u0004N"), DialogResources_en_US._0("w8z")}, {DialogResources_en_US._0("\u0011\u0004O"), DialogResources_en_US._0("lE{R}")}, {DialogResources_en_US._0("\u0011\u0005F"), DialogResources_en_US._0("~8\u0003Y~\\")}, {DialogResources_en_US._0("\u0011\u0005G"), DialogResources_en_US._0("g?\fR")}, {DialogResources_en_US._0("\u0011\u0005H"), DialogResources_en_US._0("q<}U\u0004\u0005r4}R}Y\u0007")}, {DialogResources_en_US._0("\u0011\u0005I"), DialogResources_en_US._0("hLw[")}, {DialogResources_en_US._0("\u0011\u0005J"), DialogResources_en_US._0("|;at")}, {DialogResources_en_US._0("\u0011\u0005K"), DialogResources_en_US._0("hH\na~R")}, {DialogResources_en_US._0("\u0011\u0005L"), DialogResources_en_US._0("w8}b{F\u0018")}, {DialogResources_en_US._0("\u0011\u0005M"), DialogResources_en_US._0("gB\u0003Q")}, {DialogResources_en_US._0("\u0011\u0005N"), DialogResources_en_US._0("nGwYxH")}, {DialogResources_en_US._0("\u0011\u0005O"), DialogResources_en_US._0("gB\u0003Q/.\u001a4\u0003Vr")}, {DialogResources_en_US._0("\u0011\u0006F"), DialogResources_en_US._0("kB\u0005a")}, {DialogResources_en_US._0("\u0011\u0006G"), DialogResources_en_US._0("xG\u0010Yt")}, {DialogResources_en_US._0("\u0011\u0006H"), DialogResources_en_US._0("x<\u0011R")}, {DialogResources_en_US._0("\u0011\u0006I"), DialogResources_en_US._0("zF{_/3\u0007@{")}, {DialogResources_en_US._0("\u0011\u0006J"), DialogResources_en_US._0("u4\n`\u0007T\u00187")}, {DialogResources_en_US._0("\u0011\u0006K"), DialogResources_en_US._0("f4XORH")}};

    /*public DialogResources_en_US() {
        DialogResources_en_US dialogResources_en_US;
    }
*/
    public Object[][] getContents() {
        return _0;
    }

    private static String _0(String string) {
        char[] arrc = string.toCharArray();
        char[] arrc2 = "%R\u0016l\u000fd".toCharArray();
        int n = arrc.length;
        int n2 = arrc2.length;
        int n3 = 0;
        while (n3 < n) {
            int n4 = n3 % n2;
            int n5 = arrc[n3] - arrc2[n4];
            if (n5 <= 0) {
                n5 += 127;
            }
            arrc[n3] = (char)n5;
            ++n3;
        }
        return new String(arrc);
    }
}
