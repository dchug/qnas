/*
 * Decompiled with CFR 0_115.
 */
package simu6;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import simu6.Cpu;
import simu6.Node;
import simu6.TaskQVector;

public class Task
extends Node {
    static final int INCOMING_INTO_SYSTEM = 1;
    static final int MOVE_JOB_IN_SYSTEM = 2;
    public double mServiceTime = 0.0;
    public double mInterArrivalTime;
    public double mRealArrivalTime;
    public double mRemainingServiceTime = 0.0;
    public double mRemainingInterArrivalTime = 0.0;
    public double mEventTime;
    public int mNextMove;
    public Cpu mSourceServer;
    public TaskQVector mDestTaskQueue;
    public boolean mIsBeingProcessed;
    public double mTimeInServers = 0.0;
    public int mWorkStationIndex = 0;
    public double QA = 0.0;
    public double QS = 0.0;
    Color[] mColor = new Color[]{Color.yellow, Color.black, Color.pink, Color.blue, Color.green, Color.red, Color.gray, Color.cyan};
    static int mColorSelected = 0;

    public Task() {
        super(0, 0, -1, Color.red);
        this.mIsBeingProcessed = false;
        this.mRemainingServiceTime = 0.0;
    }

    public Task(double interArrTime, double serviceTime) {
        super(0, 0, -1, Color.red);
        this.mInterArrivalTime = interArrTime;
        this.mServiceTime = serviceTime;
        this.mRemainingServiceTime = serviceTime;
        this.mEventTime = 0.0;
        this.mNextMove = 1;
        this.mSourceServer = null;
        this.mDestTaskQueue = null;
        this.mIsBeingProcessed = false;
    }

    public Task(double interArrTime, double serviceTime, Color COLOR) {
        super(0, 0, -1, COLOR);
        this.mInterArrivalTime = interArrTime;
        this.mServiceTime = serviceTime;
        this.mRemainingServiceTime = serviceTime;
        this.mEventTime = 0.0;
        this.mNextMove = 1;
        this.mSourceServer = null;
        this.mDestTaskQueue = null;
        this.mIsBeingProcessed = false;
    }

    public Task(int UpperLeftPoint_X, int UpperLeftPoint_Y, int RADIUS, Color COLOR, double interArrTime, double serviceTime) {
        super(UpperLeftPoint_X, UpperLeftPoint_Y, RADIUS, COLOR);
        this.mInterArrivalTime = interArrTime;
        this.mServiceTime = serviceTime;
        this.mRemainingServiceTime = serviceTime;
        this.mEventTime = 0.0;
        this.mNextMove = 1;
        this.mSourceServer = null;
        this.mDestTaskQueue = null;
        this.mIsBeingProcessed = false;
    }

    public Task(Point c, int RADIUS, Color COLOR, double interArrTime, double serviceTime) {
        super(c, RADIUS, COLOR);
        this.mInterArrivalTime = interArrTime;
        this.mServiceTime = serviceTime;
        this.mRemainingServiceTime = serviceTime;
        this.mEventTime = 0.0;
        this.mNextMove = 1;
        this.mSourceServer = null;
        this.mDestTaskQueue = null;
        this.mIsBeingProcessed = false;
    }

    public void drawTask(Graphics g) {
        super.drawNode(g);
    }

    public void setRandomColor() {
        if (mColorSelected < 0 || mColorSelected >= 8) {
            mColorSelected = 0;
        }
        super.setColor(this.mColor[mColorSelected]);
        ++mColorSelected;
    }
}

