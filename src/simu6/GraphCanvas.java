/*
 * Decompiled with CFR 0_115.
 */
package simu6;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.Point;
import java.awt.PopupMenu;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.ImageObserver;
import java.io.PrintStream;
import java.text.NumberFormat;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Vector;
import kcmdg.sntp.InfoStorage;
import simu6.Applet1;
import simu6.Arrow;
import simu6.Cpu;
import simu6.EventVector;
import simu6.InitTaskVector;
import simu6.Node;
import simu6.RecycleChannel;
import simu6.ResultFrame;
import simu6.Square;
import simu6.Task;
import simu6.TaskQVector;
import simu6.WorkStation;

public class GraphCanvas
extends Canvas
implements Runnable {
    int mW;
    Color[] mColor = new Color[]{Color.yellow, Color.black, Color.pink, Color.blue, Color.green, Color.red, Color.gray, Color.cyan};
    final int MAXNODES = 20;
    final int MAXQUEUELEN = 20;
    final int JOB_NO = 1000;
    final int QSIZE = 500;
    final double mFasterCoefficient = 0.8;
    final double mSlowerCoefficient = 1.2;
    int queueLen;
    int arrowNumber;
    int cpuNumber;
    int mWorkStationNumber;
    final int mTaskQVectorNum = 1;
    double[] mProbability;
    public Cpu[] CPU;
    public WorkStation[] mWorkStation;
    public Square[] qBox;
    public Arrow[] arrow;
    public RecycleChannel mDownRecycleChannel;
    public TaskQVector[] mTaskQVector;
    private InitTaskVector mTasks;
    public EventVector mEventVector;
    public int mRunFlag = 0;
    static final int WAIT = 0;
    static final int RUN = 1;
    public int mDisplayMode;
    public int mSingleStepMode_MoveOneStep = 0;
    static final int NORMAL_DISPLAY_MODE = 100;
    static final int SINGLE_STEP_DISPLAY_MODE = 101;
    static final int MAX_SPEED_DISPLAY_MODE = 102;
    public double mZoomDelta = 1.0;
    public NumberFormat mDoubleFormat;
    public NumberFormat mIntegerFormat;
    public NumberFormat mDoubleFormat7Digits;
    public boolean mFromBeginning;
    Font roman = new Font("TimesRoman", 1, 12);
    Font helvetica = new Font("Helvetica", 1, 15);
    FontMetrics fmetrics;
    int h;
    private Image offScreenImage;
    private Graphics offScreenGraphics;
    private Dimension offScreenSize;
    Thread algrthm;
    public boolean mStepByStepMode;
    Applet1 parent;
    InfoStorage mInfoStorage;
    PopupMenu popup;
    MenuItem mCpuMenuItem;
    Random mRandomInstance;
    static long[] r;
    static long xmod;
    static double rnmax;
    static int i;
    static int j;
    static int k;
    static int n;
    static int n2;
    static int firstcall;
    static double[] table;

    static {
        long[] arrl = new long[18];
        arrl[0] = 585064;
        arrl[1] = 120340;
        arrl[2] = 336032;
        arrl[3] = 360031;
        arrl[4] = 645901;
        arrl[5] = 19751;
        arrl[6] = 19746;
        arrl[7] = 60323;
        arrl[8] = 17001;
        arrl[9] = 89672;
        arrl[10] = 20304;
        arrl[11] = 12434;
        arrl[12] = 45302;
        arrl[13] = 89603;
        arrl[14] = 31234;
        arrl[15] = 68690;
        arrl[16] = 234085;
        r = arrl;
        xmod = 1049057;
        rnmax = xmod;
        i = 17;
        j = 16;
        k = 0;
        n = 18;
        n2 = 200;
        firstcall = 1;
        table = new double[n2];
    }

    GraphCanvas(InfoStorage infoStorage, Applet1 myparent) {
        this.fmetrics = this.getFontMetrics(this.roman);
        this.h = this.fmetrics.getHeight() / 3;
        this.mRandomInstance = null;
        this.parent = myparent;
        this.mInfoStorage = infoStorage;
        this.setBackground(Color.white);
        this.mDisplayMode = 100;
        this.mRunFlag = 0;
        this.mFromBeginning = true;
    }

    double FINV(double U, double A, double S) {
        return (Math.sqrt(S * S + 2.0 * A * U) - S) / A;
    }

    double RNG(int ID, double A, double S) {
        double R = 0.0;
        double U = this.URNG();
        if (ID < 1 || ID > 5) {
            System.out.println("\n\nERROR: Incorrect selection of random number generator\n");
            System.exit(1);
        }
        switch (ID) {
            case 1: {
                if (A <= 0.0 && S <= 0.0) {
                    R = 0.001;
                    break;
                }
                int i = 2;
                while (i < 13) {
                    U += this.URNG();
                    ++i;
                }
                R = A + S * (U - 6.0);
                break;
            }
            case 2: {
                if (A <= 0.0 && S <= 0.0) {
                    R = 0.001;
                    break;
                }
                R = A + U * (S - A);
                break;
            }
            case 3: {
                if (A <= 0.0) {
                    R = 0.001;
                    break;
                }
                R = (- A) * Math.log(U);
                break;
            }
            case 4: {
                R = this.FINV(U, A, S);
                break;
            }
            case 5: {
                if (A <= 0.0) {
                    R = 0.001;
                    break;
                }
                R = A;
                break;
            }
        }
        return R;
    }

    double URNG() {
        GraphCanvas.r[GraphCanvas.i] = (r[j] + r[k]) % xmod;
        double rn = (double)r[i] / rnmax;
        i = (i + 1) % n;
        j = (j + 1) % n;
        k = (k + 1) % n;
        return rn;
    }

    public int choice() {
        if (this.mProbability.length == 1) {
            return 0;
        }
        int I = 0;
        double U = 0.0;
        double F = this.mProbability[0];
        while (U <= 0.0) {
            U = this.rng();
        }
        while (U > F) {
            F += this.mProbability[++I];
        }
        return I;
    }

    public void clear() {
        int i;
        if (this.algrthm != null) {
            this.algrthm.stop();
        }
        int j = 0;
        while (j < 1) {
            this.mTaskQVector[j] = null;
            ++j;
        }
        this.mTaskQVector = null;
        this.mTasks = null;
        if (this.queueLen > 0) {
            i = 0;
            while (i < this.queueLen) {
                this.qBox[i].setCurrTask(null);
                ++i;
            }
        }
        if (this.cpuNumber > 0) {
            i = 0;
            while (i < this.cpuNumber) {
                this.CPU[i].setRunningTask(null);
                ++i;
            }
        }
        this.mDownRecycleChannel.clearAllTasks();
        this.mDisplayMode = 100;
        this.mRunFlag = 0;
        this.mSingleStepMode_MoveOneStep = 0;
        this.repaint();
    }

    public void decrementZoomDelta() {
        this.mZoomDelta *= 0.8;
        this.mInfoStorage.mSlowModelAvgServTime = (int)(this.mZoomDelta * this.mInfoStorage.mAvgServA[0]);
    }

    public int findRandomEmptyCPU(Cpu[] szCPU) {
        if (this.mRandomInstance == null) {
            this.mRandomInstance = new Random(System.currentTimeMillis());
        }
        int I = 0;
        double U = 0.0;
        while (U <= 0.0) {
            U = this.mRandomInstance.nextDouble();
        }
        if (szCPU.length <= 0) {
            return -1;
        }
        Vector<Integer> szEmptyCpuVector = new Vector<Integer>();
        int i = 0;
        while (i < szCPU.length) {
            if (szCPU[i].mCurrentTask == null) {
                szEmptyCpuVector.addElement(new Integer(i));
            }
            ++i;
        }
        int szEmptyCpuNumber = szEmptyCpuVector.size();
        if (szEmptyCpuNumber <= 0) {
            return -1;
        }
        if (szEmptyCpuNumber == 1) {
            return (Integer)szEmptyCpuVector.elementAt(0);
        }
        double[] szDoubleList = new double[szEmptyCpuNumber];
        double szDoubleInterval = 1.0 / (double)szEmptyCpuNumber;
        double szDoubleStart = 0.0;
        int i2 = 0;
        while (i2 < szEmptyCpuNumber) {
            szDoubleList[i2] = szDoubleStart += szDoubleInterval;
            ++i2;
        }
        double F = szDoubleList[0];
        while (U > F) {
            F += szDoubleList[++I];
        }
        return (Integer)szEmptyCpuVector.elementAt(I);
    }

    public void incrementZoomDelta() {
        this.mZoomDelta *= 1.2;
        this.mInfoStorage.mSlowModelAvgServTime = (int)(this.mZoomDelta * this.mInfoStorage.mAvgServA[0]);
    }

    public void init() {
        this.mDisplayMode = 100;
        this.mSingleStepMode_MoveOneStep = 0;
        this.mWorkStationNumber = this.mInfoStorage.mWSNum;
        this.queueLen = 8;
        this.arrowNumber = 4;
        this.cpuNumber = this.mInfoStorage.mProcessorNum;
        this.qBox = new Square[8];
        this.CPU = new Cpu[this.cpuNumber];
        this.mWorkStation = new WorkStation[this.mWorkStationNumber];
        this.arrow = new Arrow[this.arrowNumber];
        int w = 8;
        int h = 4;
        int midPointX = 490;
        int midPointY = 160;
        this.mW = w;
        int i = 0;
        while (i < this.queueLen) {
            this.qBox[i] = new Square(midPointX - i * 4 * w, midPointY, 4 * w, Color.black);
            ++i;
        }
        this.arrow[0] = new Arrow(midPointX - (this.queueLen + 1) * 4 * w, midPointY + 2 * w, midPointX - (this.queueLen - 1) * 4 * w - w, midPointY + 2 * w);
        Point line1_start = new Point(this.qBox[0].UpperLeftPoint.x + this.qBox[0].height, this.qBox[0].UpperLeftPoint.y + this.qBox[0].height / 2);
        Point line1_end = new Point(this.qBox[0].UpperLeftPoint.x + this.qBox[0].height + 4 * w, this.qBox[0].UpperLeftPoint.y + this.qBox[0].height / 2);
        this.arrow[2] = new Arrow(line1_start, line1_end);
        int szTempArrowStart_X = this.arrow[2].getRealHead_X();
        int szTempArrowStart_Y = this.arrow[2].getRealHead_Y();
        if (this.cpuNumber == 1) {
            this.CPU[0] = new Cpu(szTempArrowStart_X + 4 * w, szTempArrowStart_Y - 4 * w, 4 * w, Color.white, this);
            this.CPU[0].setName("CPU0");
        } else if (this.cpuNumber % 2 == 0) {
            i = 0;
            while (i < this.cpuNumber) {
                if (i < this.cpuNumber / 2) {
                    this.CPU[i] = new Cpu(szTempArrowStart_X + 4 * w, szTempArrowStart_Y - w / 2 - i * w - (i + 1) * 8 * w, 4 * w, Color.white, this);
                    this.CPU[i].setName("CPU" + i);
                } else {
                    this.CPU[i] = new Cpu(szTempArrowStart_X + 4 * w, szTempArrowStart_Y + w / 2 + (i - this.cpuNumber / 2) * w + (i - this.cpuNumber / 2) * 8 * w, 4 * w, Color.white, this);
                    this.CPU[i].setName("CPU" + i);
                }
                ++i;
            }
        } else {
            int szCpuNumberHalf = (int)((double)this.cpuNumber / 2.0);
            i = 0;
            while (i < this.cpuNumber) {
                if (i < szCpuNumberHalf) {
                    this.CPU[i] = new Cpu(szTempArrowStart_X + 4 * w, szTempArrowStart_Y - (i + 1) * (w + 8 * w) - 4 * w, 4 * w, Color.white, this);
                    this.CPU[i].setName("CPU" + i);
                } else if (i < this.cpuNumber - 1) {
                    this.CPU[i] = new Cpu(szTempArrowStart_X + 4 * w, szTempArrowStart_Y + (i - szCpuNumberHalf + 1) * w + (i - szCpuNumberHalf) * 8 * w + 4 * w, 4 * w, Color.white, this);
                    this.CPU[i].setName("CPU" + i);
                } else {
                    this.CPU[i] = new Cpu(szTempArrowStart_X + 4 * w, szTempArrowStart_Y - 4 * w, 4 * w, Color.white, this);
                    this.CPU[i].setName("CPU" + i);
                }
                ++i;
            }
        }
        this.arrow[3] = new Arrow(this.CPU[0].mRightMidPoint.x + 4 * w, line1_start.y, this.CPU[0].mRightMidPoint.x + 8 * w, line1_start.y);
        int szWorkStationWidth = 6 * w;
        int szWorkStationHeight = 4 * w;
        szTempArrowStart_X = this.arrow[0].start.x;
        szTempArrowStart_Y = this.arrow[0].start.y;
        if (this.mWorkStationNumber == 1) {
            this.mWorkStation[0] = new WorkStation(szTempArrowStart_X - szWorkStationWidth - 4 * w, szTempArrowStart_Y - szWorkStationHeight / 2, Color.white, this, szWorkStationWidth, szWorkStationHeight);
            this.mWorkStation[0].setName("Workstation0");
        } else if (this.mWorkStationNumber % 2 == 0) {
            i = 0;
            while (i < this.mWorkStationNumber) {
                this.mWorkStation[i] = i < this.mWorkStationNumber / 2 ? new WorkStation(szTempArrowStart_X - szWorkStationWidth - 4 * w, szTempArrowStart_Y - w / 2 - i * w - (i + 1) * szWorkStationHeight, Color.white, this, szWorkStationWidth, szWorkStationHeight) : new WorkStation(szTempArrowStart_X - szWorkStationWidth - 4 * w, szTempArrowStart_Y + w / 2 + (i - this.mWorkStationNumber / 2) * w + (i - this.mWorkStationNumber / 2) * szWorkStationHeight, Color.white, this, szWorkStationWidth, szWorkStationHeight);
                this.mWorkStation[i].setName("Workstation" + i);
                ++i;
            }
        } else {
            int szWSNumberHalf = (int)((double)this.mWorkStationNumber / 2.0);
            i = 0;
            while (i < this.mWorkStationNumber) {
                this.mWorkStation[i] = i < szWSNumberHalf ? new WorkStation(szTempArrowStart_X - szWorkStationWidth - 4 * w, szTempArrowStart_Y - (i + 1) * (w + szWorkStationHeight) - szWorkStationHeight / 2, Color.white, this, szWorkStationWidth, szWorkStationHeight) : (i < this.mWorkStationNumber - 1 ? new WorkStation(szTempArrowStart_X - szWorkStationWidth - 4 * w, szTempArrowStart_Y + (i - szWSNumberHalf + 1) * w + (i - szWSNumberHalf) * szWorkStationHeight + szWorkStationHeight / 2, Color.white, this, szWorkStationWidth, szWorkStationHeight) : new WorkStation(szTempArrowStart_X - szWorkStationWidth - 4 * w, szTempArrowStart_Y - szWorkStationHeight / 2, Color.white, this, szWorkStationWidth, szWorkStationHeight));
                this.mWorkStation[i].setName("Workstation" + i);
                ++i;
            }
        }
        Arrow tempArrow = new Arrow(szTempArrowStart_X - szWorkStationWidth - 8 * w, szTempArrowStart_Y, szTempArrowStart_X - szWorkStationWidth - 12 * w, szTempArrowStart_Y);
        this.arrow[1] = new Arrow(tempArrow.getRealHead_X(), tempArrow.getRealHead_Y(), tempArrow.getRealHead_X() + 4 * w, tempArrow.getRealHead_Y());
        this.mDownRecycleChannel = new RecycleChannel(new Point(this.arrow[3].getRealHead_X(), this.arrow[3].getRealHead_Y()), new Point(this.arrow[3].getRealHead_X(), this.arrow[3].getRealHead_Y() + 24 * w), new Point(this.arrow[1].start.x, this.arrow[1].start.y + 24 * w), new Point(this.arrow[1].start.x, this.arrow[1].start.y), null);
        this.mDownRecycleChannel.setName("down recycle channel");
        this.mDownRecycleChannel.setParent(this);
        Point[] szPoint = new Point[18];
        szPoint[0] = new Point(this.arrow[3].getRealHead_X(), this.arrow[3].getRealHead_Y());
        szPoint[1] = new Point(this.arrow[3].getRealHead_X(), this.arrow[3].getRealHead_Y() + (int)((double)(this.arrow[3].getRealHead_Y() + 24 * w - this.arrow[3].getRealHead_Y()) / 4.0));
        szPoint[2] = new Point(this.arrow[3].getRealHead_X(), this.arrow[3].getRealHead_Y() + (int)((double)(this.arrow[3].getRealHead_Y() + 24 * w - this.arrow[3].getRealHead_Y()) / 4.0) * 2);
        szPoint[3] = new Point(this.arrow[3].getRealHead_X(), this.arrow[3].getRealHead_Y() + (int)((double)(this.arrow[3].getRealHead_Y() + 24 * w - this.arrow[3].getRealHead_Y()) / 4.0) * 3);
        szPoint[4] = new Point(this.arrow[3].getRealHead_X(), this.arrow[3].getRealHead_Y() + 20 * w);
        int left = this.arrow[3].getRealHead_X();
        int right = this.arrow[1].start.x;
        szPoint[5] = new Point(left + (int)((double)(right - left) / 10.0) * 2, this.arrow[1].start.y + 24 * w);
        szPoint[6] = new Point(left + (int)((double)(right - left) / 10.0) * 3, this.arrow[1].start.y + 24 * w);
        szPoint[7] = new Point(left + (int)((double)(right - left) / 10.0) * 4, this.arrow[1].start.y + 24 * w);
        szPoint[8] = new Point(left + (int)((double)(right - left) / 10.0) * 5, this.arrow[1].start.y + 24 * w);
        szPoint[9] = new Point(left + (int)((double)(right - left) / 10.0) * 6, this.arrow[1].start.y + 24 * w);
        szPoint[10] = new Point(left + (int)((double)(right - left) / 10.0) * 7, this.arrow[1].start.y + 24 * w);
        szPoint[11] = new Point(left + (int)((double)(right - left) / 10.0) * 8, this.arrow[1].start.y + 24 * w);
        szPoint[12] = new Point(left + (int)((double)(right - left) / 10.0) * 9, this.arrow[1].start.y + 24 * w);
        szPoint[13] = new Point(this.arrow[1].start.x, this.arrow[1].start.y + 24 * w);
        szPoint[14] = new Point(this.arrow[1].start.x, this.arrow[1].start.y + (int)((double)(this.arrow[1].start.y + 24 * w - this.arrow[1].start.y) / 4.0) * 3);
        szPoint[15] = new Point(this.arrow[1].start.x, this.arrow[1].start.y + (int)((double)(this.arrow[1].start.y + 24 * w - this.arrow[1].start.y) / 4.0) * 2);
        szPoint[16] = new Point(this.arrow[1].start.x, this.arrow[1].start.y + (int)((double)(this.arrow[1].start.y + 24 * w - this.arrow[1].start.y) / 4.0));
        szPoint[17] = new Point(this.arrow[1].start.x, this.arrow[1].start.y);
        this.mDownRecycleChannel.setShowPoints(szPoint);
        this.mTaskQVector = null;
        this.mTaskQVector = new TaskQVector[1];
        this.mTaskQVector[0] = new TaskQVector(500, this.CPU);
        double[] szProbability = new double[this.cpuNumber];
        i = 0;
        while (i < this.cpuNumber) {
            szProbability[i] = 1.0 / (double)this.cpuNumber;
            ++i;
        }
        this.mTaskQVector[0].setRules(szProbability, null);
        this.mTaskQVector[0].setVisualQueue(this.qBox);
        this.mTaskQVector[0].mParent = this.parent;
        i = 0;
        while (i < this.cpuNumber) {
            this.CPU[i].setSourceTaskQueue(this.mTaskQVector[0]);
            ++i;
        }
        this.mEventVector = new EventVector();
        this.mTasks = null;
        this.mTasks = new InitTaskVector(this, this.mInfoStorage);
        this.mDoubleFormat = NumberFormat.getNumberInstance();
        this.mDoubleFormat.setMaximumFractionDigits(3);
        this.mDoubleFormat.setMinimumFractionDigits(3);
        this.mDoubleFormat.setMaximumIntegerDigits(20);
        this.mDoubleFormat7Digits = NumberFormat.getNumberInstance();
        this.mDoubleFormat7Digits.setMaximumFractionDigits(7);
        this.mDoubleFormat7Digits.setMinimumFractionDigits(3);
        this.mDoubleFormat7Digits.setMaximumIntegerDigits(20);
        this.mIntegerFormat = NumberFormat.getNumberInstance();
        this.mIntegerFormat.setParseIntegerOnly(true);
        this.mZoomDelta = (double)this.mInfoStorage.mSlowModelAvgServTime / this.mInfoStorage.mAvgServA[0];
        this.algrthm = null;
        this.algrthm = new Thread(this);
        this.popup = new PopupMenu();
        this.mCpuMenuItem = new MenuItem("Workstation");
        this.popup.add(this.mCpuMenuItem);
        this.add(this.popup);
        SymMouse aSymMouse = new SymMouse();
        this.addMouseListener(aSymMouse);
        int j = 0;
        while (j < this.mWorkStationNumber) {
            Task newTask = this.mTasks.createNewColorTask();
            newTask.color = this.mColor[j];
            this.mWorkStation[j].setRunningTask(newTask);
            ++j;
        }
        this.repaint();
    }

    public String intToString(int i) {
        char c = (char)(97 + i);
        return String.valueOf(c);
    }

    public void moveTaskForward(TaskQVector szTasksInQueue, Square[] szSquare) {
        int i = 0;
        int QueueLen = szSquare.length;
        if (szTasksInQueue == null || szTasksInQueue.isEmpty()) {
            int j = 0;
            while (j < QueueLen) {
                szSquare[j].setCurrTask(null);
                ++j;
            }
            return;
        }
        i = 0;
        while (i < szTasksInQueue.size()) {
            if (i >= QueueLen) {
                return;
            }
            Task task = (Task)szTasksInQueue.elementAt(i);
            szSquare[i].setCurrTask(task);
            ++i;
        }
        if (i >= QueueLen) {
            return;
        }
        int j = i;
        while (j < QueueLen) {
            szSquare[j].setCurrTask(null);
            ++j;
        }
    }

    public void paint(Graphics g) {
        int i;
        g.setFont(this.roman);
        g.setColor(Color.white);
        Color oldColor = Color.white;
        if (this.queueLen > 0) {
            i = 0;
            while (i < this.queueLen) {
                this.qBox[i].drawSquare(g);
                ++i;
            }
        }
        if (this.arrowNumber > 0) {
            i = 0;
            while (i < this.arrowNumber) {
                this.arrow[i].drawArrow(g);
                ++i;
            }
        }
        if (this.cpuNumber > 0) {
            i = 0;
            while (i < this.cpuNumber) {
                this.CPU[i].drawCpu(g);
                ++i;
            }
        }
        int szArrow2End_X = this.arrow[2].getRealHead_X();
        int szArrow2End_Y = this.arrow[2].getRealHead_Y();
        int szTempArrowStart_X = this.arrow[3].start.x;
        int szLineLen = 4 * this.mW;
        if (this.cpuNumber > 1) {
            i = 0;
            while (i < this.cpuNumber) {
                g.drawLine(this.CPU[i].mRightMidPoint.x, this.CPU[i].mRightMidPoint.y, szTempArrowStart_X, this.CPU[i].mRightMidPoint.y);
                g.drawLine(this.CPU[i].mLeftMidPoint.x, this.CPU[i].mLeftMidPoint.y, this.CPU[i].mLeftMidPoint.x - szLineLen, this.CPU[i].mLeftMidPoint.y);
                g.drawLine(this.CPU[i].mLeftMidPoint.x - szLineLen, this.CPU[i].mLeftMidPoint.y, this.CPU[i].mLeftMidPoint.x - szLineLen, szArrow2End_Y);
                g.drawLine(szTempArrowStart_X, this.CPU[i].mRightMidPoint.y, szTempArrowStart_X, szArrow2End_Y);
                ++i;
            }
        } else {
            g.drawLine(this.CPU[0].mRightMidPoint.x, this.CPU[0].mRightMidPoint.y, szTempArrowStart_X, this.CPU[0].mRightMidPoint.y);
            g.drawLine(this.CPU[0].mLeftMidPoint.x, this.CPU[0].mLeftMidPoint.y, this.CPU[0].mLeftMidPoint.x - szLineLen, this.CPU[0].mLeftMidPoint.y);
        }
        szTempArrowStart_X = this.arrow[0].start.x;
        szLineLen = szTempArrowStart_X - this.mWorkStation[0].mRightMidPoint.x;
        szArrow2End_X = this.arrow[2].getRealHead_X();
        szArrow2End_Y = this.arrow[2].getRealHead_Y();
        if (this.mWorkStationNumber > 1) {
            i = 0;
            while (i < this.mWorkStationNumber) {
                g.drawLine(this.mWorkStation[i].mRightMidPoint.x, this.mWorkStation[i].mRightMidPoint.y, szTempArrowStart_X, this.mWorkStation[i].mRightMidPoint.y);
                g.drawLine(this.mWorkStation[i].mLeftMidPoint.x, this.mWorkStation[i].mLeftMidPoint.y, this.mWorkStation[i].mLeftMidPoint.x - szLineLen, this.mWorkStation[i].mLeftMidPoint.y);
                g.drawLine(this.mWorkStation[i].mLeftMidPoint.x - szLineLen, this.mWorkStation[i].mLeftMidPoint.y, this.mWorkStation[i].mLeftMidPoint.x - szLineLen, szArrow2End_Y);
                g.drawLine(szTempArrowStart_X, this.mWorkStation[i].mRightMidPoint.y, szTempArrowStart_X, szArrow2End_Y);
                ++i;
            }
        } else {
            g.drawLine(this.mWorkStation[0].mRightMidPoint.x, this.mWorkStation[0].mRightMidPoint.y, szTempArrowStart_X, this.mWorkStation[0].mRightMidPoint.y);
            g.drawLine(this.mWorkStation[0].mLeftMidPoint.x, this.mWorkStation[0].mLeftMidPoint.y, this.mWorkStation[0].mLeftMidPoint.x - szLineLen, this.mWorkStation[0].mLeftMidPoint.y);
        }
        i = 0;
        while (i < this.mWorkStationNumber) {
            this.mWorkStation[i].drawWorkStation(g);
            ++i;
        }
        this.mDownRecycleChannel.drawRecycleChannel(g);
        g.setColor(oldColor);
    }

    double rng() {
        if (firstcall > 0) {
            int i = 0;
            while (i < n) {
                GraphCanvas.table[i] = this.URNG();
                ++i;
            }
            firstcall = 0;
        }
        int itable = (int)((double)n * this.URNG());
        double rnumber = table[itable];
        GraphCanvas.table[itable] = this.URNG();
        return rnumber;
    }

    public void run() {
        double TA;
        double szEventTime;
        int szIndex = 0;
        double TIME = 0.0;
        double T = 0.0;
        long JOBA = 0;
        long JOBS = 0;
        double TBUSY = 0.0;
        double JOBSEC = 0.0;
        double SDJOB = 0.0;
        double TNA = 0.0;
        double TND = 0.0;
        double ATS = 0.0;
        double SDTS = 0.0;
        double ATA = 0.0;
        double SDTA = 0.0;
        double ART = 0.0;
        double SDRT = 0.0;
        int JMAX = 20000;
        int JPRINT = this.mInfoStorage.mJPRINT;
        int IDA = this.mInfoStorage.mArrivalDistribution;
        double AA = this.mInfoStorage.mAvgArrivalA;
        double SA = this.mInfoStorage.mAvgArrivalS;
        int IDS = this.mInfoStorage.mServDistribution[0];
        int k = 0;
        while (k < this.cpuNumber) {
            this.CPU[k].AS = this.mInfoStorage.mAvgServA[0];
            this.CPU[k].SS = this.mInfoStorage.mAvgServS[0];
            ++k;
        }
        System.out.println("\n\nSIMULATION RESULTS\n\nJOBS  TIME   ATA  SDTA  ATS  SDTS  ART  SDRT  AJOB  SDJOB  U   X\tT\n-----------------------------------------------------------------------------");
        int j = 0;
        while (j < this.mWorkStationNumber) {
            szEventTime = 0.0;
            while ((TA = this.RNG(IDA, AA, SA)) <= 0.0) {
            }
            Task newTask = this.mTasks.createNewColorTask();
            newTask.color = this.mColor[j];
            newTask.mRemainingInterArrivalTime = newTask.mInterArrivalTime = TA;
            newTask.mEventTime = szEventTime += TA;
            newTask.mNextMove = 1;
            newTask.mIsBeingProcessed = false;
            newTask.mWorkStationIndex = j;
            this.mEventVector.insertTaskByEventTime(newTask);
            this.mWorkStation[j].setRunningTask(newTask);
            ++j;
        }
        this.repaint();
        while (this.mRunFlag == 1 && this.mEventVector.size() > 0) {
            double XSDTA;
            double X;
            double XSDTS;
            double U;
            double XSDJOB;
            double XSDRT;
            double XATA;
            double XART;
            double AJOB;
            double XATS;
            if (this.mRunFlag == 1) {
                block78 : {
                    while (this.mDisplayMode == 101 && this.mSingleStepMode_MoveOneStep <= 0) {
                        try {
                            Thread.currentThread();
                            Thread.sleep(500);
                            continue;
                        }
                        catch (InterruptedException v0) {
                            continue;
                        }
                        catch (Exception v1) {}
                    }
                    this.mSingleStepMode_MoveOneStep = 0;
                    try {
                        int szIndex2;
                        double DELTAT;
                        long NJOB;
                        Task szIncomingTask;
                        int i;
                        double szElapsedTime;
                        TaskQVector szSrcTaskQueue;
                        double TS;
                        int j2;
                        int k2;
                        int i2;
                        Task szFirstTask;
                        szIncomingTask = (Task)this.mEventVector.firstElement();
                        this.mEventVector.removeElementAt(0);
                        if (szIncomingTask.mNextMove == 1) {
                            szElapsedTime = szIncomingTask.mRemainingInterArrivalTime;
                            if (szIncomingTask.mRemainingInterArrivalTime < 0.0) {
                                szIncomingTask.mRemainingInterArrivalTime = 0.0;
                            }
                            int szSize = this.mEventVector.size();
                            int k3 = 0;
                            while (k3 < szSize) {
                                Task szTempTask = (Task)this.mEventVector.elementAt(k3);
                                if (szTempTask.mNextMove == 1) {
                                    szTempTask.mRemainingInterArrivalTime -= szElapsedTime;
                                }
                                ++k3;
                            }
                            if (this.mDisplayMode != 102) {
                                try {
                                    Thread.currentThread();
                                    Thread.sleep((int)(szIncomingTask.mRemainingInterArrivalTime * this.mZoomDelta));
                                }
                                catch (InterruptedException v2) {
                                }
                                catch (Exception v3) {}
                                this.mWorkStation[szIncomingTask.mWorkStationIndex].setRunningTask(null);
                            }
                            NJOB = JOBA - JOBS;
                            ++JOBA;
                            ATA += szIncomingTask.mInterArrivalTime;
                            SDTA += szIncomingTask.mInterArrivalTime * szIncomingTask.mInterArrivalTime;
                            DELTAT = szIncomingTask.mEventTime - TIME;
                            int i3 = 0;
                            while (i3 < this.cpuNumber) {
                                if (this.CPU[i3].mCurrentTask != null) {
                                    this.CPU[i3].TBUSY += DELTAT;
                                }
                                ++i3;
                            }
                            JOBSEC += DELTAT * (double)NJOB;
                            SDJOB += DELTAT * (double)NJOB * (double)NJOB;
                            TIME = szIncomingTask.mEventTime;
                            szIncomingTask.mIsBeingProcessed = false;
                            szIncomingTask.mRealArrivalTime = szIncomingTask.mEventTime;
                            this.mTaskQVector[0].appendToList(szIncomingTask);
                            if (this.mDisplayMode != 102) {
                                this.moveTaskForward(this.mTaskQVector[0], this.mTaskQVector[0].mVisualQueue);
                            }
                            j2 = 0;
                            while (j2 < 1) {
                                do {
                                    k2 = 0;
                                    while (k2 < this.mTaskQVector[j2].mConnectedServers.length) {
                                        if (this.mTaskQVector[j2].mConnectedServers[k2].mCurrentTask != null) {
                                            this.mTaskQVector[j2].mConnectedServers[k2].mCurrentTask.mRemainingServiceTime -= szElapsedTime;
                                        }
                                        ++k2;
                                    }
                                    i2 = this.findRandomEmptyCPU(this.mTaskQVector[j2].mConnectedServers);
                                    if (i2 < 0) break;
                                    szFirstTask = null;
                                    szFirstTask = this.mTaskQVector[j2].getFirstUnprocessedElement();
                                    if (szFirstTask == null) break;
                                    szFirstTask.mIsBeingProcessed = true;
                                    while ((TS = this.RNG(IDS, this.mTaskQVector[j2].mConnectedServers[i2].AS, this.mTaskQVector[j2].mConnectedServers[i2].SS)) <= 0.0) {
                                    }
                                    szFirstTask.mRemainingServiceTime = szFirstTask.mServiceTime = TS;
                                    szFirstTask.mEventTime = TIME + szFirstTask.mServiceTime;
                                    szFirstTask.mNextMove = 2;
                                    szFirstTask.mSourceServer = this.mTaskQVector[j2].mConnectedServers[i2];
                                    this.mTaskQVector[j2].setJobDirection(szFirstTask);
                                    this.mEventVector.insertTaskByEventTime(szFirstTask);
                                    this.mTaskQVector[j2].mConnectedServers[i2].setRunningTask(szFirstTask);
                                } while (true);
                                ++j2;
                            }
                            if (this.mDisplayMode != 102) {
                                this.repaint();
                            }
                            break block78;
                        }
                        if (szIncomingTask.mNextMove != 2) break block78;
                        szElapsedTime = szIncomingTask.mRemainingServiceTime;
                        if (szIncomingTask.mRemainingServiceTime < 0.0) {
                            szIncomingTask.mRemainingServiceTime = 0.0;
                        }
                        if (this.mDisplayMode != 102 && this.CPU[0].mCurrentTask != null) {
                            try {
                                Thread.currentThread();
                                Thread.sleep((int)(szIncomingTask.mServiceTime * this.mZoomDelta));
                            }
                            catch (InterruptedException v4) {
                            }
                            catch (Exception v5) {}
                        }
                        boolean szShouldExit = false;
                        Task szExitTask = null;
                        Object szRecycleTask = null;
                        if (szIncomingTask.mDestTaskQueue == null) {
                            NJOB = JOBA - JOBS;
                            DELTAT = szIncomingTask.mEventTime - TIME;
                            i = 0;
                            while (i < this.cpuNumber) {
                                if (this.CPU[i].mCurrentTask != null) {
                                    this.CPU[i].TBUSY += DELTAT;
                                }
                                ++i;
                            }
                            JOBSEC += DELTAT * (double)NJOB;
                            SDJOB += DELTAT * (double)NJOB * (double)NJOB;
                            TIME = szIncomingTask.mEventTime;
                            ++JOBS;
                            ATS += szIncomingTask.mServiceTime;
                            SDTS += szIncomingTask.mServiceTime * szIncomingTask.mServiceTime;
                            ART = ART + TIME - szIncomingTask.mRealArrivalTime;
                            SDRT += (TIME - szIncomingTask.mRealArrivalTime) * (TIME - szIncomingTask.mRealArrivalTime);
                            szSrcTaskQueue = szIncomingTask.mSourceServer.mTaskQVector;
                            szIndex2 = szSrcTaskQueue.indexOf(szIncomingTask);
                            if (szIndex2 >= 0) {
                                szSrcTaskQueue.removeElementAt(szIndex2);
                            }
                            szIncomingTask.mSourceServer.setRunningTask(null);
                            if (this.mDisplayMode != 102) {
                                this.moveTaskForward(szSrcTaskQueue, szSrcTaskQueue.mVisualQueue);
                            }
                            szShouldExit = true;
                            szExitTask = szIncomingTask;
                        } else {
                            DELTAT = szIncomingTask.mEventTime - TIME;
                            i = 0;
                            while (i < this.cpuNumber) {
                                if (this.CPU[i].mCurrentTask != null) {
                                    this.CPU[i].TBUSY += DELTAT;
                                }
                                ++i;
                            }
                            TIME = szIncomingTask.mEventTime;
                            szSrcTaskQueue = szIncomingTask.mSourceServer.mTaskQVector;
                            szIndex2 = szSrcTaskQueue.indexOf(szIncomingTask);
                            if (szIndex2 >= 0) {
                                szSrcTaskQueue.removeElementAt(szIndex2);
                            }
                            if (this.mDisplayMode != 102) {
                                this.moveTaskForward(szSrcTaskQueue, szSrcTaskQueue.mVisualQueue);
                                this.repaint();
                            }
                            szIncomingTask.mNextMove = 2;
                            szIncomingTask.mIsBeingProcessed = false;
                            szIncomingTask.mDestTaskQueue.appendToList(szIncomingTask);
                            if (this.mDisplayMode != 102) {
                                this.moveTaskForward(szIncomingTask.mDestTaskQueue, szIncomingTask.mDestTaskQueue.mVisualQueue);
                            }
                            szIncomingTask.mSourceServer.setRunningTask(null);
                            if (this.mDisplayMode != 102) {
                                this.repaint();
                            }
                        }
                        j2 = 0;
                        while (j2 < 1) {
                            do {
                                k2 = 0;
                                while (k2 < this.mTaskQVector[j2].mConnectedServers.length) {
                                    if (this.mTaskQVector[j2].mConnectedServers[k2].mCurrentTask != null) {
                                        this.mTaskQVector[j2].mConnectedServers[k2].mCurrentTask.mRemainingServiceTime -= szElapsedTime;
                                    }
                                    ++k2;
                                }
                                i2 = this.findRandomEmptyCPU(this.mTaskQVector[j2].mConnectedServers);
                                if (i2 < 0) break;
                                szFirstTask = null;
                                szFirstTask = this.mTaskQVector[j2].getFirstUnprocessedElement();
                                if (szFirstTask == null) break;
                                szFirstTask.mIsBeingProcessed = true;
                                while ((TS = this.RNG(IDS, this.mTaskQVector[j2].mConnectedServers[i2].AS, this.mTaskQVector[j2].mConnectedServers[i2].SS)) <= 0.0) {
                                }
                                szFirstTask.mRemainingServiceTime = szFirstTask.mServiceTime = TS;
                                szFirstTask.mEventTime = TIME + szFirstTask.mServiceTime;
                                szFirstTask.mNextMove = 2;
                                szFirstTask.mSourceServer = this.mTaskQVector[j2].mConnectedServers[i2];
                                this.mTaskQVector[j2].setJobDirection(szFirstTask);
                                this.mEventVector.insertTaskByEventTime(szFirstTask);
                                this.mTaskQVector[j2].mConnectedServers[i2].setRunningTask(szFirstTask);
                            } while (true);
                            if (szShouldExit) {
                                if (this.mDisplayMode != 102) {
                                    this.mDownRecycleChannel.recycleTask(szExitTask);
                                }
                                szShouldExit = false;
                                int szSize = this.mEventVector.size();
                                int k4 = 0;
                                while (k4 < szSize) {
                                    Task szTempTask = (Task)this.mEventVector.elementAt(k4);
                                    if (szTempTask.mNextMove == 1) {
                                        szTempTask.mRemainingInterArrivalTime -= szElapsedTime;
                                    }
                                    ++k4;
                                }
                                szEventTime = TIME;
                                while ((TA = this.RNG(IDA, AA, SA)) <= 0.0) {
                                }
                                szExitTask.mRemainingInterArrivalTime = szExitTask.mInterArrivalTime = TA;
                                szExitTask.mEventTime = szEventTime += TA;
                                szExitTask.mNextMove = 1;
                                szExitTask.mIsBeingProcessed = false;
                                this.mEventVector.insertTaskByEventTime(szExitTask);
                                this.mWorkStation[szExitTask.mWorkStationIndex].setRunningTask(szExitTask);
                            }
                            ++j2;
                        }
                        if (this.mDisplayMode != 102) {
                            this.repaint();
                        }
                        if (JOBS <= 0 || JOBS % (long)JPRINT > 0 && JOBS < (long)JMAX) continue;
                        if (JOBS > (long)JMAX) break;
                        U = this.CPU[0].TBUSY / TIME;
                        AJOB = JOBSEC / TIME;
                        XSDJOB = SDJOB / TIME - AJOB * AJOB <= 0.0 ? 0.0 : Math.sqrt(SDJOB / TIME - AJOB * AJOB);
                        XATS = JOBS > 0 ? ATS / (double)JOBS : 0.0;
                        XSDTS = SDTS / (double)JOBS - XATS * XATS <= 0.0 ? 0.0 : Math.sqrt(SDTS / (double)JOBS - XATS * XATS);
                        XATA = JOBA > 0 ? ATA / (double)JOBA : 0.0;
                        XSDTA = SDTA / (double)JOBA - XATA * XATA <= 0.0 ? 0.0 : Math.sqrt(SDTA / (double)JOBA - XATA * XATA);
                        XART = JOBS > 0 ? ART / (double)JOBS : 0.0;
                        XSDRT = SDRT / (double)JOBS - XART * XART <= 0.0 ? 0.0 : Math.sqrt(SDRT / (double)JOBS - XART * XART);
                        X = (double)JOBS / TIME;
                        double XT = T / (double)JOBS;
                        System.out.println(String.valueOf(this.mIntegerFormat.format(JOBS)) + "   " + this.mDoubleFormat.format(TIME) + "   " + this.mDoubleFormat.format(XATA) + "   " + this.mDoubleFormat.format(XSDTA) + "   " + this.mDoubleFormat.format(XATS) + "   " + this.mDoubleFormat.format(XSDTS) + "   " + this.mDoubleFormat.format(XART) + "   " + this.mDoubleFormat.format(XSDRT) + "   " + this.mDoubleFormat.format(AJOB) + "   " + this.mDoubleFormat.format(XSDJOB) + "   " + this.mDoubleFormat.format(U) + "   " + this.mDoubleFormat.format(X) + "   " + this.mDoubleFormat.format(XT));
                        System.out.println("==============================================================");
                        this.parent.mTableSorterFrame.hide();
                        if (this.mDisplayMode != 102) {
                            this.parent.mTableSorterFrame.setValueAt(this.mIntegerFormat.format(JOBS), 0, 0);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(TIME), 0, 1);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XATA), 0, 2);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDTA), 0, 3);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XATS), 0, 4);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDTS), 0, 5);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XART), 0, 6);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDRT), 0, 7);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(AJOB), 0, 8);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDJOB), 0, 9);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(U), 0, 10);
                            if (X >= 1.0) {
                                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(X), 0, 11);
                            } else {
                                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat7Digits.format(X), 0, 11);
                            }
                        } else {
                            this.parent.mTableSorterFrame.setValueAt(this.mIntegerFormat.format(JOBS), 0, 0);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(TIME), 0, 1);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XATA), 0, 2);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDTA), 0, 3);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XATS), 0, 4);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDTS), 0, 5);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XART), 0, 6);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDRT), 0, 7);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(AJOB), 0, 8);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDJOB), 0, 9);
                            this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(U), 0, 10);
                            if (X >= 1.0) {
                                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(X), 0, 11);
                            } else {
                                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat7Digits.format(X), 0, 11);
                            }
                            ++szIndex;
                        }
                        this.parent.mTableSorterFrame.show();
                        if (this.mDisplayMode == 102) {
                            try {
                                Thread.currentThread();
                                Thread.sleep(2000);
                            }
                            catch (InterruptedException v6) {
                            }
                            catch (Exception v7) {}
                        }
                    }
                    catch (NoSuchElementException v8) {
                    }
                    catch (ArrayIndexOutOfBoundsException v9) {}
                }
                if (!this.mStepByStepMode) continue;
                this.mRunFlag = 1 - this.mRunFlag;
                continue;
            }
            U = this.CPU[0].TBUSY / TIME;
            AJOB = JOBSEC / TIME;
            XSDJOB = SDJOB / TIME - AJOB * AJOB <= 0.0 ? 0.0 : Math.sqrt(SDJOB / TIME - AJOB * AJOB);
            XATS = JOBS > 0 ? ATS / (double)JOBS : 0.0;
            XSDTS = SDTS / (double)JOBS - XATS * XATS <= 0.0 ? 0.0 : Math.sqrt(SDTS / (double)JOBS - XATS * XATS);
            XATA = JOBA > 0 ? ATA / (double)JOBA : 0.0;
            XSDTA = SDTA / (double)JOBA - XATA * XATA <= 0.0 ? 0.0 : Math.sqrt(SDTA / (double)JOBA - XATA * XATA);
            XART = JOBS > 0 ? ART / (double)JOBS : 0.0;
            XSDRT = SDRT / (double)JOBS - XART * XART <= 0.0 ? 0.0 : Math.sqrt(SDRT / (double)JOBS - XART * XART);
            X = (double)JOBS / TIME;
            System.out.println(String.valueOf(this.mIntegerFormat.format(JOBS)) + "   " + this.mDoubleFormat.format(TIME) + "   " + this.mDoubleFormat.format(XATA) + "   " + this.mDoubleFormat.format(XSDTA) + "   " + this.mDoubleFormat.format(XATS) + "   " + this.mDoubleFormat.format(XSDTS) + "   " + this.mDoubleFormat.format(XART) + "   " + this.mDoubleFormat.format(XSDRT) + "   " + this.mDoubleFormat.format(AJOB) + "   " + this.mDoubleFormat.format(XSDJOB) + "   " + this.mDoubleFormat.format(U) + "   " + this.mDoubleFormat.format(X));
            System.out.println("==============================================================");
            this.parent.mTableSorterFrame.hide();
            if (this.mDisplayMode != 102) {
                this.parent.mTableSorterFrame.setValueAt(this.mIntegerFormat.format(JOBS), 0, 0);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(TIME), 0, 1);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XATA), 0, 2);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDTA), 0, 3);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XATS), 0, 4);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDTS), 0, 5);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XART), 0, 6);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDRT), 0, 7);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(AJOB), 0, 8);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDJOB), 0, 9);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(U), 0, 10);
                if (X >= 1.0) {
                    this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(X), 0, 11);
                } else {
                    this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat7Digits.format(X), 0, 11);
                }
            } else {
                this.parent.mTableSorterFrame.setValueAt(this.mIntegerFormat.format(JOBS), 0, 0);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(TIME), 0, 1);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XATA), 0, 2);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDTA), 0, 3);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XATS), 0, 4);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDTS), 0, 5);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XART), 0, 6);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDRT), 0, 7);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(AJOB), 0, 8);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(XSDJOB), 0, 9);
                this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(U), 0, 10);
                if (X >= 1.0) {
                    this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat.format(X), 0, 11);
                } else {
                    this.parent.mTableSorterFrame.setValueAt(this.mDoubleFormat7Digits.format(X), 0, 11);
                }
                ++szIndex;
            }
            this.parent.mTableSorterFrame.show();
            while (this.mRunFlag == 0) {
                try {
                    Thread.currentThread();
                    Thread.sleep(100);
                    continue;
                }
                catch (InterruptedException v10) {
                    continue;
                }
                catch (Exception v11) {}
            }
        }
        this.algrthm = null;
    }

    public void runalg(boolean fromBeginning) {
        System.out.println("hrere -2");
        this.mFromBeginning = fromBeginning;
        if (this.mFromBeginning) {
            this.mDisplayMode = 100;
            this.mSingleStepMode_MoveOneStep = 0;
            System.out.println("hrere -1");
            if (this.algrthm != null) {
                this.algrthm.stop();
            }
            this.init();
            this.start();
        }
    }

    public void start() {
        this.algrthm.start();
    }

    public void stop() {
        if (this.algrthm != null) {
            this.algrthm.suspend();
        }
        this.clear();
        this.mDisplayMode = 100;
        this.mSingleStepMode_MoveOneStep = 0;
    }

    public final synchronized void update(Graphics g) {
        Dimension d = this.size();
        if (this.offScreenImage == null || d.width != this.offScreenSize.width || d.height != this.offScreenSize.height) {
            this.offScreenImage = this.createImage(d.width, d.height);
            this.offScreenSize = d;
            this.offScreenGraphics = this.offScreenImage.getGraphics();
        }
        this.offScreenGraphics.setColor(Color.white);
        this.offScreenGraphics.fillRect(0, 0, d.width, d.height);
        this.paint(this.offScreenGraphics);
        g.drawImage(this.offScreenImage, 0, 0, null);
    }

    class SymMouse
    extends MouseAdapter {
        SymMouse() {
        }

        public void mouseClicked(MouseEvent event) {
            Point queueLowerRight = new Point(GraphCanvas.this.qBox[0].UpperLeftPoint.x + GraphCanvas.this.qBox[0].height, GraphCanvas.this.qBox[0].UpperLeftPoint.y + GraphCanvas.this.qBox[0].height);
            Point mQueueUpperLeft = new Point(GraphCanvas.this.qBox[GraphCanvas.this.queueLen - 1].UpperLeftPoint.x, GraphCanvas.this.qBox[GraphCanvas.this.queueLen - 1].UpperLeftPoint.y);
            Point mQueueLowerRight = new Point(queueLowerRight.x, queueLowerRight.y);
            int click_X = event.getX();
            int click_Y = event.getY();
            if (click_X >= mQueueUpperLeft.x && click_X <= mQueueLowerRight.x && click_Y >= mQueueUpperLeft.y && click_Y <= mQueueLowerRight.y) {
                GraphCanvas.this.popup.getItem(0).setLabel("Queue");
                GraphCanvas.this.popup.show(GraphCanvas.this, click_X, click_Y);
                return;
            }
            GraphCanvas.i = 0;
            while (GraphCanvas.i < GraphCanvas.this.cpuNumber) {
                if (click_X >= GraphCanvas.this.CPU[GraphCanvas.i].UpperLeftPoint.x && click_X <= GraphCanvas.this.CPU[GraphCanvas.i].UpperLeftPoint.x + 2 * GraphCanvas.this.CPU[GraphCanvas.i].radius && click_Y >= GraphCanvas.this.CPU[GraphCanvas.i].UpperLeftPoint.y && click_Y <= GraphCanvas.this.CPU[GraphCanvas.i].UpperLeftPoint.y + 2 * GraphCanvas.this.CPU[GraphCanvas.i].radius) {
                    GraphCanvas.this.popup.getItem(0).setLabel("Processor " + (GraphCanvas.i + 1));
                    GraphCanvas.this.popup.show(GraphCanvas.this, click_X, click_Y);
                    return;
                }
                ++GraphCanvas.i;
            }
            GraphCanvas.i = 0;
            while (GraphCanvas.i < GraphCanvas.this.mWorkStationNumber) {
                if (click_X >= GraphCanvas.this.mWorkStation[GraphCanvas.i].UpperLeftPoint.x && click_X <= GraphCanvas.this.mWorkStation[GraphCanvas.i].UpperLeftPoint.x + GraphCanvas.this.mWorkStation[GraphCanvas.i].mWidth && click_Y >= GraphCanvas.this.mWorkStation[GraphCanvas.i].UpperLeftPoint.y && click_Y <= GraphCanvas.this.mWorkStation[GraphCanvas.i].UpperLeftPoint.y + GraphCanvas.this.mWorkStation[GraphCanvas.i].mHeight) {
                    GraphCanvas.this.popup.getItem(0).setLabel("Workstation");
                    GraphCanvas.this.popup.show(GraphCanvas.this, click_X, click_Y);
                    return;
                }
                ++GraphCanvas.i;
            }
        }
    }

}

