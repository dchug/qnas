/*
 * Decompiled with CFR 0_115.
 */
package simu6;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Insets;
import java.awt.Label;
import java.awt.LayoutManager;
import java.awt.TextArea;
import java.text.NumberFormat;
import kcmdg.sntp.InfoStorage;
import simu6.GraphCanvas;
import simu6.HorizontalOptions;
import simu6.Options;
import simu6.ResultFrame;

public class Applet1
extends Applet {
    public GraphCanvas graphcanvas;
    public Options options;
    public HorizontalOptions hOptions;
    public TextArea mTextAreaResults;
    public InfoStorage mInfoStorage;
    public ResultFrame mTableSorterFrame;
    public Label mLabel;
    public Thread mOptionsThread;
    public Thread mResultTableThread;

    public void init() {
        this.mInfoStorage = new InfoStorage();
        String[] szColumnNames = new String[]{"JOBS", "TIME", "ATA", "SDTA", "ATS", "SDTS", "ART", "SDRT", "AJOB", "SDJOB", "U", "X"};
        NumberFormat mDoubleFormat = NumberFormat.getNumberInstance();
        mDoubleFormat.setMaximumFractionDigits(3);
        mDoubleFormat.setMinimumFractionDigits(3);
        mDoubleFormat.setMaximumIntegerDigits(10);
        NumberFormat mIntegerFormat = NumberFormat.getNumberInstance();
        mIntegerFormat.setParseIntegerOnly(true);
        String[][] szData = new String[][]{{mIntegerFormat.format(0), mDoubleFormat.format(0.0), mDoubleFormat.format(0.0), mDoubleFormat.format(0.0), mDoubleFormat.format(0.0), mDoubleFormat.format(0.0), mDoubleFormat.format(0.0), mDoubleFormat.format(0.0), mDoubleFormat.format(0.0), mDoubleFormat.format(0.0), mDoubleFormat.format(0.0), mDoubleFormat.format(0.0)}};
        this.mTableSorterFrame = new ResultFrame(this);
        this.mTableSorterFrame.resetData(szData);
        this.mTableSorterFrame.pack();
        this.mResultTableThread = new Thread(this.mTableSorterFrame);
        this.mResultTableThread.start();
        this.graphcanvas = new GraphCanvas(this.mInfoStorage, this);
        this.options = new Options(this.mInfoStorage, this, 1);
        this.mOptionsThread = new Thread(this.options);
        this.mOptionsThread.start();
        this.hOptions = new HorizontalOptions(this.mInfoStorage, this);
        this.setLayout(new BorderLayout(10, 10));
        this.resize(688, 430);
        this.add("North", new Label("Interactive System (Machine Repairman)"));
        this.add("Center", this.graphcanvas);
        this.add("East", this.options);
        this.add("South", this.hOptions);
    }

    public Insets insets() {
        return new Insets(10, 10, 10, 10);
    }
}

