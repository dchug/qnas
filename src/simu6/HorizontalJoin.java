/*
 * Decompiled with CFR 0_115.
 */
package simu6;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import simu6.Arrow;

public class HorizontalJoin {
    public Point mLine1Start;
    public Point mLine1End;
    public Point mLine2Start;
    public Point mLine2End;
    public Point mLine3Start;
    public Point mLine3End;
    public Point mP1;
    public Point mP2;
    public Point mP3;
    public Arrow[] mArrow;
    public final int SPLIT = 1;
    public final int JOIN = 2;
    public final int arrowNumber = 3;

    public HorizontalJoin() {
    }

    public HorizontalJoin(Point line1_start, Point line1_end, Point line2_start, Point line2_end, Point line3_start, Point line3_end, Point p1, Point p2, Point p3) {
        this.mLine1Start = new Point(line1_start.x, line1_start.y);
        this.mLine1End = new Point(line1_end.x, line1_end.y);
        this.mLine2Start = new Point(line2_start.x, line2_start.y);
        this.mLine2End = new Point(line2_end.x, line2_end.y);
        this.mLine3Start = new Point(line3_start.x, line3_start.y);
        this.mLine3End = new Point(line3_end.x, line3_end.y);
        this.mP1 = new Point(p1.x, p1.y);
        this.mP2 = new Point(p2.x, p2.y);
        this.mP3 = new Point(p3.x, p3.y);
        this.mArrow = new Arrow[3];
        this.mArrow[0] = new Arrow(this.mLine1Start, this.mLine1End);
        this.mArrow[1] = new Arrow(this.mLine2Start, this.mLine2End);
        this.mArrow[2] = new Arrow(this.mLine3Start, this.mLine3End);
    }

    public HorizontalJoin(Arrow firstArrow, int format, int w) {
        if (format == 1) {
            this.mLine1Start = new Point(firstArrow.start.x, firstArrow.start.y);
            this.mLine1End = new Point(firstArrow.end.x, firstArrow.end.y);
            this.mLine2Start = new Point(firstArrow.getRealHead_X() + 2 * w, firstArrow.getRealHead_Y() - 5 * w);
            this.mLine2End = new Point(firstArrow.getRealHead_X() + 6 * w, firstArrow.getRealHead_Y() - 5 * w);
            this.mLine3Start = new Point(firstArrow.getRealHead_X() + 2 * w, firstArrow.getRealHead_Y() + 5 * w);
            this.mLine3End = new Point(firstArrow.getRealHead_X() + 6 * w, firstArrow.getRealHead_Y() + 5 * w);
            this.mP1 = new Point(firstArrow.getRealHead_X(), firstArrow.getRealHead_Y());
            this.mP2 = new Point(firstArrow.getRealHead_X() + 2 * w, firstArrow.getRealHead_Y() - 7 * w);
            this.mP3 = new Point(firstArrow.getRealHead_X() + 2 * w, firstArrow.getRealHead_Y() + 7 * w);
            this.mArrow = new Arrow[3];
            this.mArrow[0] = new Arrow(this.mLine1Start, this.mLine1End);
            this.mArrow[1] = new Arrow(this.mLine2Start, this.mLine2End);
            this.mArrow[2] = new Arrow(this.mLine3Start, this.mLine3End);
        } else if (format == 2) {
            this.mLine1Start = new Point(firstArrow.start.x, firstArrow.start.y);
            this.mLine1End = new Point(firstArrow.end.x, firstArrow.end.y);
            this.mLine2Start = new Point(firstArrow.start.x - 6 * w, firstArrow.start.y - 5 * w);
            this.mLine2End = new Point(firstArrow.start.x - 2 * w, firstArrow.start.y - 5 * w);
            this.mLine3Start = new Point(firstArrow.start.x - 6 * w, firstArrow.start.y + 5 * w);
            this.mLine3End = new Point(firstArrow.start.x - 2 * w, firstArrow.start.y + 5 * w);
            this.mArrow = new Arrow[3];
            this.mArrow[0] = new Arrow(this.mLine1Start, this.mLine1End);
            this.mArrow[1] = new Arrow(this.mLine2Start, this.mLine2End);
            this.mArrow[2] = new Arrow(this.mLine3Start, this.mLine3End);
            this.mP1 = new Point(this.mArrow[1].getRealHead_X() + 2 * w, firstArrow.start.y);
            this.mP2 = new Point(this.mArrow[1].getRealHead_X(), firstArrow.start.y - 7 * w);
            this.mP3 = new Point(this.mArrow[1].getRealHead_X(), firstArrow.start.y + 7 * w);
        }
    }

    public void drawHorizontalJoin(Graphics g) {
        int[] arrowhead_x = new int[]{this.mP1.x, this.mP2.x, this.mP3.x, this.mP1.x};
        int[] arrowhead_y = new int[]{this.mP1.y, this.mP2.y, this.mP3.y, this.mP1.y};
        g.setColor(Color.black);
        this.mArrow[0].drawArrow(g);
        this.mArrow[1].drawArrow(g);
        this.mArrow[2].drawArrow(g);
        g.drawLine(this.mLine1Start.x, this.mLine1Start.y, this.mLine1End.x, this.mLine1End.y);
        g.drawLine(this.mLine2Start.x, this.mLine2Start.y, this.mLine2End.x, this.mLine2End.y);
        g.drawLine(this.mLine3Start.x, this.mLine3Start.y, this.mLine3End.x, this.mLine3End.y);
        g.fillPolygon(arrowhead_x, arrowhead_y, 4);
    }

    public void repositionParallelArrows(int X) {
        this.mLine2Start.x = X;
        this.mLine3Start.x = X;
        this.mArrow[1].start.x = this.mLine2Start.x;
        this.mArrow[2].start.x = this.mLine3Start.x;
    }
}

