/*
 * Decompiled with CFR 0_115.
 */
package simu6;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import simu6.GraphCanvas;
import simu6.Node;
import simu6.Task;
import simu6.TaskQVector;

public class Cpu
extends Canvas {
    public final int STANDARD_RADIUS = 10;
    public Point UpperLeftPoint;
    public Point mLeftMidPoint;
    public Point mRightMidPoint;
    public int radius;
    public Color color;
    public Point mCenter;
    public boolean mHasTask;
    public Task mCurrentTask = null;
    public boolean noWaitTasks = false;
    public String mName;
    public int mProcessTime;
    private GraphCanvas mParent;
    public TaskQVector mTaskQVector;
    public double SS = 0.0;
    public double AS = 0.0;
    public double TBUSY = 0.0;

    public Cpu(int UpperLeftPoint_X, int UpperLeftPoint_Y, int RADIUS, Color COLOR, GraphCanvas parent) {
        if (RADIUS < 0) {
            RADIUS = 10;
        }
        this.radius = RADIUS;
        this.UpperLeftPoint = new Point(UpperLeftPoint_X, UpperLeftPoint_Y);
        this.color = COLOR;
        this.mCenter = new Point(UpperLeftPoint_X + this.radius, UpperLeftPoint_Y + this.radius);
        this.mHasTask = false;
        this.mParent = parent;
        this.mLeftMidPoint = new Point(UpperLeftPoint_X, this.mCenter.y);
        this.mRightMidPoint = new Point(UpperLeftPoint_X + 2 * this.radius, this.mCenter.y);
    }

    public Cpu(Point c, int RADIUS, Color COLOR, GraphCanvas parent) {
        if (RADIUS < 0) {
            RADIUS = 10;
        }
        this.radius = RADIUS;
        this.UpperLeftPoint = new Point(c.x, c.y);
        this.color = COLOR;
        this.mCenter = new Point(c.x + this.radius, c.y + this.radius);
        this.mHasTask = false;
        this.mParent = parent;
        this.mLeftMidPoint = new Point(this.UpperLeftPoint.x, this.mCenter.y);
        this.mRightMidPoint = new Point(this.UpperLeftPoint.x + 2 * this.radius, this.mCenter.y);
    }

    public void drawCpu(Graphics g) {
        g.setColor(this.color);
        int ovalHeight = 2 * this.radius;
        g.fillOval(this.UpperLeftPoint.x, this.UpperLeftPoint.y, ovalHeight, ovalHeight);
        g.setColor(Color.black);
        g.drawOval(this.UpperLeftPoint.x, this.UpperLeftPoint.y, ovalHeight, ovalHeight);
    }

    public Color getColor() {
        return this.color;
    }

    public int getRadius() {
        return this.radius;
    }

    public Point getUpperLeftPoint() {
        return this.UpperLeftPoint;
    }

    public void paint(Graphics g) {
        this.drawCpu(g);
    }

    public void setColor(Color COLOR) {
        this.color = COLOR;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public void setProcessTime(int processTime) {
        this.mProcessTime = processTime;
    }

    public void setRadius(int RADIUS) {
        if (RADIUS < 0) {
            RADIUS = 10;
        }
        this.radius = RADIUS;
    }

    public void setRunningTask(Task currentTask) {
        this.mCurrentTask = null;
        if (currentTask == null) {
            this.mHasTask = false;
            this.color = Color.white;
            return;
        }
        this.mCurrentTask = currentTask;
        this.color = this.mCurrentTask.color;
        this.mCurrentTask.setUpperLeftPoint(this.mCenter.x - currentTask.radius, this.mCenter.y - currentTask.radius);
        this.mHasTask = true;
    }

    public void setSourceTaskQueue(TaskQVector taskQVector) {
        this.mTaskQVector = taskQVector;
    }

    public void setUpperLeftPoint(int X, int Y) {
        this.UpperLeftPoint.x = X;
        this.UpperLeftPoint.y = Y;
    }

    public void setUpperLeftPoint(Point c) {
        this.UpperLeftPoint.x = c.x;
        this.UpperLeftPoint.y = c.y;
    }
}

