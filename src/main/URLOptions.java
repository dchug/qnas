package main;

import java.applet.AppletContext;
import java.awt.BorderLayout;
import java.awt.Choice;
//import java.awt.Component;
import java.awt.Event;
//import java.awt.LayoutManager;
import java.awt.Panel;
//import java.io.PrintStream;
import java.net.URL;
import main.MainPage;

@SuppressWarnings("serial")
public class URLOptions extends Panel {
    Choice mChooser = new Choice();
    String[] mChoose;
    String[] mTarget = new String[]{"simu1", "simu4", "simu3", "simu6", "simu7"};
    MainPage mParent;

    URLOptions(MainPage myparent) {
        this.mParent = myparent;
        this.setLayout(new BorderLayout());
        this.mChoose = new String[5];
        this.mChoose[0] = new String("Single Server Model");
        this.mChoose[1] = new String("Two Servers Model");
        this.mChoose[2] = new String("Single Server with Feedback");
        this.mChoose[3] = new String("Interactive System");
        this.mChoose[4] = new String("Central Server Model");
        this.mChooser.addItem("Select one model from here");
        this.mChooser.addItem(this.mChoose[0]);
        this.mChooser.addItem(this.mChoose[1]);
        this.mChooser.addItem(this.mChoose[2]);
        this.mChooser.addItem(this.mChoose[3]);
        this.mChooser.addItem(this.mChoose[4]);
        this.add("Center", this.mChooser);
    }

    public boolean action(Event evt, Object arg) {
        String tempSelection;
        if (evt.target instanceof Choice && (tempSelection = this.mChooser.getSelectedItem().trim()).length() > 0) {
            this.display(tempSelection);
        }
        return true;
    }

    public void display(String url) {
        int target = -1;
        int i = 0;
        while (i < 5) {
            if (this.mChoose[i].equals(url)) {
                target = i;
                break;
            }
            ++i;
        }
        if (target < 0) {
            return;
        }
        try {
            AppletContext context = this.mParent.getAppletContext();
            URL szCurrentAppletURL = this.mParent.getCodeBase();
            String tempStr = szCurrentAppletURL.toString().trim();
            tempStr = tempStr.concat(this.mTarget[target]);
            tempStr = tempStr.concat(".html");
            szCurrentAppletURL = new URL(tempStr);
            System.out.println("URL == " + tempStr);
            context.showDocument(szCurrentAppletURL);
        }
        catch (Exception e) {
            System.out.println("Error == " + e);
        }
    }
}
