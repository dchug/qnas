package main;

import java.applet.Applet;
import java.awt.BorderLayout;
//import java.awt.Component;
import java.awt.Insets;
//import java.awt.LayoutManager;
import main.URLOptions;

@SuppressWarnings("serial")
public class MainPage
extends Applet {
    public URLOptions hOptions;

    public void init() {
        this.hOptions = new URLOptions(this);
        this.setLayout(new BorderLayout(10, 10));
        this.resize(700, 430);
        this.add("Center", this.hOptions);
    }

    public Insets insets() {
        return new Insets(10, 10, 10, 10);
    }
}
